<?php
require __DIR__.'/../../vendor/autoload.php';

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// use PHPExcel; 
// use PHPExcel_IOFactory;

class UpdateMiniMaxArticles extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'update:minimax';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		$products_file = "files/vagner.xlsx";
		//$products_file_img = "files/vagner_img.xml";
		$extension = pathinfo($products_file, PATHINFO_EXTENSION);
		

            $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
            $excelObj = $excelReader->load($products_file);
            $worksheet = $excelObj->getSheet(0);
            $lastRow = $worksheet->getHighestRow();
           

            for ($row = 2; $row <= $lastRow; $row++) {
                 $sifra = $worksheet->getCell('A'.$row)->getValue();
                 $cena  = $worksheet->getCell('I'.$row)->getValue();
                 $opis = $worksheet->getCell('R'.$row)->getValue();
                 $opis = preg_replace('~<a.*?</a>~i', 'NO_LINK_HERE', $opis);
                 $regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?).*$)@";
                 $opis= preg_replace($regex, ' ', $opis);
                  
                 if(isset($sifra))
                 {                 
                    DB::statement( "UPDATE roba SET flag_prikazi_u_cenovniku=1,web_opis='".$opis."'  WHERE sifra_is = '".$sifra."' ");                 
               
                 }
            }
		
		
		// $products_img = simplexml_load_file($products_file_img);
      	
  //       foreach($products_img as $product) {

  //           $sifra_is = $product->KOD;
  //           $slike = $product->IMGURL;
          
  //           $article = DB::select("SELECT roba_id FROM roba WHERE sifra_is = '".trim($sifra_is)."'");
            
  //           if(isset($article[0])){
            	  	
				
		// 		if(count(DB::table('web_slika')->where('roba_id',$article[0]->roba_id)->get()) == 0){
					
		//             foreach($slike as $slika){

		//                 try { 
		//                     $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
		//                     $name = $slika_id.'.jpg';
		//                     $destination = 'images/products/big/'.$name;
		//                     $url = $slika;
		//                     $akcija = 1;
                    	
	 //                        $content = @file_get_contents($url);
		                    
	 //                        file_put_contents($destination,$content);

		//                     $insert_data = array(
		//                         'web_slika_id'=>intval($slika_id),
		//                         'roba_id'=> $article[0]->roba_id,
		//                         'akcija'=>$akcija, 
		//                         'flag_prikazi'=>1,
		//                         'putanja'=>'images/products/big/'.$name
		//                         );

		//                     DB::table('web_slika')->insert($insert_data);
		 
		//                 }
		//                 catch (Exception $e) {
		//                 }
		//             }

		// 		}

  //           }

  //       }

		$this->info('Finish.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
