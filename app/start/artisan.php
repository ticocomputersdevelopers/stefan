<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new UpdateDatabaseCommand);
Artisan::add(new ImportOrderAddressCommand);
Artisan::add(new TransferDBDataCommand);
Artisan::add(new ClearInitAopDataCommand);
Artisan::add(new ResizeImagesCommand);
Artisan::add(new TransferArticlesDetailsCommand);
Artisan::add(new InsertOldLinksCommand);
Artisan::add(new GetSbcsImagesCommand);
Artisan::add(new GetImagesCommand);
Artisan::add(new TranslatorCommand);
Artisan::add(new ImportGsmDataCommand);
//Artisan::add(new GetOldDataCommand);
Artisan::add(new TransferDBGembirdCommand);
Artisan::add(new DCMappingCommand);
Artisan::add(new LinkedRobaAndDCCommand);
Artisan::add(new DSCCrawlerCommand);
Artisan::add(new TranslatorStaticCommand);
Artisan::add(new TranslatorStaticAdminCommand);
Artisan::add(new ElasticReindexingCommand);
Artisan::add(new ImportCityExpressPostCodes);
Artisan::add(new UpdateMiniMaxArticles);
Artisan::add(new ImportSiteDataCommand);
Artisan::add(new GenerisaneKarakteristikeCommand);
Artisan::add(new SendMailCrmCommand);