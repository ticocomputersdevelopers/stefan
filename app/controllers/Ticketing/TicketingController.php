<?php
use Service\Mailer;

class TicketingController extends Controller {
    public function reklamacije(){
        $strana = 'ticketing';
        $data=array(
            "org_strana"=>'reklamacije',
            "strana"=>'reklamacije',
            "naziv"=>'Reklamacije',
            "title"=>'Reklamacije',
            "description"=>'Reklamacije',
            "keywords"=>'reklamacije',
            "radniNalozi" => Ticketing::radniNalozi(Input::get('page')?Input::get('page'):1)
        );
        return View::make('ticketing/pages/reklamacije', $data);
	}
    public function index(){
        $data=array(
            "org_strana"=>'prijem_na_servis',
            "strana"=>'prijem_na_servis',
            "naziv"=>'Prijem na servis',
            "title"=>'Prijem na servis',
            "description"=>'Prijem na servis',
            "keywords"=>'prijem na servis',
            "is_shop" => true
        );

        $kupac = (object) array('web_kupac_id'=>0);
        $partner = DB::table('partner')->where('partner_id',1)->first();

        if(is_null($partner)){
            $partner = (object) array('partner_id'=>0);
        }
        $data["radni_nalog"] = RMA::radni_nalog_null((!is_null($kupac) ? $kupac->web_kupac_id : 0),(!is_null($partner) ? $partner->partner_id : 0));
        $data["radni_nalog_troskovi"] = array();

        $data["partner"] = $partner;
        $data["kupac"] = $kupac;

        return View::make('ticketing/pages/radni_nalog_prijem', $data);
	}

    public function radni_nalog_prijem_post(){
        $data = Input::get();
        $data['partner_id'] = 1;
        $data['primio_serviser_id'] = (count($serviseri = DB::select("SELECT * FROM serviser WHERE partner_id = 1 ORDER BY serviser_id ASC")) > 0) ? $serviseri[0]->serviser_id : 0;
        $data['kupac_id'] = Session::get('ticketing_user');
        $data['datum_prijema'] = date('Y-m-d H:i:s');
        $data['datum_servisiranja'] = null;
        
        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!',
            'max' => 'Dužina sadržaja nije dozvoljena!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!',
            'digits_between' => 'Dužina sadržaja nije dozvoljena!',
            'date' => 'Neodgovarajući format!'
        );

        $validator_arr = array(
            'partner_id' => 'required|integer',
            'primio_serviser_id' => 'required|integer|not_in:0',
            // 'datum_prijema' => 'required',
            'uredjaj' => 'required|regex:'.RmaSupport::regex().'|max:500',
            'serijski_broj' => 'required|regex:'.RmaSupport::regex().'|max:255',
            'napomena' => 'regex:'.RmaSupport::regex().'|max:500',
            'opis_kvara' => 'required|regex:'.RmaSupport::regex().'|max:1000',
            // 'datum_kupovine' => 'date',
            'broj_fiskalnog_racuna' => 'regex:'.RmaSupport::regex().'|max:500',
            'proizvodjac' => 'regex:'.RmaSupport::regex().'|max:255',
            // 'datum_servisiranja' => 'date',
            // 'telefon' => 'regex:'.RmaSupport::regex().'|between:3,15',
            // 'email' => 'email|unique:web_kupac,email,'.$data['kupac_id'].',web_kupac_id,status_registracije,1|between:5,50',
            // 'fax' => 'between: 3, 20|regex:'.RmaSupport::regex().'',
            // 'telefon_mobilni' => 'between: 3, 20|regex:'.RmaSupport::regex().''
        );


        $validator = Validator::make($data, $validator_arr, $messages);
        if ($validator->passes()) {

            unset($data['check_serijski_broj']);

            $next_id = DB::select("SELECT nextval('radni_nalog_radni_nalog_id_seq') as new_id")[0]->new_id;
            $data['broj_naloga'] = 'SRN'.str_pad($next_id, 4, '0', STR_PAD_LEFT);
            $data['radni_nalog_id'] = $next_id;
            $data['status_id'] = 1;
            $data['pregledan'] = 0;

            DB::table('radni_nalog')->insert($data);

            $message='Uspešno su poslati podaci.';

            $webKupac = DB::table('web_kupac')->where('web_kupac_id',Session::get('ticketing_user'))->first();
            $subject='Zahtev za prijem na servis';
            $body='<h2>Hvala što ste se prijavili. Vaša reklamacija je zavedena pod '.$data['broj_naloga'].' brojem. Naš serviser će Vam se javiti u što kraćem roku.</h2>'; 
            $body.='<table>
                        <tr>
                            <td>Ime i Prezime</td>
                            <td>'.$webKupac->ime.' '.$webKupac->prezime.'</td>
                        </tr>
                        <tr>
                            <td>E-mail</td>
                            <td>'.$webKupac->email.'</td>
                        </tr>
                        <tr>
                            <td>Uređaj</td>
                            <td>'.$data['uredjaj'].'</td>
                        </tr>
                        <tr>
                            <td>Opis kvara</td>
                            <td>'.$data['opis_kvara'].'</td>
                        </tr>
                        <tr>
                            <td>Grupa</td>
                            <td>'.($data['roba'] == 1 ? 'Roba' : 'Usluga').'</td>
                        </tr>
                        <tr>
                            <td>Hitno</td>
                            <td>'.($data['hitno'] == 1 ? 'Da' : 'Ne').'</td>
                        </tr>
                        <tr>
                            <td>Napomena</td>
                            <td>'.$data['napomena'].'</td>
                        </tr>
                    </table>';

            Mailer::send(Options::company_email(),$webKupac->email,$subject,$body);

            
            return Redirect::to(TicketingOptions::base_url().'ticketing')->with('message',$message);
            
        }else{
            return Redirect::back()->withInput()->withErrors($validator);
        }
    }

}