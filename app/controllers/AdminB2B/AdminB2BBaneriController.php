<?php

class AdminB2BBaneriController extends Controller {

	 public function banners_sliders($id,$type=1){
        $banners = DB::table('baneri_b2b')->where('tip_prikaza',1)->orderBy('redni_broj','asc')->get();
        $sliders = DB::table('baneri_b2b')->where('tip_prikaza',2)->orderBy('redni_broj','asc')->get();
        $popup   = DB::table('baneri_b2b')->where('tip_prikaza',9)->orderBy('redni_broj','asc')->get();

        $od=DB::table('baneri_b2b')->where('baneri_id',$id)->pluck('datum_od');
        $do=DB::table('baneri_b2b')->where('baneri_id',$id)->pluck('datum_do'); 

        $data = array(
            "strana"=>'b2b_baneri_slajderi',
            "title"=> 'Baneri i slajderi',
            "id"=> $id,
            "type"=> $type,
            "banners"=> $banners,
            "sliders"=> $sliders,
            "datum_od"=> Input::get('datum_od') != 0 ? Input::get('datum_od'): $od,
            "datum_do"=> Input::get('datum_do') != 0 ? Input::get('datum_do'): $do,    
            "popup"=> $popup,
            "item"=> $id != 0 ? DB::table('baneri_b2b')->where('baneri_id',$id)->first() : (object) array("baneri_id" => 0,"img" => "","aktivan"=>"","datum_do"=>"","datum_od"=>"","link" => "#","link2" =>"#","naziv" => "","tip_prikaza" => $type)
            );

        return View::make('adminb2b/pages/baneri-slajderi',$data);
    }

    public function banner_edit(){
        $inputs = Input::get();
        $inputs['img'] = Input::file('img');

        $rules = array(
                'naziv' => 'required|regex:'.AdminSupport::regex().'|max:200',
                'link' => 'required|max:200'
                );
        if($inputs['baneri_id'] == 0){
            $rules['img'] = 'required';
        }

        $validator = Validator::make($inputs, $rules,
            array(
                'required' => 'Niste popunili polje.',
                'regex' => 'Polje sadrži nedozvoljene karaktere.',
                'max' => 'Sadržaj polja je predugačak.',
                )
            );
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{

            if(!is_null($inputs['datum_do']) && $inputs['datum_do'] != ''){
                $inputs['datum_do'] = date('Y-m-d',strtotime($inputs['datum_do']));
            }else{
                 $inputs['datum_do']=null;
            }
            if(!is_null($inputs['datum_od']) && $inputs['datum_od'] != ''){
                $inputs['datum_od'] = date('Y-m-d',strtotime($inputs['datum_od']));
            }else{
                 $inputs['datum_od']=null;
            }

            $file = Input::file('img');
            if($file){
                if(!in_array($file->getClientOriginalExtension(),array('jpg','png','jpeg','gif'))){
                    $validator->getMessageBag()->add('img', 'Podrzani fajlvi mogu da budu .jpg, .png, .jpeg i .gif');
                    return Redirect::back()->withInput()->withErrors($validator->messages());
                }

                if($inputs['tip_prikaza'] == 1){
                    $path = 'images/banners/';
                }elseif($inputs['tip_prikaza'] == 9){
                    $path = 'images/banners/';
                }else{
                  $path = 'images/slider/';  
                }
            }

            if($inputs['baneri_id'] == 0){
                unset($inputs['baneri_id']);
                unset($inputs['img']);
                DB::table('baneri_b2b')->insert($inputs);
                if($inputs['tip_prikaza'] == 1){
                    AdminSupport::saveLog('B2B_BANERI_DODAJ', array(DB::table('baneri_b2b')->max('baneri_id')));
                }elseif ($inputs['tip_prikaza'] == 2) {
                    AdminSupport::saveLog('B2B_SLAJDERI_DODAJ', array(DB::table('baneri_b2b')->max('baneri_id')));
                }elseif ($inputs['tip_prikaza'] == 9) {
                    AdminSupport::saveLog('B2B_POP_UP_DODAJ', array(DB::table('baneri_b2b')->max('baneri_id')));
                }
                $id = DB::select("SELECT currval('baneri_b2b_baneri_id_seq') as current_id")[0]->current_id;
            }else{
                $id = $inputs['baneri_id'];
                unset($inputs['img']);
                unset($inputs['baneri_id']);
                DB::table('baneri_b2b')->where('baneri_id',$id)->update($inputs);             
                if($inputs['tip_prikaza'] == 1){
                    AdminSupport::saveLog('B2B_BANERI_IZMENI', array($id)); 
                }elseif ($inputs['tip_prikaza'] == 2) {
                    AdminSupport::saveLog('B2B_SLAJDERI_IZMENI', array($id)); 
                }elseif ($inputs['tip_prikaza'] == 9) {
                    AdminSupport::saveLog('B2B_POP_UP_IZMENI', array($id)); 
                }
            }
            if($file){
                $name = $id.'.'.$file->getClientOriginalExtension();
                $file->move($path, $name);
                DB::table('baneri_b2b')->where('baneri_id',$id)->update(array('img'=>$path.$name));  
            }
            return Redirect::to(AdminOptions::base_url().'admin/b2b/baneri-slajdovi/'.$id.'/'.$inputs['tip_prikaza'])->with('message','Uspešno ste sačuvali podatke.');
        }
    }
    public function baneri_delete($id){
        $tip_prikaza = DB::table('baneri_b2b')->where('baneri_id',$id)->pluck('tip_prikaza');
        if($tip_prikaza == 1){
            AdminSupport::saveLog('B2B_BANERI_OBRISI', array($id));
        }elseif ($tip_prikaza == 2) {
            AdminSupport::saveLog('B2B_SLAJDERI_OBRISI', array($id));
        }elseif ($tip_prikaza == 9) {
            AdminSupport::saveLog('B2B_POP_UP_OBRISI', array($id));
        }
        DB::table('baneri_b2b')->where('baneri_id',$id)->delete();
        return Redirect::to(AdminOptions::base_url().'admin/b2b/baneri-slajdovi/0/1')->with('message','Uspešno ste obrisali sadržaj.');
    }
    public function position_baner(){
        $moved = Input::get('moved');
        $order_arr = Input::get('order');

        foreach($order_arr as $key => $val){
            DB::table('baneri_b2b')->where('baneri_id',$val)->update(array('redni_broj'=>$key));
        }
        $tip_prikaza = DB::table('baneri_b2b')->where('baneri_id',$moved)->pluck('tip_prikaza');

        if($tip_prikaza == 1){
            AdminSupport::saveLog('B2B_BANER_POZICIJA', array($moved));
        }elseif ($tip_prikaza == 2) {
            AdminSupport::saveLog('B2B_SLAJDER_POZICIJA', array($moved));
        }elseif ($tip_prikaza == 9) {
            AdminSupport::saveLog('B2B_POP_UP_POZICIJA', array($moved));
        }
        
    }
    public function bg_uload(){
        
        $slika_ime=$_FILES['bgImg']['name'];
        $slika_tmp_ime=$_FILES['bgImg']['tmp_name'];
        $naziv = Input::get('naziv');
        if( Input::get('link')){
            $link  = Input::get('link');
        }else{
             $link  = '#!';
        }
        if( Input::get('link2')){
            $link2  = Input::get('link2');
        }else{
             $link2  = '#!';
        }
        if ( !empty($slika_ime) ) {
            move_uploaded_file($slika_tmp_ime,"./images/upload/b2b/" . $slika_ime);

        $data = array(
            'naziv' => $naziv,
            'link' => $link,
            'link2' => $link2,
            'img' => "./images/upload/b2b/" . $slika_ime,
            'tip_prikaza' => 3
        );
        
        $count = DB::table('baneri_b2b')->where('tip_prikaza', 3)->count();

        if($count == 1){
            DB::table('baneri_b2b')->where('tip_prikaza', 3)->update($data);
        } else {
            DB::table('baneri_b2b')->where('tip_prikaza', 3)->insert($data);
        }          
        AdminSupport::saveLog('B2B_BANERI_SLIKA_DODAJ');
        return Redirect::back()->with('message','Uspešno ste dodali sliku.');
        }elseif(!empty($link)){
            $data = array(
                'naziv' => $naziv,
                'link' => $link,
                'link2' => $link2,
                'tip_prikaza' => 3
            );

            $count = DB::table('baneri_b2b')->where('tip_prikaza', 3)->count();

            if($count == 1){
                DB::table('baneri_b2b')->where('tip_prikaza', 3)->update($data);
            } else {
                DB::table('baneri_b2b')->where('tip_prikaza', 3)->insert($data);
            } 
            AdminSupport::saveLog('B2B_BANERI_LINKOVI_DODAJ');
            return Redirect::back()->with('message','Uspešno ste dodali linkove.'); 
        }elseif(!empty($link2)){
            $data = array(
                'naziv' => $naziv,
                'link' => $link,
                'link2' => $link2,
                'tip_prikaza' => 3
            );

            $count = DB::table('baneri_b2b')->where('tip_prikaza', 3)->count();

            if($count == 1){
                DB::table('baneri_b2b')->where('tip_prikaza', 3)->update($data);
            } else {
                DB::table('baneri_b2b')->where('tip_prikaza', 3)->insert($data);
            } 
            AdminSupport::saveLog('B2B_BANERI_LINKOVI_DODAJ');
            return Redirect::back()->with('message','Uspešno ste dodali linkove.');
        }else{
            return Redirect::back()->with('message','Unesite podatke.');
        }
    }
   

    public function bgImgDelete() {
        AdminSupport::saveLog('B2B_BANERI_SLIKE_LINKOVI_OBRISI');
        DB::table('baneri_b2b')->where('tip_prikaza', 3)->delete();
        return Redirect::to('admin/b2b/baneri-slajdovi/0/1');
    }

}