<?php

class AdminGarancijaController extends Controller {

    public function garancije($search,$datum_od,$datum_do,$sort_column,$sort_direction){
        $limit = 20;

        if(is_null($datum_od) || $datum_od=='' || $datum_od=='null'){
            $datum_od=null;
        }
        if(is_null($datum_do) || $datum_do=='' || $datum_do=='null'){
            $datum_do=null;
        }

        $garancije=AdminGarancije::getGarancije($limit,(is_null(Input::get('page')) ? 1 : Input::get('page')),($search != 'null' ? $search : null),$datum_od,$datum_do,$sort_column,$sort_direction);

        if(Input::get('izvestaj') == 1){
            $store_paths = array();

            $izvestaj_limit = 2000;
            $max_page = intdiv($garancije->count,$izvestaj_limit);
            if(fmod($garancije->count,$izvestaj_limit)){
                $max_page++;
            }

            foreach(range(1,$max_page) as $pg){
                $garancije=AdminGarancije::getGarancije($izvestaj_limit,$pg,($search != 'null' ? $search : null),$datum_od,$datum_do,$sort_column,$sort_direction);

                $data = array(
                    array('','RB','Ime','Prezime','Adresa','Grad','Grupa','Proizvod','Serijski broj','Naziv mp.','Mesto mp.','Fisk. račun','Dat. kreiranja','Poslednja izmena')
                );
                foreach($garancije->items as $key => $item){

                    $data[] = array(
                        (($izvestaj_limit*($pg-1))+$key+1), $item->produzene_garancije_id, $item->ime, $item->prezime, $item->adresa, $item->grad, $item->grupa, $item->naziv_web, $item->serijski_broj, $item->maloprodaja, $item->prodavnica_mesto, $item->fiskalni, $item->final_parent_created, $item->datum_kreiranja
                    );
                }
                
                $doc = new PHPExcel();
                $doc->setActiveSheetIndex(0);
                $doc->getActiveSheet()->fromArray($data);
                 
                $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

                $store_path = 'files/garancije_izvestaj_'.$pg.'_'.date('Y-m-d').'.xls';
                $objWriter->save($store_path);

                $store_paths[] = $store_path;
            }

            $zipname = 'files/garancije.zip';
            if(File::exists($zipname)){
                File::delete($zipname);
            }
            $zip = new ZipArchive;
            $zip->open($zipname, ZipArchive::CREATE);

            foreach($store_paths as $path){
                $zip->addFile($path);
            }
            $zip->close();

            foreach($store_paths as $path){
                File::delete($path);
            }

            return Redirect::to(RmaOptions::base_url().$zipname);      
        }

        $data=array(
            "strana"=>'garancije',
            "title"=> 'Garancije',
            "search" => $search != 'null' ? $search : '',
            "datum_od" => $datum_od,
            "datum_do" => $datum_do,
            "garancije" => $garancije->items,
            "count" => $garancije->count,
            "limit" => $limit,
            "sort_column" => $sort_column,
            "sort_direction" => $sort_direction
        );
        return View::make('admin/page', $data);
    }

    public function garancija($produzene_garancije_id){

        $query = "SELECT * FROM produzene_garancije WHERE produzene_garancije_id = ".$produzene_garancije_id."";

        $garancije = DB::select($query);
        if(!isset($garancije[0])){
            return Redirect::to(AdminOptions::base_url().'admin/garancije');
        }
        $branchIds = array();
        AdminGarancije::getGeneralAndChildsIds($produzene_garancije_id,$branchIds);
        $branch = DB::table('produzene_garancije')->select('*','proizvodjac.naziv AS proizvodjac')->leftJoin('roba','roba.roba_id','=','produzene_garancije.roba_id')->leftJoin('grupa_pr','grupa_pr.grupa_pr_id','=','roba.grupa_pr_id')->leftJoin('proizvodjac','proizvodjac.proizvodjac_id','=','produzene_garancije.proizvodjac_id')->whereIn('produzene_garancije_id',$branchIds)->orderBy('produzene_garancije_id','asc')->paginate(10);


        $data=array(
            "strana"=>'garancija',
            "title"=> 'Garancija',
            "garancija" => $garancije[0],
            "proizvodjaci"=>DB::table('proizvodjac')->where('produzena_garancija',1)->whereIn('proizvodjac_id',array_map('current',DB::table('roba')->select('proizvodjac_id')->where('produzena_garancija',1)->get()))->get(),
            "branch" => $branch
        );
        return View::make('admin/page', $data);
    }

    public function garancija_save(){

        $inputs = Input::get();
        
        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'email' => 'Unesite odgovarajuči mail!',
            'between' => 'Unesite više od dva a manje od 50 karaktera/brojeva!',
            'unique' => 'Mail već postoji u bazi!',
            'max'=>'Prekoračili ste limit!',
            'numeric'=>'Unesite samo brojeve!',
            'digits_between' => 'Dužina sadržaja nije odgovarajuća!',
            'integer' => 'Neodgovarajući sadržaj polja!'
        );
        $rules = array(
            'ime' => 'required|between:0,50',
            'prezime' => 'required|between:0,50',
            'adresa' => 'required|between:0,50',
            'grad' => 'required|between:0,50',
            'email' => 'email|max:50',
            'telefon' => 'required|max:50',
            'proizvodjac_id' => 'required|integer',
            'serijski_broj' => 'required|numeric|digits_between:15,50',
            'maloprodaja' => 'required|between:0,50',
            'prodavnica_mesto' => 'required|between:0,50',
            'fiskalni' => 'required|digits_between:0,6',
            'datum_izdavanja' => 'required',
            'roba_id' => 'required|integer'
        );
        $validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        }else{
            $garancija = DB::table('produzene_garancije')->where('produzene_garancije_id',$inputs['parent_id'])->first();
            if(
                $inputs['ime'] != $garancija->ime
                || $inputs['prezime'] != $garancija->prezime
                || $inputs['adresa'] != $garancija->adresa
                || $inputs['grad'] != $garancija->grad
                || $inputs['email'] != $garancija->email
                || $inputs['telefon'] != $garancija->telefon
                || $inputs['proizvodjac_id'] != $garancija->proizvodjac_id
                || $inputs['serijski_broj'] != $garancija->serijski_broj
                || $inputs['maloprodaja'] != $garancija->maloprodaja
                || $inputs['prodavnica_mesto'] != $garancija->prodavnica_mesto
                || $inputs['fiskalni'] != $garancija->fiskalni
                || $inputs['datum_izdavanja'] != $garancija->datum_izdavanja
                || $inputs['roba_id'] != $garancija->roba_id
            ){
                unset($inputs['model']);
                $inputs['datum_kreiranja'] = date('Y-m-d H:i:s');
                DB::table('produzene_garancije')->insert($inputs);
                return Redirect::to(AdminOptions::base_url().'admin/garancija/'.DB::table('produzene_garancije')->max('produzene_garancije_id'))->with('message','Uspešno ste sačuvali izmene!');
            }

            return Redirect::back()->with('error_message','Nema izmena koje bi ste sačuvali!');
        }
    }

    public function garancija_delete($produzene_garancije_id){
        $garancija = DB::table('produzene_garancije')->where('produzene_garancije_id',$produzene_garancije_id)->first();
        if(is_null($garancija->parent_id)){
            DB::table('produzene_garancije')->where('produzene_garancije_id',$produzene_garancije_id)->delete();
        }else{
            $finalParent = AdminGarancije::getFinalParent($garancija->parent_id);
            DB::table('produzene_garancije')->where('produzene_garancije_id',$finalParent->produzene_garancije_id)->delete();
        }
        return Redirect::to(AdminOptions::base_url().'admin/garancije')->with('message','Uspešno ste obrisali garanciju!');
    }
    
    public function proizvodjac_articles(){
        $proizvodjac_id = Input::get('proizvodjac_id');
        $garancija_proizvodjac = DB::table('proizvodjac')->where('proizvodjac_id',$proizvodjac_id)->where('produzena_garancija',1)->orderBy('proizvodjac_id','asc')->first();
        $articles = DB::select("SELECT roba_id, sku, naziv_web, grupa FROM roba r LEFT JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id LEFT JOIN proizvodjac p ON r.proizvodjac_id = p.proizvodjac_id WHERE p.proizvodjac_id > 0".(!is_null($garancija_proizvodjac) ? " AND p.proizvodjac_id = ".$garancija_proizvodjac->proizvodjac_id : "")." AND r.produzena_garancija = 1 AND p.produzena_garancija = 1 ORDER BY grupa ASC");

        $render = '';
        foreach($articles as $article){
            $render .= '<option value="'.$article->roba_id.'">'.$article->naziv_web.'</option>';
        }
        return $render;
    }

    public function produzena_garancija_pdf($id) {
        $data = array(
            'garancija' => DB::select("SELECT *, CASE WHEN parent_id IS NULL THEN datum_kreiranja ELSE get_final_parent_date_created(parent_id) END AS final_parent_created FROM produzene_garancije WHERE produzene_garancije_id=".$id)[0]
        );
        
        $pdf = App::make('dompdf');
        $pdf->loadView('admin.produzena_garancija_pdf', $data);

        return $pdf->stream();
    }

}