<?php

class AdminNacinIsporukeController extends Controller {

	public function index($id) {
		$data = array(
	                "strana"=>'nacin_isporuke',
	                "title"=> 'Način isporuke',	  
	                "nacini_isporuke" => AdminNacinIsporuke::all(),
	                "web_nacin_isporuke_id" => $id,
	                "nacin_isporuke" => AdminNacinIsporuke::getSingle($id)       
	            );
		return View::make('admin/page', $data);
	}

	public function edit($id) {
		$inputs = Input::get();
		
		if(isset($inputs['aktivno'])){
			$aktivno = 1;
		} else {
			$aktivno = 0;
		}

		$validator = Validator::make($inputs, array('naziv' => 'required'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	if($id == 0){
        		$current = DB::select("SELECT MAX(web_nacin_isporuke_id) AS max FROM web_nacin_isporuke")[0]->max;
        		$web_nacin_isporuke_id = $current + 1;
        		DB::table('web_nacin_isporuke')->insert(array('web_nacin_isporuke_id' => $web_nacin_isporuke_id, 'naziv' => $inputs['naziv'], 'selected' => $aktivno));

        		AdminSupport::saveLog('SIFARNIK_NACIN_ISPORUKE_DODAJ', array(DB::table('web_nacin_isporuke')->max('web_nacin_isporuke_id')));

        		return Redirect::to(AdminOptions::base_url().'admin/nacin_isporuke/0')->withMessage('Uspešno ste sačuvali podatke.');
			} else {
				DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id', $id)->update(array('naziv' => $inputs['naziv'], 'selected' => $aktivno));

				AdminSupport::saveLog('SIFARNIK_NACIN_ISPORUKE_IZMENI', array($id));

				return Redirect::to(AdminOptions::base_url().'admin/nacin_isporuke/'.$id)->withMessage('Uspešno ste sačuvali podatke.');
			}
        }

	}

	public function delete($id) {
		AdminSupport::saveLog('SIFARNIK_NACIN_ISPORUKE_OBRISI', array($id));
		DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id', $id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/nacin_isporuke/0')->withMessage('Uspešno ste obrisali vrednost.');
	}





}