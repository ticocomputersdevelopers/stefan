<?php

class VestiController extends Controller {


	public function getNews(){
        $lang = Language::multi() ? Request::segment(1) : null;

		$strana = 'blog';
		$check_page = DB::table('web_b2c_seo')->where('naziv_stranice',$strana)->count();
		if($check_page==0){
			return Redirect::to(Options::base_url());
		}
		$seo = Seo::page($strana);
		$data=array(
                "strana"=> $strana,
                "org_strana"=>$strana,
	            "title"=>$seo->title,
	            "description"=>$seo->description,
	            "keywords"=>$seo->keywords           
	            );
		return View::make('shop/themes/'.Support::theme_path().'pages/news', $data);
	} 

	public function oneNew(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $blog_slug = Request::segment(2+$offset);


        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>Language::lang()))->pluck('jezik_id');
        $lang_blogs = DB::table('web_vest_b2c_jezik')->where('jezik_id',$jezik_id)->get();
        $id = null;

        foreach($lang_blogs as $lang_blog){
            if($blog_slug == Url_mod::slugify($lang_blog->naslov)){
                $id = $lang_blog->web_vest_b2c_id;
            }
        }

        if(is_null($id)){
            return Redirect::to(Options::base_url());
        }

        $vest = DB::table('web_vest_b2c')->where('web_vest_b2c_id',$id)->first();
        $previous_vest = DB::table('web_vest_b2c')->where('web_vest_b2c_id',$id-1)->first();
        $next_vest = DB::table('web_vest_b2c')->where('web_vest_b2c_id',$id+1)->first();

        if(is_null($vest)){
        	return Redirect::to(Options::base_url());
        }
		$vesti_jezik=DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$id, 'jezik_id'=>Language::lang_id()))->first();
        $seo = Seo::vest($id);


        $previous = DB::table('web_vest_b2c')->where('rbr','<',$vest->rbr)->orderBy('rbr','desc')->first();
        $next = DB::table('web_vest_b2c')->where('rbr','>',$vest->rbr)->orderBy('rbr','asc')->first();
        if(!is_null($previous)){
			$previous_vest = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('web_vest_b2c.web_vest_b2c_id as web_vest_b2c_id','web_vest_b2c_jezik.naslov as naslov')->where(array('web_vest_b2c.web_vest_b2c_id'=>$previous->web_vest_b2c_id,'aktuelno'=>1,'jezik_id'=>Language::lang_id()))->first();
		}
		if(!is_null($next)){
			$next_vest = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('web_vest_b2c.web_vest_b2c_id as web_vest_b2c_id','web_vest_b2c_jezik.naslov as naslov')->where(array('web_vest_b2c.web_vest_b2c_id'=>$next->web_vest_b2c_id,'aktuelno'=>1,'jezik_id'=>Language::lang_id()))->first();
		}

		$data=array(
                "strana"=> "vest",
                "org_strana"=> "vest",
                "title"=>$seo->title,
                "description"=>$seo->description,
                "keywords"=>$seo->keywords,
                "og_image"=> isset($vest->slika) ? $vest->slika : null,
                "id" => $id,
                'slika' => $vest && $vest->slika ? $vest->slika : '',
                'datum' => $vest && $vest->datum ? $vest->datum : '',
                'naslov' => $vesti_jezik && $vesti_jezik->naslov ? $vesti_jezik->naslov : '',
                'sadrzaj' => $vesti_jezik && $vesti_jezik->sadrzaj ? $vesti_jezik->sadrzaj : '',
                'previous_vest' => !is_null($previous) ? $previous_vest : null,
                'next_vest' => !is_null($next) ? $next_vest : null
	            );
		return View::make('shop/themes/'.Support::theme_path().'pages/new', $data);
	}	
}