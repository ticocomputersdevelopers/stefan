<?php

class AkcijaController extends Controller {
    public function __construct(){
        View::share('cartArticlesIds',Cart::cartArticlesIds());
    }

	public function akcija_products(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$grupa = Request::segment(2+$offset);

		$datum = date('Y-m-d');
		if($grupa==null){
			$check_grupa = "";
		}else{
			foreach(DB::table('grupa_pr')->where('grupa_pr_id','!=',-1)->where('grupa_pr_id','!=',0)->get() as $gr){
			
		    $count = DB::select("SELECT DISTINCT COUNT(r.roba_id) FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' ELSE 1 = 1 END ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND r.grupa_pr_id = ".$gr->grupa_pr_id."")[0]->count;
		    
				if(Url_mod::slug_trans($gr->grupa) == $grupa && $count > 0){
					$grupa_pr_id = $gr->grupa_pr_id;
					break;
				}
			}

			$level2=Groups::vratiSveGrupe($grupa_pr_id);

			$check_grupa = "AND r.grupa_pr_id IN(".implode(",",$level2).")";
		}

		    $select="SELECT DISTINCT r.roba_id";
		    $roba=" FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' ELSE 1 = 1 END  ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."".$check_grupa."";

			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC")." LIMIT ".$limit." OFFSET ".$offset."");
				}

		$seo = Seo::page("akcija");
		$data=array(
			"strana"=>"akcija",
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords,
            "url"=>"akcija".(!is_null($grupa) ? "/".$grupa : ""),
			"grupa"=>isset($grupa) ? $grupa : '',
			"articles"=>$artikli,
			"limit"=>$limit,
			"count_products"=>$count,
			"filter_prikazi" => 0
		);
 		$data['sekcije'] = DB::table('stranica_sekcija')->select('sekcija_stranice.*','sekcija_stranice_tip.naziv as tip_naziv')->where('web_b2c_seo_id',34)->join('sekcija_stranice','sekcija_stranice.sekcija_stranice_id','=','stranica_sekcija.sekcija_stranice_id')->join('sekcija_stranice_tip','sekcija_stranice_tip.sekcija_stranice_tip_id','=','sekcija_stranice.sekcija_stranice_tip_id')->orderBy('stranica_sekcija.rbr','asc')->get();
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);		
	}
}