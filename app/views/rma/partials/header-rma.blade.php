<header id="admin-header" class="row">
	<!-- WIDTH TOGGLE BUTTON -->
	<div class="header-width-toggle"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>

	<div class="logo-wrapper">
		<a class="logo" href="{{ RmaOptions::base_url()}}admin" title="Selltico">
			<img src="{{ RmaOptions::base_url()}}/images/admin/logo-selltico-white.png" alt="Selltico">
		</a>
	</div>

	<!-- <span class="main-menu-toggler"></span> -->
	<nav class="main-menu">

		<ul class="clearfix">
			<li class="menu-item">
				<div class="">
					<div class="logged-user">Ulogovan: 
						@if($user = RmaOptions::user())
							{{ $user->naziv }}
						@endif
					</div>
				</div>
			</li>

			<li class="menu-item">
				<a href="{{RmaOptions::base_url()}}rma" class="menu-item__link  @if(in_array($strana,array('rma','radni_nalog_prijem','radni_nalog_opis_rada','radni_nalog_dokumenti','radni_nalog_predaja'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<span class="menu-tooltip">Radni nalozi</span>
					</div>
					<div class="menu-item__text">Radni nalozi</div>
				</a>
			</li>


			@if(RmaOptions::user('admin'))
			<li class="menu-item">
				<a href="{{RmaOptions::base_url()}}rma/operacije" class="menu-item__link  @if($strana == 'operacije') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-file-text" aria-hidden="true"></i>
						<span class="menu-tooltip">Operacija</span>
					</div>
					<div class="menu-item__text">Operacija</div>
				</a>
			</li>
			@endif

			<li class="menu-item">
				<a href="{{RmaOptions::base_url()}}rma/serviseri" class="menu-item__link  @if(in_array($strana,array('serviseri'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span class="menu-tooltip">Serviseri</span>
					</div>
					<div class="menu-item__text">Serviseri</div>
				</a>
			</li>
			
			@if(RmaOptions::user('admin'))
				@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI')) && Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')) && AdminOptions::is_shop())
				<li class="menu-item">
					<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/kupci_partneri/partneri" class="menu-item__link @if(in_array($strana,array('partneri'))) active @endif">
						<div class="menu-item__icon">
							<i class="fa fa-users" aria-hidden="true"></i>
							<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Partneri') }}</span>
						</div>
						<div class="menu-item__text">{{ AdminLanguage::transAdmin('Partneri') }}</div>
					</a>
				</li>
				<li class="menu-item">
					<a href="{{ AdminOptions::base_url()}}admin/kupci_partneri/kupci" class="menu-item__link @if(in_array($strana,array('kupci'))) active @endif">
						<div class="menu-item__icon">
							<i class="fa fa-users" aria-hidden="true"></i>
							<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Kupci') }}</span>
						</div>
						<div class="menu-item__text">{{ AdminLanguage::transAdmin('Kupci') }}</div>
					</a>
				</li>
				@endif
				@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')) && AdminOptions::is_shop())
				<li class="menu-item">
					<a href="{{ AdminOptions::base_url()}}admin/artikli/0/0/0/0/0/0/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-1-nn-nn-nn-nn-nn-nn/0/0/nn-nn" class="menu-item__link  @if($strana == 'artikli') active @endif">
						<div class="menu-item__icon">
							<i class="fa fa-cubes" aria-hidden="true"></i>
							<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Artikli') }}</span>
						</div>
						<div class="menu-item__text">{{ AdminLanguage::transAdmin('Artikli') }}</div>
					</a>
				</li>
				@endif
				@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')) AND AdminOptions::is_shop())
				<li class="menu-item">
					<a href="{{ AdminOptions::base_url()}}admin/grupe/0" class="menu-item__link  @if(in_array($strana,array('grupe','proizvodjac','mesto','tip_artikla','jedinica_mere','poreske_stope','stanje_artikla','status_narudzbine','kurirska_sluzba','konfigurator','osobine','nacin_placanja','nacin_isporuke','troskovi_isporuke','kurs'))) active @endif">
						<div class="menu-item__icon">
							<i class="fa fa-industry" aria-hidden="true"></i>
							<span class="menu-tooltip">Šifarnici</span>
						</div>
						<div class="menu-item__text">Šifarnici</div>
					</a>
				</li>
				@endif
			@endif

		</ul>
	</nav>

	<div class="logout-wrapper">
		<div class="menu-item">
			<a href="{{ RmaOptions::base_url()}}rma/logout" class="menu-item__link">
				<div class="menu-item__icon">
					<i class="fa fa-sign-out" aria-hidden="true"></i>
					<span class="menu-tooltip">Odjavi se</span>
				</div>
				<div class="menu-item__text">Odjavi se</div>
			</a>
		</div>
		<p class="footnote">TiCo &copy; {{ date('Y') }} - All rights reserved</p>
	</div>
</header>

