@foreach(B2bBasket::cartItems() as $row)
<?php $rabatCena = B2bArticle::b2bRabatCene($row->roba_id); ?>

<div class="row mini-cart-item"> 

    <div class="col-xs-3"> 
        <img class="mini-cart-img img-responsive" src="{{ B2bOptions::base_url() }}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bUrl::slugify(B2bArticle::seo_title($row->roba_id)) }}" />
    </div>

    <div class="col-xs-9 no-padding"> 
        <a class="mini-cart-title inline-block lineh" href="{{ B2bOptions::base_url() }}b2b/artikal/{{ B2bUrl::slugify(B2bArticle::seo_title($row->roba_id)) }}">{{B2bArticle::short_title($row->roba_id)}}</a>

        <div class="text-bold"> 
    	    <span>{{ round($row->kolicina) }}</span> x
    	    <span>{{ B2bBasket::cena($rabatCena->ukupna_cena) }}</span>
        </div>

        <a href="javascript:deleteItemCart({{ $row->web_b2b_korpa_stavka_id }})" class="remove-cart-item"><i class="fas fa-times"></i></a> 
    </div>
</div>
@endforeach

<?php  $cartTotal = B2bBasket::total();  ?>

<ul class="mini-cart-sum">
    @if($cartTotal->cena_sa_rabatom > 0)
    <li>Osnovica: <i>{{ B2bBasket::cena($cartTotal->cena_sa_rabatom) }}</i></li>
    <li>PDV: <i>{{ B2bBasket::cena($cartTotal->porez_cena) }}</i></li>
    <li class="text-bold">Ukupno: <i>{{ B2bBasket::cena($cartTotal->ukupna_cena) }}</i></li>
    @endif
</ul>
 
