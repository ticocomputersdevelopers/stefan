<div id="main-content" >
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi proizvođača') }}</h3>
				<div class="manufacturer"> 
					<select class="admin-select JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/proizvodjac/0">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
						@foreach(AdminSupport::getProizvodjaci() as $row)
						<option value="{{ AdminOptions::base_url() }}admin/proizvodjac/{{ $row->proizvodjac_id }}" @if($row->proizvodjac_id == $proizvodjac_id) {{ 'selected' }} @endif >{{ $row->naziv }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<!-- ====================== -->
			<div class="text-center">
				<a href="#" class="video-manual" data-reveal-id="manufacturer-manual">{{ AdminLanguage::transAdmin('Uputstvo') }} <i class="fa fa-film"></i></a>
				<div id="manufacturer-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
					<div class="video-manual-container"> 
						<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Proizvođači') }}</span></p>
						<iframe src="https://player.vimeo.com/video/271252069" width="840" height="425" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				</div>
			</div>
			<!-- ====================== -->
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1> 

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/proizvodjac-edit" enctype="multipart/form-data">
					<input type="hidden" name="proizvodjac_id" value="{{ $proizvodjac_id }}"> 
					<input type="hidden" name="jezik_id" value="{{ $jezik_id }}"> 

					<div class="row">
						<div class=" columns medium-7 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv proizvođača') }}</label>
							<input type="text" name="naziv" data-id="" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>

						<div class="columns medium-2">
							<label for="brend_prikazi">{{ AdminLanguage::transAdmin('Brend') }}</label>
							<select name="brend_prikazi" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if(Input::old('brend_prikazi') ? Input::old('brend_prikazi') : $brend_prikazi)
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
								@endif
							</select>
						</div>
						<div class="columns medium-3 {{ $errors->first('rbr') ? ' error' : '' }}">
							<label for="rbr">{{ AdminLanguage::transAdmin('Redni broj') }}</label>
							<input type="text" name="rbr" data-id="" value="{{ Input::old('rbr') ? Input::old('rbr') : $rbr }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div> 
					</div>

					@if(AdminOptions::gnrl_options(3039) == 1)
					<div class="row"> 
						<div class="columns medium-4 field-group">
							<label for="produzena_garancija">{{ AdminLanguage::transAdmin('Produžena garancija') }}</label>
							<select name="produzena_garancija" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if(Input::old('produzena_garancija') ? Input::old('produzena_garancija') : $produzena_garancija)
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
								@endif
							</select>
						</div>
					</div>
					@endif

					<div class="row"> 
						<div class="columns medium-6 manufacturers-img"> 
							<img <?php if(isset($slika)){ ?> src="{{ AdminOptions::base_url().$slika }}" <?php } ?>  />
							@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
							<div class="field-group">
								<label>{{ AdminLanguage::transAdmin('Preporučene dimenzije slike') }}: 140x60</label>
								<div class="bg-image-file"> 
									<input type="file" name="slika">
								</div>
								@if(isset($slika))
								<input type="checkbox" name="slika_delete">{{ AdminLanguage::transAdmin('Obriši sliku') }}
								@endif
							</div>
							@endif
						</div>

						@if(AdminOptions::gnrl_options(3039) == 1)
						<div class="columns medium-6">
							@if(!is_null($produzena_garancija_slika) AND $produzena_garancija_slika != '')
							<img src="{{ AdminOptions::base_url().$produzena_garancija_slika }}" />
							@endif
							@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
							<label for="produzena_garancija_slika">{{ AdminLanguage::transAdmin('Produžena garancija slika') }}</label>
							<input type="file" name="produzena_garancija_slika" > 
							@endif

							<label for="produzena_garancija_link">{{ AdminLanguage::transAdmin('Produžena garancija link') }}</label>
							<input class="no-margin" type="text" name="produzena_garancija_link" data-id="" value="{{ Input::old('produzena_garancija_link') ? Input::old('produzena_garancija_link') : $produzena_garancija_link }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						@endif
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					@if($proizvodjac_id != 0 && count($jezici) > 1) 
					<div class="languages">
						<ul>	
							@foreach($jezici as $jezik)
							<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/proizvodjac/{{$proizvodjac_id}}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
							@endforeach
						</ul>
					</div> 
					@endif
					@endif

					<div class="row"> 
						<div class="columns">
							<h3 class="center seo-info tooltipz" aria-label="{{ AdminLanguage::transAdmin('SEO (Search Engine Optimization) polja je potrebno popuniti jednostavnim jezikom, kako bi Vas kupci lakše pronašli na internetu - ovi opisi nisu vidljivi direktno u prodavnici, već samo kako bi Google prepoznao Vaš proizvod i povezao Vas sa ljudima koji ga traže') }}.">{{ AdminLanguage::transAdmin('SEO') }}</h3>


							<div class="{{ $errors->first('seo_title') ? ' error' : '' }}">
								<label>{{ AdminLanguage::transAdmin('Naslov (title) do 60 karaktera') }}</label>
								<input type="text" name="seo_title" data-id="" value="{{ htmlentities(Input::old('seo_title') ? Input::old('seo_title') : $seo_title) }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
							</div>

							<div class="{{ $errors->first('description') ? ' error' : '' }}">
								<label id="seo_opis_label" for="description">{{ AdminLanguage::transAdmin('Opis (description) do 158 karaktera') }}</label>
								<textarea id="seo_opis" name="description" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('description') ? Input::old('description') : $description }}</textarea>
							</div>

							<div class="{{ $errors->first('keywords') ? ' error' : '' }}">
								<label for="keywords">{{ AdminLanguage::transAdmin('Ključne reči (keywords)') }}</label>
								<input type="text" name="keywords" value="{{ Input::old('keywords') ? Input::old('keywords') : $keywords }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>  
							</div>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						@if($proizvodjac_id != 0)
						<a class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/proizvodjac-delete/{{ $proizvodjac_id }}" href="#">{{ AdminLanguage::transAdmin('Obriši') }}</a>
						@endif
					</div> 
					@endif
				</form>
			</div>
		</section>
	</div> 
</div>  
