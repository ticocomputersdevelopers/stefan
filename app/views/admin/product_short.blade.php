<div id="main-content" class="article-edit">
	@if(Session::has('message_success'))
	<script>
		alertify.success('{{ Session::get('message_success') }}');
	</script>
	@endif
	@if(Session::has('message_delete'))
	<script>
		alertify.success('{{ Session::get('message_delete') }}');
	</script>
	@endif

	<?php
	$old = count(Input::old()) ? 1 : 0;
	// echo '<pre>';
	// var_dump(Input::old()); die();
	?>

	<div class="row">
		@if(Session::has('message'))
		<div class="column medium-12 erase-div"> 
			<div class="text-red">{{ AdminLanguage::transAdmin('Artikal ima istoriju') }}!</div> 
			<a class="btn btn-danger" href="{{ AdminOptions::base_url() }}admin/product-delete/{{ $roba_id }}/1">{{ AdminLanguage::transAdmin('Ipak obriši') }}!</a>
		</div>
		@endif
		@if(Session::has('limit_message'))
		<div class="column medium-12 erase-div"> 
			<div class="text-red">{{ AdminLanguage::transAdmin('Unošenje novih artikala nije dozvoljeno') }}!</div>
		</div>
		@endif
		<form method="POST" action="{{AdminOptions::base_url()}}admin/product-edit-short" enctype="multipart/form-data" id="JSArticleForm">
			<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
			<input type="hidden" name="clone_id" value="{{ $clone_id ? $clone_id : 0 }}">
			<input type="hidden" name="orgj_id" value="{{ $orgj_id }}">
			<input type="hidden" name="jedinica_mere_id" value="{{ $jedinica_mere_id }}">
			<input type="hidden" name="tarifna_grupa_id" value="{{ $tarifna_grupa_id }}">

			<div class="clearfix floating-field-group">

				<div class="columns medium-10 cut-padding medium-centered">
					<div class="row flat-box padding-v-8">
						<div class="columns medium-10">
							<div class="naziv-web-big ">
								<label>{{ AdminLanguage::transAdmin('Naziv na web-u') }}</label>
								<input type="text" name="naziv_web" value="{{ htmlentities($old ? Input::old('naziv_web') : $naziv_web) }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('naziv_web') }}</div>
							</div>
						</div>
						<div class="columns medium-2 no-padd">
							<label class="hidden-for-small-only">&nbsp;</label>
							@if($roba_id!=0)
							<a href="{{AdminArticles::article_link($roba_id)}}" target="_blank" class="btn btn-secondary btn-small">{{ AdminLanguage::transAdmin('Vidi artikal') }} </a>
							@endif
						</div>
					</div>

					<div class="flat-box padding-v-8">
						<div class="row"> 
							<div class="columns medium-2 small-6"> 
								<label>{{ AdminLanguage::transAdmin('SKU') }}</label>
								<input type="text" name="sku" value="{{ htmlentities($old ? Input::old('sku') : $sku) }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('sku') }}</div> 
							</div>
							<div class="columns medium-5 small-6"> 
								<label>{{ AdminLanguage::transAdmin('Grupa') }}</label>
								<div class="custom-select-arrow"> 
									<input type="text" name="grupa_pr_grupa" value="{{Input::old('grupa_pr_grupa') ? Input::old('grupa_pr_grupa') : $grupa_pr_grupa }}" autocomplete="off" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								</div>
								<div class="short-group-container"> 
									{{ AdminSupport::listGroups() }}
								</div>
								<div class="error">{{ $errors->first('grupa_pr_grupa') }}</div> 
							</div>
							<div class="columns medium-3 small-6"> 
								<label>{{ AdminLanguage::transAdmin('Stanje artikla') }}</label>
								<div class="custom-select-arrow"> 
									<input type="text" name="roba_flag_cene" value="{{Input::old('roba_flag_cene') ? Input::old('roba_flag_cene') : $roba_flag_cene }}" autocomplete="off" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								</div>
								<div class="short-group-container"> 
									<ul id="JSListaFlagCene" hidden="hidden">
										@foreach(AdminSupport::getFlagCene() as $row)
										<li class="JSListaFlagCena" data-roba_flag_cene="{{ $row->naziv }}">{{ $row->naziv }}</li>
										@endforeach
									</ul>
								</div>
								<div class="error">{{ $errors->first('roba_flag_cene') }}</div> 
							</div>
							<div class="columns medium-2 small-6"> 
								<label>{{ AdminLanguage::transAdmin('Količina') }}</label>
								<input type="text" name="kolicina" value="{{ $old ? Input::old('kolicina') : $kolicina }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('kolicina') }}</div> 
							</div>
						</div>
					</div>

					<div class="flat-box padding-v-8">
						<div class="row">
							<div class="columns medium-2">		 
								<label>{{ AdminLanguage::transAdmin('Na Web-u') }} </label>
								<select name="flag_prikazi_u_cenovniku" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									@if($old)
									@if(Input::old('flag_prikazi_u_cenovniku'))
									<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
									<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
									@else
									@if($flag_prikazi_u_cenovniku)
									<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
									<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
									@endif
								</select> 
							</div>

							<div class="columns medium-2 small-6"> 
								<label>{{ AdminLanguage::transAdmin('Akcija') }}</label>
								<select name="akcija_flag_primeni" id="JSAkcijaFlagPrimeni" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									@if($old)
									@if(Input::old('akcija_flag_primeni'))
									<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
									<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
									@else
									@if($akcija_flag_primeni)
									<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
									<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
									@endif
								</select> 
							</div>

							<div class="columns medium-4 small-6"> 
								<label>{{ AdminLanguage::transAdmin('Tip artikla') }}</label>
								<div class="custom-select-arrow"> 
									<input type="text" name="tip_artikla" value="{{Input::old('tip_artikla') ? Input::old('tip_artikla') : $tip_artikla }}" autocomplete="off" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								</div>
								<div class="short-group-container"> 
									<ul id="JSListaTipaArtikla" hidden="hidden">
										<li class="JSListaTipArtikla" data-tip_artikla="">{{ AdminLanguage::transAdmin('Bez tipa') }}</li>
										@foreach(AdminSupport::getTipovi() as $row)
										<li class="JSListaTipArtikla" data-tip_artikla="{{ $row->naziv }}">{{ $row->naziv }}</li>
										@endforeach
									</ul>
								</div>
								<div class="error">{{ $errors->first('tip_artikla') }}</div> 
							</div>

							<div class="columns medium-2 small-6"> 
								<label>{{ AdminLanguage::transAdmin('Web cena') }}</label>
								<input type="text" name="web_cena" class="JSCenaChange" value="{{ $old ? Input::old('web_cena') : $web_cena }}" {{ AdminArticles::vrsteCena('WEB') ? '' : 'disabled' }} {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('web_cena') }}</div> 
							</div>

							<div class="columns medium-2 small-6">
								<div class="{{ $errors->first('akcijska_cena') ? 'error' : '' }}">
									<label>{{ AdminLanguage::transAdmin('Akcijska cena') }}</label>
									<input type="text" name="akcijska_cena" id="JSAkcijskaCena" value="{{ $old ? Input::old('akcijska_cena') : $akcijska_cena }}" {{ AdminArticles::vrsteCena('NC') ? '' : 'disabled' }} {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								</div>
							</div>

							<div class="columns medium-12 small-11 no-padd">
								<div class="short-product-action" id="JSAkcijaDatum" {{ (Input::old('akcija_flag_primeni') ? Input::old('akcija_flag_primeni') : $akcija_flag_primeni) == 0 ? 'hidden' : '' }}>
									<div class="column medium-2 small-6">
										<label>{{ AdminLanguage::transAdmin('Datum od') }}</label>
										<input class="akcija-input" id="datum_akcije_od" name="datum_akcije_od" autocomplete="off" type="text" value="{{ Input::old('datum_akcije_od') ? Input::old('datum_akcije_od') : $datum_akcije_od }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
										@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
										<span id="datum_od_delete">&times;</span>
										@endif
									</div>
									<div class="column medium-2 small-6">
										<label>{{ AdminLanguage::transAdmin('Datum do') }}</label>
										<input class="akcija-input" id="datum_akcije_do" name="datum_akcije_do" autocomplete="off" type="text" value="{{ Input::old('datum_akcije_do') ? Input::old('datum_akcije_do') : $datum_akcije_do }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
										@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
										<span id="datum_do_delete">&times;</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="flat-box padding-v-8">
						<div class="row">
							<div class="column medium-12">
								<h3 class="title-med">{{ AdminLanguage::transAdmin('Slike') }}</h3>
							</div>
						@if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)    
							<div class="column medium-2 col-slika">
								<div class="col-slika-ads"> 
									<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
								</div>
							</div> 
							@if(Product::pitchPrintImageExist($roba_id) == TRUE) 
							<div class="column medium-2 col-slika">
								<div class="col-slika-ads"> 
									<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766">
								</div>
							</div>                         
							@endif
                        @else
							@foreach(AdminArticles::getSlike($roba_id) as $row)
							<div class="column medium-2 col-slika">
								<div class="col-slika-ads"> 
									<div class="radio-button-field">
										@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
										<span class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Glavna slika') }}">
											@endif
											<input type="radio" name="akcija" class="akcija" value="{{ $row->web_slika_id }}" @if($row->akcija) checked @endif {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}></span>
											@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))	
											<a data-link="{{AdminOptions::base_url()}}admin/slika-delete/{{ $roba_id }}/{{$row->web_slika_id}}/true" class="tooltipz right JSSlikaDeleteShort" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}"> 
												&times;
											</a> 
											@endif
										</div>
										<img src="{{ AdminOptions::base_url().$row->putanja }}">
									</div>
								</div>
								@endforeach
						@endif
								<div id="JSUploadImages" class="column medium-12 short-article-img">				 
									<div class=" {{ $errors->first('slike') ? 'error' : '' }}">
										@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
										<div class="bg-image-file"> 
											<input type="file" name="slike[]" multiple>
										</div>
										@endif
									</div>
									<div class="error">{{ $errors->first('slike') ? $errors->first('slike') : '' }}</div>			 
								</div>
							</div>
						</div>

						<div class="flat-box padding-v-8">
							<div class="row">
								
								<div class="columns medium-12"> 
									<h3 class="title-med">{{ AdminLanguage::transAdmin('Opis') }}</h3>
									<input type="hidden" id="roba_id" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
									<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif id="mc" name="web_opis" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('web_opis') ? Input::old('web_opis') : $web_opis }}</textarea>
								</div>
							</div>
						</div>

						<div class="flat-box"> 
							<div class="btn-container center">
								@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
								@if(isset($clone_id))
								<input type="checkbox" name="slike_clone" checked>{{ AdminLanguage::transAdmin('Kloniraj slike') }}
								@endif
								<button type="submit" id="JSArticleSubmit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button> 

								@if($roba_id != 0)
								<a class="Deleting1 btn btn-danger" >Obriši</a>
								@if(Admin_model::check_admin(array(10002)))
								<a class="btn-secondary btn" href="{{AdminOptions::base_url()}}admin/product/0/{{ $roba_id }}">{{ AdminLanguage::transAdmin('Kloniraj artikal') }}</a>
								@endif
								@endif

								<a href="#" id="JSArtikalRefresh" class="btn btn-primary">{{ AdminLanguage::transAdmin('Poništi izmene') }}</a>
								@endif
								<a href="{{AdminOptions::base_url()}}admin/product/{{$roba_id}}" class="btn btn-primary">{{ AdminLanguage::transAdmin('Detaljnija izmena') }} </a>
							</div> 
						</div>
					</div> 
				</div>
			</form>
		</div>  
	</div>



