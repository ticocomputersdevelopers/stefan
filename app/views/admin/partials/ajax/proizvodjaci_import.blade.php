	<option selected>{{ AdminLanguage::transAdmin('Izaberite proizvođača') }}</option>
	<option value="-1">{{ AdminLanguage::transAdmin('Nedefinisan') }}</option>
@foreach(AdminSupport::getProizvodjaci() as $proizvodjac)
	<option value="{{ $proizvodjac->proizvodjac_id }}">{{ $proizvodjac->naziv }}</option>
@endforeach