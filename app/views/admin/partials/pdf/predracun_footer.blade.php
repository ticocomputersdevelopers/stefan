<div class="row"> 
	<div class="napomena">
		<p>{{ AdminLanguage::transAdmin('Kod plaćanja - poziv na broj') }}: {{AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta')}}</p>
		<!-- <p><strong>{{ AdminLanguage::transAdmin('Napomena o poreskom oslobađanju') }}:</strong></p> -->
		<p><strong>{{ AdminLanguage::transAdmin('Napomena') }}:</strong></p>

		<p>{{substr(AdminNarudzbine::find($web_b2c_narudzbina_id,'napomena'),0,220)}}</p>
	</div>
</div>

<br>

<div class="row"> 
	<table class="signature">
		<tr>
			<td class=""></td>
			<td class=""></td>
			
			<!-- <td class="text-right"><span class="robu_izdao"></span> <br>  ___________________________  <br> {{ AdminLanguage::transAdmin('Predračun izdao') }}</td> -->
			<td class="text-right" style="border-top: 1px solid #666;"><span class="robu_izdao" style=""></span> {{ AdminLanguage::transAdmin('Predračun izdao') }}</td>

		</tr>
		
		<tr>
			<td style="width: 60%;">
				<p style="border-top: 1px solid #666; font-size:12px; position: absolute; top: -30px"> {{ AdminLanguage::transAdmin('Predračun je izrađen na računaru i punovažan je bez pečata i potipisa') }}</p>
			</td>
			<td class=""></td>
		</tr>
	</table>
</div>