<section class="grupe-page" id="main-content">
	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif
	@include('admin/partials/tabs')

	<div class="row">
		<section class="small-12 medium-12 large-3 columns">
			@include('admin/partials/grupe')
		</section>
		<section class="small-12 medium-12 {{ $grupa_pr_id==0 ? 'large-4 large-centered' : 'large-3' }}  columns ">
			<div class="flat-box">
				<h3 class="title-med">{{ $title }}</h3>
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/grupa-edit" enctype="multipart/form-data">
				    <input type="hidden" name="grupa_pr_id" value="{{ $grupa_pr_id }}" />
					<input type="hidden" name="karak_id" value="{{ $karak_id }}" />
					<input type="hidden" name="jezik_id" value="{{ $jezik_id }}" />

					<div class="row">
						<div class="columns medium-12 field-group{{ $errors->first('grupa') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Naziv grupe') }}</label>
							<input type="text" name="grupa" data-id="" value="{{ htmlentities(Input::old('grupa') ? Input::old('grupa') : $grupa) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }} />
						</div>
					</div>

					<div class="row">
						<div class="columns medium-12">
							<label for="">{{ AdminLanguage::transAdmin('Šifra') }} {{ $sifra }}</label>
						</div>
					</div>

					<div class="row"> 
						<div id="JSParent" class="columns medium-12 field-group">
							<label for="">{{ AdminLanguage::transAdmin('Glavna grupa') }}</label>
							<select name="parrent_grupa_pr_id" class="admin-select grupa_select" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminSupport::selectParentGroups((Input::old('parrent_grupa_pr_id') ? Input::old('parrent_grupa_pr_id') : $parrent_grupa_pr_id), $grupa_pr_id) }}
							</select>
						</div>
					</div>
					<div class="row">
						@if(AdminOptions::checkB2C())
						<div class="columns medium-6 small-6 field-group">

							<label>{{ AdminLanguage::transAdmin('B2C aktivna') }}</label>
							<select name="web_b2c_prikazi" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if(Input::old('web_b2c_prikazi'))
									@if(Input::old('web_b2c_prikazi'))
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
										<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
								@else
									@if($web_b2c_prikazi)
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
										<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
								@endif
							</select>
						</div>
						@endif
						@if(AdminOptions::checkB2B())
						<div class="columns medium-6 small-6 field-group">
							<label>{{ AdminLanguage::transAdmin('B2B aktivna') }}</label>
							<select name="web_b2b_prikazi" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if(Input::old('web_b2b_prikazi'))
									@if(Input::old('web_b2b_prikazi'))
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
										<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
								@else
									@if($web_b2b_prikazi)
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
										<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
								@endif
 							</select>
						</div>
						@endif
					</div>
					<div class="row">
						<div class="field-group columns medium-12">
							<label>{{ AdminLanguage::transAdmin('Kopiraj karakteristike iz') }}:</label>
							<select name="premestanje_karakteristika" class="grupa_select" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{AdminSupport::selectGroups()}}
							</select>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
						@if($grupa_pr_id != 0 && count($jezici) > 1)
						<div class="row">
							<div class="languages">
								<ul>	
								@foreach($jezici as $jezik)
									<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/grupe/{{ $grupa_pr_id }}/{{ $karak_id ? $karak_id : 0 }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
								@endforeach
								</ul>
							</div>
						 </div>
						@endif
					@endif

					<h3 class="center seo-info tooltipz" aria-label="{{ AdminLanguage::transAdmin('SEO (Search Engine Optimization) polja je potrebno popuniti jednostavnim jezikom, kako bi Vas kupci lakše pronašli na internetu - ovi opisi nisu vidljivi direktno u prodavnici, već samo kako bi Google prepoznao Vaš proizvod i povezao Vas sa ljudima koji ga traže') }}.">{{ AdminLanguage::transAdmin('SEO') }}</h3>

					<div class="row">
						<div class="columns medium-12 field-group{{ $errors->first('seo_title') ? ' error' : '' }}">
							<label class="no-margin">{{ AdminLanguage::transAdmin('Naslov (title) do 60 karaktera') }}</label>
							<input type="text" name="seo_title" data-id="" value="{{ htmlentities(Input::old('seo_title') ? Input::old('seo_title') : $seo_title) }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }} />
						</div>
					</div>

 					<div class="row">
						<div class="columns medium-12 field-group{{ $errors->first('seo_description') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Opis (description) do 158 karaktera') }} </label>
							<textarea id="seo_opis" name="seo_description" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('opis') ? Input::old('opis') : $seo_description }}</textarea>
						</div>
					</div>

					<div class="row">
						<div class="columns medium-12 field-group{{ $errors->first('keywords') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Ključne reči (keywords)') }}</label>
							<input type="text" id="seo_keywords" name="seo_keywords" maxlength="155" value="{{ Input::old('keywords') ? Input::old('keywords') : $seo_keywords }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>
					
					<div class="row">
						<div class="columns medium-12 field-group{{ $errors->first('sablon_opis') ? ' error' : '' }}">
							<label id="sablon" for="">{{ AdminLanguage::transAdmin('Šablon') }}</label>
							<textarea id="JSsablon" @if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) class="special-textareas" @endif name="sablon_opis" maxlength="155" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('sablon_opis') ? Input::old('sablon_opis') : $sablon_opis }}</textarea>
						</div>
					</div>
					@if(empty(Input::old('parrent_grupa_pr_id') ? Input::old('parrent_grupa_pr_id') : $parrent_grupa_pr_id))

					<div class="row">
						<div class="field-group columns medium-12">
							<label>{{ AdminLanguage::transAdmin('Baner') }}:</label>
							<select name="baner_id" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="0">{{ AdminLanguage::transAdmin('Izaberite baner') }}</option>
								@foreach(AdminStranice::sliders(['Kategorija Baner']) as $baner)
								<option value="{{ $baner->slajder_id }}" {{($baner_id == $baner->slajder_id) ? 'selected' : ''}}>{{ $baner->naziv }}</option>
								@endforeach
							</select>
						</div>
					</div>
					@endif
					<div class="row"> 
						@if(isset($putanja_slika))
							<img src="{{ AdminOptions::base_url().$putanja_slika }}" class="group-image" />
						@endif
						@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
							@if(isset($putanja_slika))
								<input type="checkbox" name="slika_delete">{{ AdminLanguage::transAdmin('Obriši sliku') }}
							@endif
						  
							<div class="columns medium-12 field-group">
								<label>{{ AdminLanguage::transAdmin('Preporučene dimenzije slike') }}: 40x40</label>
								<div class="bg-image-file"> 
									<input type="file" name="slika">
								</div>
							</div>
						@endif
				  	</div>
				   	@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
				   		@if($parrent_grupa_pr_id==0 )
							<div class="row">
								<div class="columns medium-12 field-group">
									<label>{{ AdminLanguage::transAdmin('Preporučene dimenzije pozadinske slike') }}: 720x470</label>
									<div class="bg-image-file"> 
										<input type="file" name="pozadinska_slika">
									</div>
								</div>
								@if(isset($pozadinska_slika))
									<input type="checkbox" name="pozadinska_slika_delete">{{ AdminLanguage::transAdmin('Obriši sliku') }}
								@endif
						
								@if(isset($pozadinska_slika))
									<img src="{{ AdminOptions::base_url().$pozadinska_slika }}" class="group-image" />
								@endif
				   			</div>
				   		@endif
				   	@endif

				  	@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
				    	<div class="row">
							<div class="btn-container center">
								<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
								@if($grupa_pr_id != 0)
									<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/grupa-delete/{{ $grupa_pr_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
								@endif
							</div>
					 	</div>
					@endif
				</form>
			</div>
		 
		</section>
		@if($grupa_pr_id != 0)
			<section class="small-12 medium-12 large-3 columns">
				<div class="flat-box">
							
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Karakteristike') }} 	 
						@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
						 - <a href="#" class="btn btn-primary btn-small" id="nova-kar">Dodaj</a>
						@endif
					</h3>
					<div class="row nova-karak">
						<div class="columns medium-7 small-7">{{ AdminLanguage::transAdmin('Naziv') }}</div>
						<div class="columns medium-5 small-5">{{ AdminLanguage::transAdmin('Aktivna') }}</div>
					</div>

					<ul class="banner-list name-ul" @if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) id="JSListChar" @endif>
						<li id='0' class="nova-karak_li name-ul__li">
							<input class="JSCharNew naziv name-ul__li__name input-smaller" name="naziv" type="text" value="" data-id="{{ $grupa_pr_id }}" autocomplete="off" > 		
							<!-- 						
							<button class="sacuvaj-novu-karak name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" data-id="{{ $grupa_pr_id }}">
								<i class="fa fa-floppy-o" aria-hidden="true"></i></button> -->
						</li>
					
						@foreach($nazivi as $row)
							<li class="name-ul__li @if($row->grupa_pr_naziv_id == $karak_id) active @endif" id="{{ $row->grupa_pr_naziv_id }}" >
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<i class="fa fa-arrows-v sortableKar tooltipz" aria-label="{{ AdminLanguage::transAdmin('Prevucite da bi sortirali') }}"></i>
								@endif						
								<input class="JSChar naziv name-ul__li__name input-smaller" name="naziv" type="text" value="{{ htmlentities($row->naziv) }}" data-id="{{ $row->grupa_pr_naziv_id }}" autocomplete="off" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
								
								<input {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }} class="activeKarak name-ul__li__checkbox button-option" data-id="{{ $row->grupa_pr_naziv_id }}" type="checkbox" @if($row->active == 1) checked @endif>
								 
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<button title="Obriši" class="obrisi-karak name-ul__li__remove button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-id="{{ $row->grupa_pr_naziv_id }}"><i class="fa fa-times" aria-hidden="true"></i></button>
								@endif
								
								<a href="{{ AdminOptions::base_url() }}admin/grupe/{{ $row->grupa_pr_id }}/{{ $row->grupa_pr_naziv_id }}" class="name-ul__li__right-arrow button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodaj vrednost') }}">
									<i class="fa fa-arrow-right" aria-hidden="true"></i>
								</a>
							</li>
						@endforeach
					</ul>
	 			</div> <!-- end of .flat-box -->	
			</section>
		@endif

		@if(isset($karak_id) and $karak_id != 0)
			<section class="small-12 medium-12 large-3 columns">
				<div class="flat-box">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Vrednosti') }}
						@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
							 - <a href="#" class="btn btn-primary btn-small" id="nova-vred">{{ AdminLanguage::transAdmin('Dodaj vrednost') }}</a>
						@endif
					</h3>
					<div class="row nova-karak"> 
						<div class="columns medium-8 small-8">{{ AdminLanguage::transAdmin('Vrednost') }}</div> 
						<div class="columns medium-4 small-4">{{ AdminLanguage::transAdmin('Aktivna') }}</div> 
					</div>
					<ul class="banner-list name-ul row" @if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) id="JSListValues" @endif>

						<li id='0' class="nova-vred_li name-ul__li">
							<input class="JSCharNewValue nova-vred name-ul__li__name input-smaller" name="nova-vred" type="text" value="" placeholder="{{ AdminLanguage::transAdmin('Naziv') }}" data-id="{{ $karak_id }}" autofocus>
							<!-- <button class="sacuvaj-novu-vred name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" ><i class="fa fa-floppy-o" aria-hidden="true"></i></button> -->

						</li>

						@foreach($vrednost as $row)
							<li class="prca vrednost-li name-ul__li" id="{{ $row->grupa_pr_vrednost_id }}">
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<i class="fa fa-arrows-v sortableKar tooltipz" aria-label="{{ AdminLanguage::transAdmin('Prevucite da bi sortirali') }}"></i>
								@endif
								<input class="JSCharValue vrednost name-ul__li__name input-smaller" name="vrednost" type="text" value="{{ htmlentities($row->naziv) }}" data-id="{{ $row->grupa_pr_vrednost_id }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<input {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }} class="activeVred name-ul__li__checkbox button-option" data-id="{{ $row->grupa_pr_vrednost_id }}" type="checkbox" @if($row->active == 1) checked @endif>						
								<!-- <input class="vrednost_rbr name-ul__li__number" name="vrednost_rbr" type="hidden" value="{{ $row->rbr }}"> -->
								<!-- <button class="sacuvaj-vrednost name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" ><i class="fa fa-floppy-o" aria-hidden="true"></i></button> -->
								
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<button  class="obrisi-vred name-ul__li__save button-option name-ul__li__remove tooltipz" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-id="{{ $row->grupa_pr_vrednost_id }}"><i class="fa fa-times" aria-hidden="true"></i></button>
								@endif
								
							</li>							
						@endforeach
					</ul>
				</div> <!-- end of .flat-box -->
			</section>
		@endif	
	</div>
  <!-- </form> -->
</section>