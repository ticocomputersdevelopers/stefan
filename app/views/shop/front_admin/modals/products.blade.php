<div class="centered-modal"> 
<div class="modal fade" id="FAProductsModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content fast-edit-articles modal-scroller">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </button>

                <div class="modal-heading">
                    <h4 class="modal-title text-center">{{ Language::trans('Artikli') }}</h4>
                </div>
            </div>

            <div class="modal-body">
                <div class="row choose-all"> 
                    <div class="col-md-3 col-sm-3 col-xs-12 text-center">{{ Language::trans('Ukupno') }}: 
                        <span id="JSFAProductListCount"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" placeholder="{{Language::trans('Pretraži')}}..." id="JSFAArticleSearch" class="form-control search">
                         <span class="fas fa-search form-control-feedback"></span>
                    </div>
                    @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                        <div id="JSFAProductNewModalCall" class="add-new-article col-md-3 col-sm-3 col-xs-12 text-center">
                            <button class="btn btn-primary">{{ Language::trans('Dodaj novi artikal') }}</button>
                        </div>
                    @endif
                </div>
                <div class="fast-filters">
                    <div class="first_1"> 
                        <label>{{Language::trans('Grupe')}}:</label>
                        <select id="JSFAGrupaSelect" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>           
                            {{ AdminSupport::selectGroups() }}
                        </select>
                    </div>
                    <div class="first_2">
                        <label>{{ Language::trans('Na Webu') }}:</label>
                        <select id="JSFANaWebuSelect" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>           
                            <option value="null">{{ Language::trans('Svi') }}</option>
                            <option value="1">{{ Language::trans('Da') }}</option>
                            <option value="0">{{ Language::trans('Ne') }}</option>
                        </select>
                    </div>
                    <div class="first_2">
                        <label>{{ Language::trans('Na akciji') }}:</label>
                        <select id="JSFANaAkcijiSelect" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>           
                            <option value="null">{{ Language::trans('Svi') }}</option>
                            <option value="1">{{ Language::trans('Da') }}</option>
                            <option value="0">{{ Language::trans('Ne') }}</option>
                        </select>
                    </div>
                    <div class="first_2">
                        <label>{{ Language::trans('Slike') }}:</label>
                        <select id="JSFASlikeSelect" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>           
                            <option value="null">{{ Language::trans('Svi') }}</option>
                            <option value="1">{{ Language::trans('Da') }}</option>
                            <option value="0">{{ Language::trans('Ne') }}</option>
                        </select>
                    </div>
                    <div class="first_2">
                        <label>{{ Language::trans('Opis') }}:</label>
                        <select id="JSFAOpisSelect" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>           
                            <option value="null">{{ Language::trans('Svi') }}</option>
                            <option value="1">{{ Language::trans('Da') }}</option>
                            <option value="0">{{ Language::trans('Ne') }}</option>
                        </select>
                    </div>
                    <div class="first_6">
                        <label>{{ Language::trans('Komande') }}:</label>
                        <select id="JSFAActionSelect" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>           
                            <option value="web_on">{{ Language::trans('Stavi na Web') }}</option>
                            <option value="web_off">{{ Language::trans('Skini sa Weba') }}</option>
                            <option value="sell_on">{{ Language::trans('Stavi na Akciju') }}</option>
                            <option value="sell_off">{{ Language::trans('Skini sa Akcije') }}</option>
                            <option value="delete">{{ Language::trans('Obriši') }}</option>
                        </select>
                    </div>
                    @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                        <button class="btn" id="JSFAActionExecute">{{Language::trans('Primeni')}}</button>
                    @endif
                </div>
                <ul class="list-inline row custom-heading">
                    <li class="tooltipz" aria-label="Selektuj sve"> 
                        <input type="checkbox" id="JSRobaSelected">
                        <span id="JSFAProductListSelectCount"></span>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-5 text-center">&nbsp;{{Language::trans('Slika')}}</li>
                    <li class="col-md-2 col-sm-2 col-xs-3">{{Language::trans('Na Webu')}}</li>
                    <li class="col-md-2 col-sm-2 col-xs-3">{{Language::trans('Količina')}}</li>
                    <li class="col-md-6 col-sm-6 col-xs-2 hidden-xs text-center">{{Language::trans('Naziv')}}</li>
                </ul>
                <ul id="JSFAProductListContent" class="FAProductListContent">
                   <!-- product_list -->
                </ul> 
            </div>
        </div>   
    </div>
</div>
</div>
