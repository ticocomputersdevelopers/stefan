@extends('shop/themes/'.Support::theme_path().'templates/products')

@section('products_list')

<div class="product-options row flex">
			
    <div class="col-md-6 col-sm-6 col-xs-12 sm-text-center no-padding">
 
<!-- PER PAGE -->
		<!-- <span>{{ Language::trans('UKUPNO') }}: {{ $count_products }}</span> -->
        @if(Options::product_number()==1)
		<div class="dropdown inline-block">	 
        	<div class="dropdown-toggle flex" data-toggle="dropdown">
				 <button class="currency-btn" type="button">	
				 	<span class="options-label">{{ Language::trans('Prikaži') }}: </span>
				 	@if(Session::has('limit'))
					{{Session::get('limit')}}
					@else
					9
					@endif
	    		</button>
	            <div class="currency-btn-down flex justify-content-center"><i class="fas fa-chevron-down"></i></div>
        	</div>

			<ul class="dropdown-menu currency-list">			
				<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/9" rel="nofollow"><span class="options-label">{{ Language::trans('Prikaži') }}:</span> 9</a></li>
				<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/18" rel="nofollow"><span class="options-label">{{ Language::trans('Prikaži') }}:</span> 18</a></li>
				<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/27" rel="nofollow"><span class="options-label">{{ Language::trans('Prikaži') }}:</span> 27</a></li>			
			</ul>			 
		</div>
		@endif
   
 		@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
			<!-- MODAL TRIGGER BUTTON -->
			<button type="button" id="JScompareArticles" class="currency-btn {{ count(Session::get('compare_ids')) > 0 ? 'show-compered-active' : 'show-compered' }}" data-toggle="modal" data-target="#compared-articles">{{ Language::trans('Upoređeni artikli') }}</button>
		@endif
		
		@if(Options::product_currency()==1)
            <div class="dropdown inline-block">
            	<div class="dropdown-toggle flex" data-toggle="dropdown">
            	 <button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
            	 	 {{Articles::get_valuta()}} 
            	 	 <span class="caret"></span>
            	 </button>
                 
                <ul class="dropdown-menu currency-list">
                	@foreach(DB::table('valuta')->where('ukljuceno',1)->orderBy('izabran','desc')->get() as $valuta)
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('valuta') }}/{{ $valuta->valuta_id }}" rel="nofollow">{{ Language::trans($valuta->valuta_sl) }}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Options::product_sort()==1)
            <div class="dropdown inline-block"> 
            	<div class="dropdown-toggle flex" data-toggle="dropdown">
	            	<button class="currency-btn " type="button" >
	            	 	<span class="options-label">{{ Language::trans('Sortiraj po') }}: </span>
		                {{Articles::get_sort()}}
	            	</button>
	                <div class="currency-btn-down flex justify-content-center"><i class="fas fa-chevron-down"></i></div>
            	</div>
                <ul class="dropdown-menu currency-list">
                	@if(Options::web_options(207) == 0)
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow"><span class="options-label">Sortiraj po:</span> {{ Language::trans('Ceni rastuće') }}</a></li>
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow"><span class="options-label">Sortiraj po:</span> {{ Language::trans('Ceni opadajuće') }}</a></li>
                    @else
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow"><span class="options-label">Sortiraj po:</span> {{ Language::trans('Ceni rastuće') }}</a></li>
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow"><span class="options-label">Sortiraj po:</span> {{ Language::trans('Ceni opadajuće') }}</a></li>
                    @endif
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/news" rel="nofollow"><span class="options-label">Sortiraj po:</span> {{ Language::trans('Najnovije') }}</a></li>
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/name" rel="nofollow"><span class="options-label">Sortiraj po:</span> {{ Language::trans('Prema nazivu') }}</a></li>
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/rbr" rel="nofollow"><span class="options-label">Sortiraj po:</span> {{ Language::trans('Popularni') }}</a></li>
                </ul>
            </div>
        @endif
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12 sm-text-center no-padding flex justify-content-end paginator-box">
	    <!-- <span>Strana:</span>     -->
		{{ Paginator::make($articles, $count_products, $limit)->links() }}
	          
	    <!-- GRID LIST VIEW -->
    	<!-- @if(Options::product_view()==1)
		@if(Session::has('list'))
		<div class="view-buttons inline-block"> 
			<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/list" rel="nofollow"><span class="fas fa-list"></span></a>
			<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/grid" rel="nofollow"><span class="fas fa-th active"></span></a>  		 
		 </div>
		@else
		<div class="view-buttons inline-block">  
			<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/list" rel="nofollow"><span class="fas fa-list active"></span></a>
			<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/grid" rel="nofollow"><span class="fas fa-th"></span></a>
		 </div>
		@endif
        @endif  --> 
	</div>
</div>

<!-- <div class="text-center">
	{{ Paginator::make($articles, $count_products, $limit)->links() }}
</div> -->
 
   <!-- MODAL FOR COMPARED ARTICLES -->
  <div class="modal fade" id="compared-articles" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">{{ Language::trans('Upoređeni artikli') }}</h4>
        </div>
        <div class="modal-body">
			<div class="compare-section">
				<div id="compare-article">
					<div id="JScompareTable" class="compare-table text-center table-responsive"></div>
				</div>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="button" data-dismiss="modal">{{ Language::trans('Zatvori') }}</button>
        </div>
      </div>    
    </div>
  </div>

<!-- PRODUCTS -->
@if(Session::has('list') or Options::product_view()==3)

<!-- LIST PRODUCTS -->
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list') 
	@endforeach 

@else
<!-- Grid proucts -->
<div class="row">
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
	@endforeach
</div>		
@endif

@if($count_products == 0) 
	<div class="col-md-12 col-sm-12 col-xs-12 no-padding"> 
		<div class="no-articles"> {{ Language::trans('Trenutno nema artikla za date kategorije') }}</div>
	</div>
@endif

<div class="text-center "> 
	{{ Paginator::make($articles, $count_products, $limit)->links() }}
</div>

@endsection