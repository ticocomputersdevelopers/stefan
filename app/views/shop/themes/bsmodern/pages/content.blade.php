@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- CONTENT.blade -->

@foreach($sekcije as $sekcija) 
<div {{ !empty($sekcija->boja_pozadine) ? 'style="background-color:'.($sekcija->boja_pozadine).'"' : (($sekcija->puna_sirina == 1) ? 'style="background-color: #fff"' : '') }}> 
	@if($sekcija->tip_naziv == 'Text')
	<!-- TEXT SECTION -->
	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20">
			<div class="col-xs-12">
				<div {{ $strana == All::get_page_start() ? "class='JSd_content'" : "" }}> 

					{{ Support::pageSectionContent($sekcija->sekcija_stranice_id) }}

				</div>
			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Lista artikala' AND !is_null($sekcija->tip_artikla_id))
	<!-- PRODUCTS SECTION -->
	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20">
			<div class="col-xs-12">
				<div {{ $strana == All::get_page_start() ? "class='JSd_content'" : "" }}> 

					@if($sekcija->tip_artikla_id == -1 AND count($latestAdded = Articles::latestAdded(5)) > 0)

					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

					<div class="JSproducts_slick"> 
						@foreach($latestAdded as $row)
						@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
						@endforeach
					</div> 

					@elseif($sekcija->tip_artikla_id == -2 AND count($mostPopularArticles = Articles::mostPopularArticles(5)) > 0)

					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

					<div class="JSproducts_slick"> 
						@foreach($mostPopularArticles as $row)
						@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
						@endforeach 
					</div> 

					@elseif($sekcija->tip_artikla_id == -3 AND count($bestSeller = Articles::bestSeller()) > 0)

					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

					<div class="JSproducts_slick"> 
						@foreach($bestSeller as $row)
						@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
						@endforeach
					</div> 

					@elseif($sekcija->tip_artikla_id == 0 AND All::broj_akcije() > 0)

					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

					<div class="JSproducts_slick"> 
						@foreach(Articles::akcija(null,100) as $row)
						@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
						@endforeach
					</div> 

					@elseif(All::provera_tipa($sekcija->tip_artikla_id)) 

					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2> 

					<div class="JSproducts_slick"> 
						@foreach(Articles::artikli_tip($sekcija->tip_artikla_id,100) as $row)
						@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
						@endforeach
					</div>   

					@endif

				</div>
			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Lista vesti')
	<!-- BLOGS SECTION-->
	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20">
			<div class="col-xs-12">
				<div {{ $strana == All::get_page_start() ? "class='JSd_content'" : "" }}> 

					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

					<div class="JSblog-slick row">   
						@foreach(All::getShortListNews() as $row) 
						<div class="col-md-4">
							<div class="news"> 
								@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))

								<a class="bg-img center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></a>

								@else

								<iframe src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

								@endif

								<h3 class="news-title overflow-hdn">
									<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ $row->naslov }}</a>
								</h3>

								<div class="text-uppercase">
									{{ Support::date_convert($row->datum) }}
								</div>
							</div>
						</div>
						@endforeach 
					</div> 

				</div>
			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Slajder' AND !is_null($sekcija->slajder_id))

	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}">  

		@if($slajder = Slider::slajder($sekcija->slajder_id) AND count($slajderStavke = Slider::slajderStavke($slajder->slajder_id)) > 0)

		<div {{ $strana == All::get_page_start() ? "class='JSd_content'" : "" }}> 

			@if($slajder->slajder_tip_naziv == 'Slajder')

			<div class="JSmain-slider JShide">
				@foreach($slajderStavke as $slajderStavka)
				<div class="relative">

					<a href="{{ $slajderStavka->link }}" target="_blank">
						<img class="img-responsive" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />
					</a>

					<div class="sliderText"> 
						<!-- @if($slajderStavka->naslov != '') 
						<div class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov }}
						</div> 
						@endif -->

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						<!-- @if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov_dugme }}
						</a> 
						@endif -->
					</div>
				</div>
				@endforeach
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Baner')
			<!-- BANNERS SECTION -->
			<div class="banners row padding-v-20">
				@foreach($slajderStavke as $slajderStavka)
				<div class="col-md-4 col-sm-4 col-xs-4">
					<a class="center-block" href="{{ $slajderStavka->link }}" target="_blank">
						<img class="img-responsive margin-auto" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />
					</a>

					@if($slajderStavka->sadrzaj != '') 
					<div class="sliderText short-desc JSInlineFull content hidden-xs" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
						{{ $slajderStavka->sadrzaj }}
					</div> 
					@endif
				</div>  
				@endforeach
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Baneri - karosel')
			<div class="banners padding-v-20">
				<!-- BANNERS SECTION -->
				<div class="JS_banners_carousel row">
					@foreach($slajderStavke as $slajderStavka)
					<div class="col-md-3 col-sm-3 col-xs-3">
						<a class="center-block" href="{{ $slajderStavka->link }}" target="_blank">
							<img class="img-responsive margin-auto" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />
						</a>

						@if($slajderStavka->sadrzaj != '') 
						<div class="sliderText short-desc JSInlineFull content hidden-xs" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif
					</div>  
					@endforeach
				</div>
			</div>
			
			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst levo)')
			<!-- TEXT BANNER LEFT SECTION -->
			@foreach($slajderStavke as $slajderStavka) 
			<div class="row padding-v-20 flex">
				<div class="col-xs-7 text-center">
					<div>   
						@if($slajderStavka->naslov != '') 
						<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						@if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="slider-btn-link JSInlineShort inline-block" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov_dugme }}
						</a> 
						@endif
					</div>
				</div>
				
				<div class="col-xs-5 no-padding txt_banner">
					<a href="{{ $slajderStavka->link }}" class="slider-link"></a>
					<div class="bg-img" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"></div>
				</div>
			</div>
			@endforeach

			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst desno)')
			<!-- TEXT BANNER RIGHT SECTION -->
			@foreach($slajderStavke as $slajderStavka) 
			<div class="row padding-v-20 flex">
				<div class="col-xs-5 no-padding txt_banner">
					<a href="{{ $slajderStavka->link }}" target="_blank" class="slider-link"></a>
					<div class="bg-img" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"></div>
				</div>

				<div class="col-xs-7 text-center">
					<div>   
						@if($slajderStavka->naslov != '') 
						<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						@if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="slider-btn-link JSInlineShort inline-block" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov_dugme }}
						</a> 
						@endif
					</div>
				</div>
			</div>
			@endforeach

			@elseif($slajder->slajder_tip_naziv == 'Galerija Baner')
			<!-- GALLERY BANNER SECTION -->
			<div class="row padding-v-20">  
				<div class="col-xs-12"> 
					<div class="gallery-ban">
						@if(isset($slajderStavke[0]))  
							<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavke[0]->image_path }}' )">
								<a href="{{ $slajderStavke[0]->link }}" target="_blank" class="slider-link"></a> 
									
								@if($slajderStavke[0]->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavke[0]->naslov }}
								</h2> 
								@endif
							</div>
						@endif

						@foreach(array_slice($slajderStavke,1) as $position => $slajderStavka) 
						
						@if($position == 0 || $position == 3)   
							<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"> 
								<a href="{{ $slajderStavka->link }}" target="_blank" class="slider-link"></a> 
								
								@if($slajderStavka->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif
							</div> 
						@endif

						@if($position == 1 || $position == 2)   
							<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavka->image_path }}' )">
								<a href="{{ $slajderStavka->link }}" target="_blank" class="slider-link"></a>
									
								@if($slajderStavka->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavka->naslov }}
								</h2>  
								@endif
							</div>
						@endif
						@endforeach 
					</div> 
				</div>
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Galerija Slajder')
			<!-- GALLERY SLIDER SECTION -->
			<div class="row padding-v-20">  
				<div class="col-xs-12"> 
					<div class="main_imgGallery flex">
	                    <img class="JSmain_img img-responsive" src="{{ Options::domain() }}{{ $slajderStavke[0]->image_path }}" alt="image" />
	                </div>
	                <!-- CUSTOM MODAL -->
	                <div class="JSmodal">
	                    <div class="flex full-screen relative"> 
	                      
	                        <div class="modal-cont relative text-center"> 
	                      
	                            <div class="inline-block relative"> 
	                                <span class="JSclose-modal"><i class="fas fa-times"></i></span>
	                          
	                                <img src="" class="img-responsive" id="JSmodal_img" alt="modal image">
	                            </div>

	                            <button class="btn JSleft_btn"><i class="fas fa-chevron-left"></i></button>
	                            <button class="btn JSright_btn"><i class="fas fa-chevron-right"></i></button>
	                        </div>
	                
	                    </div>
	                </div>
	     
					<div class="gallery_slider"> 
						@foreach($slajderStavke as $slajderStavka) 
							<a class="flex JSimg-gallery relative" href="javascript:void(0)">
									
								<img class="img-responsive" src="{{ Options::domain().$slajderStavka->image_path }}" id="{{ Options::domain().$slajderStavka->image_path }}" alt="image">

								<!-- @if($slajderStavka->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif -->

							</a> 
						@endforeach 
					</div>
				</div>
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Prodavnica')
			<!-- SHOPS SECTION -->
			<div class="row padding-v-20">  
				@foreach($slajderStavke as $slajderStavka)
				<div class="col-md-4 col-sm-4 col-xs-6 mob-w-100">
					<div class="shop-card relative">
						<a class="center-block relative" href="{{ $slajderStavka->link }}">
							<img class="img-responsive margin-auto" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />

							@if($slajderStavka->naslov != '')
							<h2 class="shop-card-title"> 
								{{ $slajderStavka->naslov }}
							</h2>  
							@endif
						</a>

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif
					</div>
				</div>  
				@endforeach
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Galerija Slika')
			<!-- GALLERY IMAGES SECTION -->
			<div class="gallery-shop-wrap row padding-v-20">  
				<div class="col-xs-12"> 
	                <!-- CUSTOM MODAL -->
	                <div class="JSmodal">
	                    <div class="flex full-screen relative"> 
	                      
	                        <div class="modal-cont relative text-center"> 
	                      
	                            <div class="inline-block relative"> 
	                                <span class="JSclose-modal"><i class="fas fa-times"></i></span>
	                          
	                                <img src="" class="img-responsive" id="JSmodal_img" alt="modal image">
	                            </div>

	                            <button class="btn JSleft_btn"><i class="fas fa-chevron-left"></i></button>
	                            <button class="btn JSright_btn"><i class="fas fa-chevron-right"></i></button>
	                        </div>
	                
	                    </div>
	                </div>
	     
					<div class="gallery-shop"> 
						@foreach($slajderStavke as $slajderStavka) 
							<a class="flex JSimg-gallery relative" href="javascript:void(0)">
									
								<img class="img-responsive" src="{{ Options::domain().$slajderStavka->image_path }}" id="{{ Options::domain().$slajderStavka->image_path }}" alt="image">

								<!-- @if($slajderStavka->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif -->

							</a> 
						@endforeach 
					</div>
				</div>
			</div>

			@endif  
		</div> <!-- JSd_content -->

		@endif
	</div> <!-- CONTAINER -->


	@elseif($sekcija->tip_naziv == 'Newsletter' AND Options::newsletter()==1 AND !is_null($newslatter_description = All::newslatter_description()))
	<!-- NEWSLETTER SECTION -->
	<div class="parent-container {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}">  
		<div class="row {{ $strana == All::get_page_start() ? 'JSd_content' : '' }} padding-v-20 flex"> 

			<div class="col-md-6 col-sm-6 col-xs-12">
				<h5 class="ft-section-title JSInlineShort" data-target='{"action":"newslatter_label"}'>
					{{ $newslatter_description->naslov }}
				</h5> 
				<div class="JSInlineFull" data-target='{"action":"newslatter_content"}'>
					{{ $newslatter_description->sadrzaj }}
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12"> 
				<div class="newsletter relative">		 
					<input type="text" placeholder="E-mail" id="newsletter" />

					<button onclick="newsletter()" class="button">{{ Language::trans('Prijavi se') }}</button>
				</div>
			</div>
		</div>
	</div> 

	@endif 
</div> <!-- END BACKGROUND COLOR -->
@endforeach 


@if(isset($anketa_id) AND $anketa_id>0)
@include('shop/themes/'.Support::theme_path().'partials/poll')		
@endif


@if(Session::has('pollTextError'))
<script>
	$(document).ready(function(){     
		bootboxDialog({ message: "<p>{{ Session::get('pollTextError') }}</p>" }); 
	});
</script>
@endif

<!-- CONTENT.blade END -->

@endsection




