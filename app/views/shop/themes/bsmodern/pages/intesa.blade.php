@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<br><br>
		<h3>{{ Language::trans('Nije moguće kliknuti na dugme Završi plaćanje karticom, a ne obaveštava se Kupac da je potrebno da se saglasi sa Uslovima korišćenja') }}.</h3></br>

		<h4>{{ Language::trans('Podaci o korisniku') }}:</h4>
		<ul>
			@if($web_kupac->flag_vrsta_kupca == 1)
				<li>{{ Language::trans('Firma') }}: {{ $web_kupac->naziv }}</li>
			@else
				<li>{{ Language::trans('Ime') }}: {{ $web_kupac->ime.' '.$web_kupac->prezime }}</li>
			@endif
			<li>{{ Language::trans('Adresa') }}: {{ $web_kupac->adresa }}, {{ $web_kupac->mesto }}</li>
		</ul>
		</br>
		<h4>{{ Language::trans('Podaci o trgovcu') }}:</h4>
		<ul>
			<li>{{ $preduzece->naziv }}</li>
			<li>{{ Language::trans('Kontakt osoba') }}: {{ $preduzece->kontakt_osoba }}</li>
			@if($preduzece->matbr_registra != '')
			<li>{{ Language::trans('MBR') }}: {{ $preduzece->matbr_registra }}</li>
			@endif
			@if($preduzece->pib != '')
			<li>VAT Reg No: {{ $preduzece->pib }}</li>
			@endif
			<li>{{ $preduzece->adresa }}, {{ $preduzece->mesto }}</li>
			<li>{{ Language::trans('Telefon') }}: {{ $preduzece->telefon }}</li>
			<li><a href="mailto:{{ $preduzece->email }}">{{ $preduzece->email }}</a></li>
		</ul>
		</br>

		<input type="checkbox" id="JSConditions" onclick="clickCheckbox()"> <a href="{{ Options::base_url() }}uslovi-kupovine" target="_blank">
		{{ Language::trans('Da li ste saglasni sa uslovima korišćenja') }}</a>

		<form method="post" action="https://bib.eway2pay.com/fim/est3Dgate">
		<!-- <form method="post" action="https://testsecurepay.eway2pay.com/fim/est3Dgate"> -->

		 		<button type="submit" class="button" id="JSIntesaSubmit" disabled>{{ Language::trans('Završi plaćanje karticom') }}</button>

 				<input type="hidden" name="clientid" value="{{ $clientId }}">
				<input type="hidden" name="oid" value="{{ $oid }}">
				<input type="hidden" name="amount" value="{{ $amount }}">
				<input type="hidden" name="okurl" value="{{ $okUrl }}">
				<input type="hidden" name="failUrl" value="{{ $failUrl }}">
				<input type='hidden' name='shopurl' value="{{ $cancelUrl }}">
				<input type="hidden" name="TranType" value="{{ $transactionType }}">
				<input type="hidden" name="currency" value="{{ $currency }}">
				<input type="hidden" name="rnd" value="{{ $rnd }}">
				<input type="hidden" name="hash" value="{{ $hash }}">
				<input type="hidden" name="storetype" value="3D_PAY_HOSTING">
				<input type="hidden" name="hashAlgorithm" value="ver2">
				<input type="hidden" name="encoding" value="utf-8" />
				<input type='hidden' name="lang" value="sr">			
				
		</form>
		<script type="text/javascript">
			function clickCheckbox(){
				if(document.getElementById("JSConditions").checked){
					document.getElementById("JSIntesaSubmit").removeAttribute('disabled');
				}else{
					document.getElementById("JSIntesaSubmit").setAttribute('disabled','disabled');
				}
			}
		</script>
	</div>
@endsection