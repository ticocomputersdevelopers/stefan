<div class="JSsearch_result">
	<div class="first-search-result">
		<h4 class="text-center"> {{Language::trans('Rezultat pretrage')}} </h4>
        <div class="first-search-content">
    		@if(count($groups) > 0)
    		<ul>
    			@foreach(array_slice($groups,0,50) as $group)
    			<li>
    				{{ $search }} u 
    				<span>
    				<a href="{{ Options::base_url() }}{{ $group['_source']['slug'] }}">{{ $group['_source']['name'] }}</a>
    				</span>
    			</li>
    			@endforeach
    		</ul>
    		@endif
        </div>
	</div>

    <div class="row second-search-result">
        <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
            <h4 class="text-center"> {{Language::trans('Proizvođač')}} </h4>
            <div class="second-search-content">
        		@if(count($manufacturers) > 0)
                <ul>
                	@foreach(array_slice($manufacturers,0,50) as $manufacturer)
                	<li><a href="{{ Options::base_url().Url_mod::slug_trans('proizvodjac') }}/{{ $manufacturer['_source']['slug'] }}">{{ $manufacturer['_source']['name'] }}</a></li>
                	@endforeach
                </ul>
            	@endif
            </div>
        </div>

         <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
            <h4 class="text-center"> {{Language::trans('Preporučeni proizvodi')}} </h4>
            <div class="second-search-content">
                @if(count($products) > 0)
                <ul>
                	@foreach($products as $product)
                    <li class="row flex">
                        <div class="col-xs-3 p-left"> 
                            <a class="search-list-photo" href="#!">
                                <img class="margin-auto center-block img-responsive" src="{{ $product['_source']['image'] }} " alt="{{ $product['_source']['name'] }}"/>
                            </a>
                        </div>

                        <div class="col-xs-9">
                            <a class="search-list-name inline-block" href="{{ Options::base_url() .Url_mod::slug_trans('artikal') }}/{{ $product['_source']['slug'] }}">
                                {{ $product['_source']['name'] }} 
                            </a> 

                            <div class="row">
                                <div class="col-xs-6 no-padding">
                                    <div class="review">
                                       {{ $product['_source']['rating'] }}
                                    </div>
                                </div>
                                <div class="col-xs-6 no-padding text-right">
                                    {{ $product['_source']['price'] }}
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @endif
            </div>
        </div>

    </div>

</div>