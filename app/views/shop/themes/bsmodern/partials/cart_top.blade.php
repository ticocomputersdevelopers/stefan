<!-- <div class="col-md-3 col-sm-2 col-xs-3"> -->
	
	<div class="header-cart-container relative">  
		
		<a class="header-cart flex relative" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}" rel="nofollow">
			
			<i class="sprite sprite-cart"></i>		
			
			<div class="flex header-cart-info-box hidden-sm hidden-xs">
				<div class="header-cart-info hidden-sm hidden-xs header-cart-info-top"><span class="JScart-count-num">{{ Cart::broj_cart() }}</span> <span class="JScart-count-label"></span></div>
				<div class="header-cart-info hidden-sm hidden-xs"><span class="cart-price-count">{{ Cart::cena(Cart::cart_ukupno()) }}</span></div>
			</div>

			
			<!-- <input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	 -->
		</a>

		<div class="JSheader-cart-content hidden-sm hidden-xs">
			@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
		</div>
	</div>
<!-- </div>  -->
