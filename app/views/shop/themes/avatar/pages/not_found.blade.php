@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
	<br>
	<h2><span class="section-title">{{ Language::trans('Greška') }}</span></h2> 
    <p class="no-articles">{{ Language::trans('Žao nam je, stranica nije dostupna') }}.</p> 
@endsection