@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
	<div class="row">
	@foreach($brendovi as $brend)
	    <div class="col-md-2 col-sm-6 col-xs-6">
	        <a class="brend-item flex" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slugify($brend->naziv) }}">
	            @if($brend->slika != null OR $brend->slika != '')
	            <img class="img-responsive" src="{{ Options::domain() }}{{ $brend->slika }}" alt="{{ $brend->naziv }}" />
	            @else
	            {{ $brend->naziv }}
	            @endif
	        </a>
	    </div>	
	@endforeach
	</div>
@endsection