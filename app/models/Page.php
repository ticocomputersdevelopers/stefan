<?php

class Page extends Eloquent
{
    protected $table = 'web_b2c_seo';

    protected $primaryKey = 'web_b2c_seo_id';
    
    public $timestamps = false;

	protected $hidden = array(
		'web_b2c_seo_id',
    	'naziv_stranice',
        'title',
        'description',
        'keywords',
        'og_title',
        'og_description',
        'og_image',
        'og_type',
        'og_url',
        'og_site_name',
        'twitter_card',
        'twitter_title',
        'twitter_description',
        'twitter_url',
        'twitter_image',
        'twitter_creator',
        'flag_page',
        'rb_strane',
        'b2b_header',
        'b2b_footer',
        'web_content',
        'seo_title',
        'flag_b2b_show',
        'tip_artikla_id',
        'disable',
        'grupa_pr_id',
        'tekst',
        'parrent_id',
        'redirect_web_b2c_seo_id'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['web_b2c_seo_id']) ? $this->attributes['web_b2c_seo_id'] : null;
	}
	public function getSlugAttribute()
	{
	    return isset($this->attributes['naziv_stranice']) ? $this->attributes['naziv_stranice'] : null;
	}
	public function getParentIdAttribute()
	{
	    return isset($this->attributes['parrent_id']) ? $this->attributes['parrent_id'] : null;
	}
    public function getSortAttribute()
    {
        return isset($this->attributes['rb_strane']) ? $this->attributes['rb_strane'] : null;
    }

	protected $appends = array(
    	'id',
    	'slug',
    	'parent_id',
        'sort'
    	);

    public function parent(){
        return $this->belongsTo('Page','parrent_id');
    }

    public function childs(){
        return $this->hasMany('Page', 'parrent_id', 'web_b2c_seo_id');
    }

    public function pageLangs(){
        return $this->hasMany('PageLang', 'web_b2c_seo_id');
    }

}