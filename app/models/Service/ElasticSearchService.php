<?php 
namespace Service;

use Elasticsearch\ClientBuilder;
use DB;
use Url_mod;
use Product;
use Cart;
use Options;
use AdminCommon;
use All;

class ElasticSearchService {
	private $client;
	private $hosts = [
		'127.0.0.1:9200'
		// 'https://elastic:3ADEFp7D5i2R8gnEiqcAsRGL@b00e01ce8c514a59a82c703f10b4445e.eu-central-1.aws.cloud.es.io:9243/'
	];
	private $basePath;

	public function __construct(){
		$this->client = ClientBuilder::create()
				// ->setHosts($hosts)
				->build();
		$this->basePath = str_replace(array('http://','https://','/'),array('','',''),Options::base_url());
	}

	public function getIndexes(){
		return $this->client->cat()->indices(array('index' => '*'));;
	}

	public function updateArticles(){
		$articles = DB::select("select roba_id, naziv_web, tags, sifra_is, sku, sifra_d, (select naziv from proizvodjac where proizvodjac_id = r.proizvodjac_id limit 1) proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) AS grupa from roba r where flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 order by naziv_web asc");

		$params = array('body'=>array());
		foreach($articles as $article){
		    $params['body'][] = [
		        'index' => [
		            '_index' => $this->basePath.'.articles',
		            '_type' => 'article',
		            '_id' => base64_encode($this->basePath.'.articles.'.$article->roba_id)
				]
		    ];

		    $clearName = $this->clear($article->naziv_web);
		    $params['body'][] = [
				'id' => $article->roba_id,
				'name' => $article->naziv_web,
				'slug' => Url_mod::slugify(Product::seo_title($article->roba_id)),
				'clear_name' =>$clearName,
				'clear_name_blank' => preg_replace('/[^A-Za-z0-9]/', ' ',$clearName),
				'clear_name_spetial' => preg_replace('/[^A-Za-z0-9]/', '',$clearName),
				'tags' => $article->tags,
				'clear_tags' => $this->clear($article->tags),
				'manufacturer' => $article->proizvodjac,
				'clear_manufacturer' => $this->clear($article->proizvodjac),
				'category' => $article->grupa,
				'clear_category' => $this->clear($article->grupa),
				'price' => Cart::cena(Product::get_price($article->roba_id)),
				'image' => Options::domain() . Product::web_slika($article->roba_id),
				'rating' => Product::getRating($article->roba_id) . ' (' . Product::getRatingNBR($article->roba_id) . ')',
				// 'description' => $article->web_opis,
				// 'clear_description' => $this->clear($article->web_opis),
				// 'add_name' => $article->naziv_dopunski,
				// 'clear_add_name' => $this->clear($article->naziv_dopunski),
				'code_is' => strtolower($article->sifra_is),
				'sku' => strtolower($article->sku),
				'code_d' => strtolower($article->sifra_d)
		    ];
		}

		$checkParams = ['index'=>$this->basePath.'.articles'];
		if($this->client->indices()->exists($checkParams)){
			$this->client->indices()->delete($checkParams);
		}
		if(!empty($params['body'])) {
			$this->client->bulk($params);
		}
	}

	public function searchProducts($search,$groupId=0){
		if(empty($search)){
			return array();
		}
		$words = $this->words($search);
		$query = "*".implode("* AND *",$words)."*";

		$searchName = [
		    'index' => $this->basePath.'.articles',
		    'type' => 'article',
		    'body' => [
		    	'size' => 50,
		    	'query' => [
			        'query_string' => [
			            'query' => $query,
			            'fields' => ['clear_name' ,'clear_name_blank', 'clear_name_spetial']
			        ]
	    		]
		    ]
		];
		$searchOther = [
		    'index' => $this->basePath.'.articles',
		    'type' => 'article',
		    'body' => [
		    	'size' => 80,
		    	'query' => [
			        'query_string' => [
			            'query' => $query,
			            'fields' => ['clear_tags', 'clear_category', 'clear_manufacturer', 'code_is', 'sku', 'code_d']
			        ]
	    		]
		    ]
		];

		$searchResult = array('hits'=>array('hits'=>array()));
		$searchResultName = $this->client->search($searchName);
		$searchResultOther = $this->client->search($searchOther);
		$checkArratIds = array();

		foreach($searchResultName['hits']['hits'] as $searchItem){
			if(!in_array($searchItem['_source']['id'],$checkArratIds)){
				array_push($searchResult['hits']['hits'],$searchItem);
				$checkArratIds[] = $searchItem['_source']['id'];
			}
		}
		foreach($searchResultOther['hits']['hits'] as $searchItem){
			if(count($checkArratIds) >= 25){
				break;
			}
			if(!in_array($searchItem['_source']['id'],$checkArratIds)){
				array_push($searchResult['hits']['hits'],$searchItem);
				$checkArratIds[] = $searchItem['_source']['id'];
			}
		}

		return $searchResult;
	}
	public function searchProductsEnter($search,$groupId=0){
		if(empty($search)){
			return array();
		}
		$words = $this->words($search);
		$query = "*".implode("* AND *",$words)."*";

		$searchName = [
		    'index' => $this->basePath.'.articles',
		    'type' => 'article',
		    'body' => [
		    	'size' => 10000,
		    	'query' => [
			        'query_string' => [
			            'query' => $query,
			            'fields' => ['clear_name' ,'clear_name_blank', 'clear_name_spetial']
			        ]
	    		]
		    ]
		];
		$searchOther = [
		    'index' => $this->basePath.'.articles',
		    'type' => 'article',
		    'body' => [
		    	'size' => 80,
		    	'query' => [
			        'query_string' => [
			            'query' => $query,
			            'fields' => ['clear_tags', 'clear_category', 'clear_manufacturer', 'code_is', 'sku', 'code_d']
			        ]
	    		]
		    ]
		];

		$searchResult = array('hits'=>array('hits'=>array()));
		$searchResultName = $this->client->search($searchName);
		$searchResultOther = $this->client->search($searchOther);
		$checkArratIds = array();

		foreach($searchResultName['hits']['hits'] as $searchItem){
			if(!in_array($searchItem['_source']['id'],$checkArratIds)){
				array_push($searchResult['hits']['hits'],$searchItem);
				$checkArratIds[] = $searchItem['_source']['id'];
			}
		}
		foreach($searchResultOther['hits']['hits'] as $searchItem){
			if(count($checkArratIds) >= 25){
				break;
			}
			if(!in_array($searchItem['_source']['id'],$checkArratIds)){
				array_push($searchResult['hits']['hits'],$searchItem);
				$checkArratIds[] = $searchItem['_source']['id'];
			}
		}

		return $searchResult;
	}
	public function updateGroups(){

		$groups = array();
		foreach(DB::select("select grupa_pr_id, grupa from grupa_pr where prikaz = 1 and web_b2c_prikazi = 1 and parrent_grupa_pr_id = 0 order by grupa_pr_id asc") as $level1){
			$childIds = array();
			AdminCommon::allGroups($childIds,$level1->grupa_pr_id);
			if(($count1=count(DB::select("select distinct r.roba_id from roba r left join roba_grupe rg ON rg.roba_id = r.roba_id where flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and ( r.grupa_pr_id in(".implode(",",$childIds).") or rg.grupa_pr_id in(".implode(",",$childIds)."))"))) > 0){
				$groups[] = (object) array(
					'grupa_pr_id' => $level1->grupa_pr_id,
					'grupa' => $level1->grupa,
					'slug' => Url_mod::slug_trans($level1->grupa),
					'count' => $count1
				);
			}
			foreach(DB::select("select grupa_pr_id, grupa from grupa_pr where prikaz = 1 and web_b2c_prikazi = 1 and parrent_grupa_pr_id = ".$level1->grupa_pr_id." order by grupa_pr_id asc") as $level2){
				$childIds = array();
				AdminCommon::allGroups($childIds,$level2->grupa_pr_id);
				if(($count2=count(DB::select("select distinct r.roba_id from roba r left join roba_grupe rg ON rg.roba_id = r.roba_id where flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and ( r.grupa_pr_id in(".implode(",",$childIds).") or rg.grupa_pr_id in(".implode(",",$childIds)."))"))) > 0){
					$groups[] = (object) array(
						'grupa_pr_id' => $level2->grupa_pr_id,
						'grupa' => $level1->grupa.' / '.$level2->grupa,
						'slug' => Url_mod::slug_trans($level1->grupa).'/'.Url_mod::slug_trans($level2->grupa),
						'count' => $count2
					);
				}
				foreach(DB::select("select grupa_pr_id, grupa from grupa_pr where prikaz = 1 and web_b2c_prikazi = 1 and parrent_grupa_pr_id = ".$level2->grupa_pr_id." order by grupa_pr_id asc") as $level3){
					$childIds = array();
					AdminCommon::allGroups($childIds,$level3->grupa_pr_id);
					if(($count3=count(DB::select("select distinct r.roba_id from roba r left join roba_grupe rg ON rg.roba_id = r.roba_id where flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and ( r.grupa_pr_id in(".implode(",",$childIds).") or rg.grupa_pr_id in(".implode(",",$childIds)."))"))) > 0){
						$groups[] = (object) array(
							'grupa_pr_id' => $level3->grupa_pr_id,
							'grupa' => $level1->grupa.' / '.$level2->grupa.' / '.$level3->grupa,
							'slug' => Url_mod::slug_trans($level1->grupa).'/'.Url_mod::slug_trans($level2->grupa).'/'.Url_mod::slug_trans($level3->grupa),
							'count' => $count3
						);
					}
					foreach(DB::select("select grupa_pr_id, grupa from grupa_pr where prikaz = 1 and web_b2c_prikazi = 1 and parrent_grupa_pr_id = ".$level3->grupa_pr_id." order by grupa_pr_id asc") as $level4){
						$childIds = array();
						AdminCommon::allGroups($childIds,$level4->grupa_pr_id);
						if(($count4=count(DB::select("select distinct r.roba_id from roba r left join roba_grupe rg ON rg.roba_id = r.roba_id where flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and ( r.grupa_pr_id in(".implode(",",$childIds).") or rg.grupa_pr_id in(".implode(",",$childIds)."))"))) > 0){
							$groups[] = (object) array(
								'grupa_pr_id' => $level4->grupa_pr_id,
								'grupa' => $level1->grupa.' / '.$level2->grupa.' / '.$level3->grupa.' / '.$level4->grupa,
								'slug' => Url_mod::slug_trans($level1->grupa).'/'.Url_mod::slug_trans($level2->grupa).'/'.Url_mod::slug_trans($level3->grupa).'/'.Url_mod::slug_trans($level4->grupa),
								'count' => $count4
							);
						}
					}
				}
			}
		}

		usort($groups, function($a, $b) {
			return ($a->grupa_pr_id - $b->grupa_pr_id);
		});

		$params = array('body'=>array());
		foreach($groups as $group){
		    $params['body'][] = [
		        'index' => [
		            '_index' => $this->basePath.'.groups',
		            '_type' => 'group',
		            '_id' => base64_encode($this->basePath.'.groups.'.$group->grupa_pr_id)
				]
		    ];

		    $params['body'][] = [
				'id' => $group->grupa_pr_id,
				'name' => $group->grupa,
				'slug' => $group->slug,
				'count' => $group->count,
				'clear_name' => $this->clear($group->grupa),
				'artices_names' => implode("|",array_map('current',DB::select("SELECT naziv_web from roba where grupa_pr_id = ".$group->grupa_pr_id." and flag_prikazi_u_cenovniku = 1 and flag_aktivan = 1")))

		    ];
		}

		$checkParams = ['index'=>$this->basePath.'.groups'];
		if($this->client->indices()->exists($checkParams)){
			$this->client->indices()->delete($checkParams);
		}
		if(!empty($params['body'])) {
			$this->client->bulk($params);
		}
	}

	public function searchGroups($search,$groupId=0){
		if(empty($search)){
			return array();
		}
		$words = $this->words($search);

		return $this->client->search([
		    'index' => $this->basePath.'.groups',
		    'type' => 'group',
		    'body' => [
		    	'size' => 100,
		    	'query' => [
			        'query_string' => [
			            'query' => "*".implode("* AND *",$words)."*",
			            'fields' => ['clear_name','artices_names']
			        ],			        
		    	]
		    ]
		]);
	}

	public function updateManufacturers(){
		$manufacturers = DB::select("select proizvodjac_id, naziv, (select count(roba_id) from roba where flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and proizvodjac_id = p.proizvodjac_id) as count from proizvodjac p where brend_prikazi = 1 and (select count(roba_id) from roba where flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and proizvodjac_id = p.proizvodjac_id) > 0 order by (select count(roba_id) from roba where flag_aktivan = 1 and flag_prikazi_u_cenovniku = 1 and proizvodjac_id = p.proizvodjac_id) desc");

		$params = array('body'=>array());
		foreach($manufacturers as $manufacturer){
		    $params['body'][] = [
		        'index' => [
		            '_index' => $this->basePath.'.manufacturers',
		            '_type' => 'manufacturer',
		            '_id' => base64_encode($this->basePath.'.manufacturers.'.$manufacturer->proizvodjac_id)
				]
		    ];

		    $params['body'][] = [
				'id' => $manufacturer->proizvodjac_id,
				'name' => $manufacturer->naziv,
				'slug' => Url_mod::slugify($manufacturer->naziv),
				'count' => $manufacturer->count,
				'clear_name' => $this->clear($manufacturer->naziv)
		    ];
		}

		$checkParams = ['index'=>$this->basePath.'.manufacturers'];
		if($this->client->indices()->exists($checkParams)){
			$this->client->indices()->delete($checkParams);
		}
		if(!empty($params['body'])) {
			$this->client->bulk($params);
		}
	}

	public function searchManufacturers($search,$groupId=0){
		if(empty($search)){
			return array();
		}
		$words = $this->words($search);

		return $this->client->search([
		    'index' => $this->basePath.'.manufacturers',
		    'type' => 'manufacturer',
		    'body' => [
		    	'size' => 15,
		    	'query' => [
			        'query_string' => [
			            'query' => "*".implode("* AND *",$words)."*",
			            'fields' => ['clear_name']
			        ]
		    	]
		    ]
		]);
	}

	private function clear($text){
		return strtolower(str_replace(array('ć','č','š','đ','ž','Ć','Č','Š','Đ','Ž'),array('c','c','s','d','z','C','C','S','D','Z'),$text));
	}

	private function words($search){
		$search = $this->clear($search);
		$search = preg_replace('/[^A-Za-z0-9]/', ' ',$search);
		$words = explode(' ',$search);
		$words = array_filter($words,function($word){
			return !empty(trim($word));
		});
		return $words;
	}
}