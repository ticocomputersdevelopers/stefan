<?php

class ArticleComment extends Eloquent
{
    protected $table = 'web_b2c_komentari';
    
    public $timestamps = false;

	protected $hidden = array(
		'web_b2c_komentar_id',
        'roba_id',
        'ime_osobe',
        'ip_adresa',
        'pitanje',
        'odgovor',
        'komentar_odobren',
        'datum',
        'odgovoreno',
        'ocena'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['web_b2c_komentar_id']) ? $this->attributes['web_b2c_komentar_id'] : null;
	}
    public function getArticleIdAttribute()
    {
        return isset($this->attributes['roba_id']) ? $this->attributes['roba_id'] : null;
    }
    public function getClientNameAttribute()
    {
        return isset($this->attributes['ip_adresa']) ? $this->attributes['ip_adresa'] : null;
    }
    public function getIpAddressAttribute()
    {
        return isset($this->attributes['ip_adresa']) ? $this->attributes['ip_adresa'] : null;
    }
    public function getQuestionAttribute()
    {
        return isset($this->attributes['pitanje']) ? $this->attributes['pitanje'] : null;
    }
    public function getAnswerAttribute()
    {
        return isset($this->attributes['odgovor']) ? $this->attributes['odgovor'] : null;
    }
    public function getAllowAttribute()
    {
        return isset($this->attributes['komentar_odobren']) ? $this->attributes['komentar_odobren'] : null;
    }
    public function getDateAttribute()
    {
        return isset($this->attributes['datum']) ? $this->attributes['datum'] : null;
    }
    public function getAnsweredAttribute()
    {
        return isset($this->attributes['odgovoreno']) ? $this->attributes['odgovoreno'] : null;
    }
    public function getRatingAttribute()
    {
        return isset($this->attributes['ocena']) ? $this->attributes['ocena'] : null;
    }

	protected $appends = array(
    	'id',
        'article_id',
        'client_name',
        'ip_address',
        'question',
        'answer',
        'allow',
        'date',
        'answered',
        'rating'
    	);

    public function article(){
        return $this->belongsTo('Article','roba_id');
    }

}