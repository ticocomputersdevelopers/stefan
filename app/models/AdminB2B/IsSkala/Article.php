<?php
namespace IsSkala;
use DB;
use AdminB2BOptions;

class Article {

	public static function table_body($articles){
		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			$id_is = $article->id_proizvoda;

			$roba_id++;
			$sifra_k++;
			$sifra_is = $id_is;
			$naziv = substr(pg_escape_string($article->naziv_proizvoda),0,299);
			$jedinica_mere_id = 1;
			$proizvodjac_id = isset($article->id_proizvodjaca) ? Support::getManufacturerId($article->id_proizvodjaca) : -1;
			$grupa_pr_id = '-1';
			// $akcija = isset($article->na_akciji) && $article->na_akciji == 1 ? 1 : 0;
			$akcija = 0;
			$web_opis = pg_escape_string($article->opis_proizvoda);
			$flag_cenovnik = 1;

			$racunska_cena_nc = 0;
			$racunska_cena_end = 0;
			$web_cena = 0;
			if(isset($article->cena_proizvoda) && is_numeric($article->cena_proizvoda) && $article->cena_proizvoda > 0){
				// $racunska_cena_end = floatval($article->cena_proizvoda) * $course->ziralni;
				$racunska_cena_nc = floatval($article->cena_proizvoda);
				// $web_cena = $racunska_cena_nc * 1.2;
			}
			$mpcena = $web_cena;
			$akcijska_cena = 0;

			$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",0,".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,NULL,0,-1,0,0,0,0,9,0,0,0,0,1,".strval($flag_cenovnik).",0,NULL,1,".strval($racunska_cena_nc).",0,".strval($racunska_cena_end).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".$naziv."',".strval($flag_cenovnik).",NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,".strval($akcija).",0,NULL,NULL,NULL,NULL,1,0,NULL,0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,".strval($akcijska_cena).",0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0)";
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="sifra_is" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		//update
		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_is=roba_temp.sifra_is::varchar");
		//insert
		DB::statement("INSERT INTO roba (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM roba t WHERE t.sifra_is=roba_temp.sifra_is::varchar))");

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.sifra_is=roba_temp.sifra_is::varchar) AND t.sifra_is IS NOT NULL");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}