<?php
namespace IsSkala;

use DB;

class Course {

	public static function query_insert_update($courses) {
		if(isset($courses[0])) {

		   	$last_course = DB::table('kursna_lista')->orderBy('kursna_lista_id','DESC')->first();
		   	if(!is_null($last_course)){
		   		DB::table('kursna_lista')->where('kursna_lista_id',$last_course->kursna_lista_id)->update(array('ziralni'=>$courses[0]->kurs));
		   	}else{
		   		DB::table('kursna_lista')->insert(array(
		   			'kupovni'=>$courses[0]->kurs,
		   			'srednji'=>$courses[0]->kurs,
		   			'prodajni'=>$courses[0]->kurs,
		   			'ziralni'=>$courses[0]->kurs,
	        		'kursna_lista_id' => DB::table('kursna_lista')->max('kursna_lista_id')+1,
	        		'valuta_id' => 2,
	        		'datum' => date('Y-m-d'),
	        		'paritet' => 0
		   			));
		   	}

		}
	}

}