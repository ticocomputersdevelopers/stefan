<?php
namespace ISCustom;
use AdminB2BIS;
use PHPExcel;
use PHPExcel_IOFactory;
use DB;

class LagerExcelNiansa {

	public static function table_body(){

		$products_file = "files/IS/excel/lager.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
		$excelObj = $excelReader->load($products_file);
		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();

		$result_arr = array();
		$roba_id = -1;
		for ($row = 1; $row <= $lastRow; $row++) {
		    $A = $worksheet->getCell('A'.$row)->getValue();
		    $C = $worksheet->getCell('C'.$row)->getValue();
		    $D = $worksheet->getCell('D'.$row)->getValue();
		    $L = $worksheet->getCell('L'.$row)->getValue();
		    $M = $worksheet->getCell('M'.$row)->getValue();


	    	if(isset($A) && is_numeric($C) && isset($D) && is_numeric($D)){
	    		$roba_id--;
	    		// if(!is_numeric($M) || !isset($M)){
	    		// 	$M = "0";
	    		// }
				$result_arr[] = "(".$roba_id.",1,0,0,0,".$L.",0,0,0,0,0,0,0,0,(NULL)::integer,'0',0,0,0,0,2014,-1,0,0,'".$A."')";

	    	}

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {

		

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="sifra_is"){
		    	$updated_columns[] = "".$col." = lager_temp.".$col."";
			}
		}
		DB::statement("UPDATE lager t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_is=lager_temp.sifra_is");

		//insert
		DB::statement("INSERT INTO lager (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM lager t WHERE t.sifra_is=lager_temp.sifra_is))");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("UPDATE lager l SET roba_id = roba.roba_id FROM roba WHERE l.sifra_is=roba.sifra_d");
		DB::statement("DELETE FROM lager WHERE roba_id < 0");

	}

	public static function query_delete($table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM lager t WHERE NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.sifra_is=roba_temp.sifra_is)");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

	public static function get_jm_id($jm_string) {
		if($jm_string==trim('kom')){
			$jm = 1;
		}
		elseif($jm_string==trim('pak')){
			$jm = 2;
		}
		elseif($jm_string==trim('m')){
			$jm = 3;
		}
		elseif($jm_string==trim('kgr')){
			$jm = 5;
		}
		else{
			$jm = 4;
		}
		return $jm;
	}
}