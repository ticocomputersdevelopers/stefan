<?php
namespace IsPromobi;

use DB;

class Customer {

	public static function table_body($customers){

		$result_arr = array();

		$kupac_id = DB::select("SELECT nextval('web_kupac_web_kupac_id_seq')")[0]->nextval;
		foreach($customers as $customer) {
			if(isset($customer->sifra)){
		    	$kupac_id++;
	            $id_is=$customer->sifra;
	            $sifra= $customer->sifra;
	            $naziv= isset($customer->naziv) ? substr(Support::convert(strval($customer->naziv)),0,250) : null;
	            $mesto=isset($customer->mesto) ? substr(Support::convert(strval($customer->mesto)),0,200) : null;;
	            $adresa=isset($customer->adresa) ? substr(Support::convert(strval($customer->adresa)),0,250) : null;
	            $telefon=isset($customer->telefon) ? pg_escape_string(substr(strval($customer->telefon),0,250)) : null;
	            $pib=isset($customer->pib) && is_numeric(intval($customer->pib)) ? pg_escape_string(substr($customer->pib,0,100)) : null;
	            $broj_maticni=isset($customer->maticni_broj) ? substr(strval($customer->maticni_broj),0,30) : null;
	            $mail=isset($customer->email) ? substr(strval($customer->email),0,100) : null;
	            $rabat= isset($customer->rabat) && is_numeric($customer->rabat) ? floatval($customer->rabat) : 0;
	            $login = isset($customer->korisnicko_ime) ? substr(strval($customer->korisnicko_ime),0,255) : null;
	            $password = isset($customer->lozinka) ? substr(strval($customer->lozinka),0,255) : null;

		        $result_arr[] = "(".strval($kupac_id).",NULL,'".base64_encode(trim($password))."',NULL,'".trim($adresa)."','".trim($telefon)."','".$mail."',0,-1,NULL,0,1,NULL,'".trim($naziv)."',NULL,'".trim($mesto)."','".strval($pib)."',1,'".$id_is."',0.00,NULL,(NULL)::INTEGER,NULL,0,0,(NULL)::INTEGER)";
		    }
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_kupac'"));
		$table_temp = "(VALUES ".$table_temp_body.") web_kupac_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="web_kupac_id" && $col!="sifra_connect"){
		    	$updated_columns[] = "".$col." = web_kupac_temp.".$col."";
			}
		}
		DB::statement("UPDATE web_kupac t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_connect=web_kupac_temp.sifra_connect");

		//insert
		DB::statement("INSERT INTO web_kupac (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM web_kupac t WHERE t.sifra_connect=web_kupac_temp.sifra_connect))");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('web_kupac_web_kupac_id_seq', (SELECT MAX(web_kupac_id) FROM web_kupac) + 1, FALSE)");
	}
}