<?php
namespace IsAcis;
use AdminB2BOptions;
use All;

class APIData {

	public static function artikli($username,$password,$portal,$api_url){

	$curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $api_url.'/artikli_SVE',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_POSTFIELDS =>'{
        					"prijava":
					        [
					            {
				                "naziv_firme": '.$portal.',
				                "korisničko_ime":'.$username.',
				                "lozinka":'.$password.'
            					}
        					]
    						}',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
      ),
    ));

    $response = curl_exec($curl);
	$err = curl_error($curl);
	
    curl_close($curl);
   	//All::dd(json_decode($response));
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      //print_r($result);die;
	      if(isset($result)){
		      return $result;
		  }else{
		  	  return array();
		  }
	    }		
	}

	public static function lager($username,$password,$portal,$api_url){
		
	$curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $api_url.'/zalihe',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_POSTFIELDS =>'{
        					"prijava":
					        [
					            {
				                "naziv_firme": '.$portal.',
				                "korisničko_ime":'.$username.',
				                "lozinka":'.$password.'
            					}
        					]
    						}',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
      ),
    ));

    $response = curl_exec($curl);
	$err = curl_error($curl);
	
    curl_close($curl);
   	//All::dd(json_decode($response));
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      //print_r($result);die;
	      if(isset($result)){
		      return $result;
		  }else{
		  	  return array();
		  }
	    }		
	}

	public static function partners($username,$password,$portal,$api_url){
		$curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $api_url.'/pp',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_POSTFIELDS =>'{
        					"prijava":
					        [
					            {
				                "naziv_firme": '.$portal.',
				                "korisničko_ime":'.$username.',
				                "lozinka":'.$password.'
            					}
        					]
    						}',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
      ),
    ));

    $response = curl_exec($curl);
	$err = curl_error($curl);
	
    curl_close($curl);
   	//All::dd(json_decode($response));
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      //print_r($result);die;
	      if(isset($result)){
		      return $result;
		  }else{
		  	  return array();
		  }
	    }		
	}
	// public static function kupac_info($access_token){
	// 	$url = "https://portal.wings.rs/api/v1/".AdminB2BOptions::info_sys('wings')->portal."/partner.kupac.info?output=jsonapi";

	// 	$curl = curl_init();
	// 	curl_setopt_array($curl, array(
	// 	  CURLOPT_URL => $url,
	// 	  CURLOPT_RETURNTRANSFER => true,
	// 	  CURLOPT_ENCODING => "",
	// 	  CURLOPT_MAXREDIRS => 10,
	// 	  CURLOPT_TIMEOUT => 30,
	// 	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 	  CURLOPT_CUSTOMREQUEST => "GET",
	// 	  CURLOPT_HTTPHEADER => array(
	// 	    "Cookie: PHPSESSID=".$access_token.""
	// 	  ),
	// 	));

	// 	$response = curl_exec($curl);
	// 	$err = curl_error($curl);

	// 	curl_close($curl);

	//     if ($err) {
	//       $error = "cURL Error #:" . $err;
	//       return false;
	//     } else {
	//       $result = json_decode($response);
	//       return $result;
	//     }		
	// }

}