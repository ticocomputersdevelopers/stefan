<?php
namespace IsSbcs;

use Config;
use DB;


class DBData {

	public static function articles($stock_room_id=1){

		$articles = DB::connection('sbcs')->select("SELECT r.roba_id, r.sifra, naziv, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id AND grupa_pr_id > 0) AS grupa, (SELECT (SELECT grupa AS nadgrupa FROM grupa_pr WHERE grupa_pr_id = gr.parrent_grupa_pr_id AND grupa_pr_id > 0) AS nadgrupa FROM grupa_pr gr WHERE grupa_pr_id = r.grupa_pr_id AND grupa_pr_id > 0) AS nadgrupa, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = r.proizvodjac_id AND proizvodjac_id > -1) AS proizvodjac, (SELECT kolicina FROM lager WHERE roba_id = r.roba_id AND orgj_id = ".$stock_room_id." LIMIT 1) AS kolicina, racunska_cena_nc, mpcena, wr.web_cena AS web_cena, r.web_marza, wr.opis, r.flag_prikazi_u_cenovniku AS prikaz FROM roba r RIGHT JOIN web_roba wr ON wr.roba_id = r.roba_id WHERE r.roba_id > -1 AND r.flag_aktivan = 1");

		return array_map(function($article){
			return (object) array(
				"ID" => $article->roba_id,
				"sifra" => $article->sifra,
				"naziv" => $article->naziv,
				"pdv" => 20,
				"grupa" => !is_null($article->grupa) ? $article->grupa : '',
				"nadgrupa" => !is_null($article->nadgrupa) ? $article->nadgrupa : '',
				"proizvodjac" => !is_null($article->proizvodjac) ? $article->proizvodjac : '',
				"kolicina" => !is_null($article->kolicina) ? $article->kolicina : 0,
				"cena" => !is_null($article->racunska_cena_nc) ? $article->racunska_cena_nc : 0.00,
				"mpcena" => !is_null($article->mpcena) ? $article->mpcena : 0.00,
				"web_cena" => !is_null($article->web_cena) ? $article->web_cena : 0.00,
				"marza" => !is_null($article->web_marza) ? $article->web_marza : 0.00,
				"opis" => !is_null($article->opis) ? $article->opis : '',
				"prikaz" => $article->prikaz
				);
			},$articles);

	}

	public static function partners(){
		$partners = DB::connection('sbcs')->select("SELECT partner_id, naziv, (SELECT mesto FROM mesto WHERE mesto_id = p.mesto_id) AS mesto, adresa, telefon, pib, rabat FROM partner p WHERE partner_id > -1");
		$partner_card = self::partner_card();

		return array_map(function($partner) use ($partner_card){
			return (object) array(
				"sifra" => $partner->partner_id,
				"naziv" => $partner->naziv,
				"mesto" => $partner->mesto,
				"adresa" => $partner->adresa,
				"telefon" => $partner->telefon,
				"pib" => $partner->pib,
				"email" => '',
				"maticni_broj" => '',
				"ziro_racun" => '',
				"komercijalista" => '',
				"rabat" => $partner->rabat,
				"kartica_kupca" => isset($partner_card[$partner->partner_id]) ? (object) array('stavka_kartice' => $partner_card[$partner->partner_id] ) : (object) array()
				);
			},$partners);

	}

	private static function partner_card(){
		$partner_card = DB::connection('sbcs')->select("SELECT partner_id, opis AS naziv_dokumenta, opis, datum_dokumenta AS datum, duguje, potrazuje FROM partner_kartica");
		$result = array();
		
		foreach($partner_card as $card){
			if(!isset($result[$card->partner_id])){
				$result[$card->partner_id] = array();
			}
			array_push($result[$card->partner_id],$card);
		}
		return $result;
	}

}