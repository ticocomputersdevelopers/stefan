<?php
namespace IsGSM;

use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;
use All;


class FileData {

	public static function articlesExcel(){
		$file_path = "files/IS/excel/roba.xlsx";
		$products = array();
		// $partner_categories = array();

		if(!File::exists($file_path)){
			return (object) array('articles' => $products);
		}

        $excelReader = PHPExcel_IOFactory::createReaderForFile($file_path);
        $excelObj = $excelReader->load($file_path);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        
        for ($row = 1; $row <= $lastRow; $row++) {
        	if($row > 1){
	        	$products[] = (object) array(
	        		'sifra'    => $worksheet->getCell('A'.$row)->getValue(),
	        		'katalog'  => $worksheet->getCell('B'.$row)->getValue(),
	        		'barcode'  => $worksheet->getCell('C'.$row)->getValue(),
	        		'grupa'    => $worksheet->getCell('D'.$row)->getValue(),
	        		'podgrupa' => $worksheet->getCell('E'.$row)->getValue(),
	        		'proizvodjac' => $worksheet->getCell('F'.$row)->getValue(),
	        		'naziv'    => $worksheet->getCell('G'.$row)->getValue(),
	        		'jm'       => $worksheet->getCell('H'.$row)->getValue(),
	        		'pdv'      => $worksheet->getCell('I'.$row)->getValue(),
	        		'ncena'    => $worksheet->getCell('J'.$row)->getValue(),
	        		'mpcena'   => $worksheet->getCell('K'.$row)->getValue(),
	        		'kolicina' => $worksheet->getCell('L'.$row)->getValue()
	        	);
	        }
        }

		// self::make_backup($file_path);
		return (object) array('articles' => $products);
	}

	public static function articles(){
		$products = file_get_contents('https://gsmpcshop.com/api/woo/getproductsapi');
		$products = json_decode($products);

		return (object) array('articles' => $products);
	}

	public static function make_backup($filePath){
		if(File::exists($filePath)){
			$dirPath = 'files/IS/backup/'.date('Y-m-d');
			File::makeDirectory($dirPath,0777,true,true);

			$fileArr = explode('/',$filePath);
			$fileName = $fileArr[count($fileArr)-1];

			File::copy($filePath,$dirPath.'/'.date('H:i:s').'-'.$fileName);
			File::delete($filePath);
		}
	}

}