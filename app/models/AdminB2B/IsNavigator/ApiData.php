<?php
namespace IsNavigator;

use DB;
use AdminB2BOptions;


class ApiData {

	public static function getRequest($kind='load_changed_products'){
		$navigatorData = AdminB2BOptions::info_sys('navigator');

	    $curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => $navigatorData->api_url."/".$kind,
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "GET",
	      CURLOPT_HTTPHEADER => array(
	        "Content-Type: application/json"
	      ),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return [];
	    } else {
	      return json_decode($response);
	    }

	}

	public static function partners(){
		return DB::connection('sqlsrv')->select("SELECT * FROM _b2bSetSubj");
	}

	public static function partnersCards(){
		return DB::connection('sqlsrv')->select("SELECT * FROM _b2bAcctTransItem");
	}

}