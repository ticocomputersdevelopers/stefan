<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use AdminSupport;
use DOMDocument;
use ZipArchive;
use File;
use Response;
use View;

class BackupArticles {

	public static function execute($kind){

		$product_count = DB::select("SELECT COUNT(roba_id) as count FROM roba")[0]->count;
		$limit = 3000;
		$iter = intdiv($product_count,$limit);
		$mod = $product_count % $limit;
		if($mod > 0){
			$iter++;
		}
		if($kind=='xml'){
			$zipname = 'files/exporti/backup/backup_xml.zip';
            if(File::exists($zipname)){
                File::delete($zipname);
            }
			$zip = new ZipArchive;
			$zip->open($zipname, ZipArchive::CREATE);

			foreach(range(0,$iter-1) as $val){			
				$export_products = DB::select("SELECT roba_id, web_cena, mpcena, racunska_cena_nc, naziv_web, web_flag_karakteristike, web_karakteristike, tarifna_grupa_id, web_opis, grupa_pr_id, barkod, model, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT SUM(kolicina) FROM dobavljac_cenovnik dc WHERE dc.roba_id=roba.roba_id GROUP BY roba_id) AS kolicina FROM roba LIMIT ".$limit." OFFSET ".strval($val*$limit));

				$zip->addFile(self::xml_exe($export_products,$val));
			}
			$zip->close();

			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename=backup_xml.zip');
			header('Content-Length: ' . filesize($zipname));
			readfile($zipname);
			
			File::delete($zipname);
		}elseif($kind=='xls'){
			self::xls_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products,$offset){
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("artikli");
		$xml->appendChild($root);
		foreach($products as $article){
			
			$product   = $xml->createElement("artikal");

		    Support::xml_node($xml,"sifra_artikla",$article->roba_id,$product);
		    Support::xml_node($xml,"barkod",$article->barkod,$product);
		    Support::xml_node($xml,"naziv",Support::string_format($article->naziv_web),$product);
		    Support::xml_node($xml,"pdv",AdminSupport::find_tarifna_grupa($article->tarifna_grupa_id,'porez'),$product);
		    Support::xml_node($xml,"grupa",Support::string_format($article->grupa),$product);
		    Support::xml_node($xml,"proizvodjac",Support::string_format($article->proizvodjac),$product);
		    Support::xml_node($xml,"model",Support::string_format($article->model),$product);
		    Support::xml_node($xml,"kolicina",$article->kolicina,$product);
		    Support::xml_node($xml,"cena_nc",$article->racunska_cena_nc,$product);
		    Support::xml_node($xml,"mpcena",$article->mpcena,$product);
		    Support::xml_node($xml,"web_cena",$article->web_cena,$product);
		    Support::xml_node($xml,"opis",Support::string_format($article->web_opis).'<br>'.($article->web_flag_karakteristike == 0 ? Support::string_format($article->web_karakteristike) : ''),$product);

		    $images   = $xml->createElement("slike");
		    foreach(Support::slike($article->roba_id) as $slika){
			    Support::xml_node($xml,"slika",explode('/',$slika)[3],$images);
			}
		    $product->appendChild($images);

		    $characteristics   = $xml->createElement("karakteristike");
		    foreach(self::characteristics($article->roba_id,$article->web_flag_karakteristike) as $karakteristika){
		    	$characteristic_group = $xml->createElement("karakteristike_grupa");
		    	$characteristic_group->setAttribute("ime", Support::string_format($karakteristika->group));
		    	
		    	foreach($karakteristika->characteristic as $karak){
		    		$karakteristika_xml = $xml->createElement("karakteristika");
		    		$karakteristika_xml->setAttribute("ime", Support::string_format($karak->name));
		    		Support::xml_node($xml,"vrednost",Support::string_format($karak->value),$karakteristika_xml);
		    		$characteristic_group->appendChild($karakteristika_xml);
		    	}
		    	$characteristics->appendChild($characteristic_group);
			}
		    $product->appendChild($characteristics);

			$root->appendChild($product);

		}

		$store_path = 'files/exporti/backup/backup_xml'.strval($offset+1).'.xml';

		$xml->formatOutput = true;
		$xml->save($store_path) or die("Error");
		return $store_path;	
	}

	public static function xls_exe($products){
        echo '<h2>Dati format nije podržan!</h2>';	
	}

	public static function characteristics($roba_id,$web_flag_karakteristike){
		$karak_arr = array();
		if($web_flag_karakteristike == 1){
			$generisane = DB::select("SELECT (SELECT naziv FROM grupa_pr_naziv WHERE grupa_pr_naziv_id = wrk.grupa_pr_naziv_id) as naziv, vrednost FROM web_roba_karakteristike wrk WHERE roba_id = ".$roba_id." ORDER BY rbr ASC");
			$karak_arr[0] = (object) array('group' => 'Osnovne karakteristike', 'characteristic' => array());
			foreach($generisane as $row){
				$karak_arr[0]->characteristic[] = (object) array('name' => $row->naziv, 'value' => $row->vrednost);
			}
		}
		elseif($web_flag_karakteristike == 2){
			$dobavljac = DB::select("SELECT karakteristika_grupa, karakteristika_naziv, karakteristika_vrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = ".$roba_id." ORDER BY dobavljac_cenovnik_karakteristike_id ASC");
			$check_groups = false;
			foreach($dobavljac as $row){
				if(!is_null($row->karakteristika_grupa) && $row->karakteristika_grupa != '' && (self::index_charac_group($karak_arr,$row->karakteristika_grupa) === false)){
					$karak_arr[] = (object) array('group' => $row->karakteristika_grupa, 'characteristic' => array());
					$check_groups = true;
				}else{
					if(!$check_groups && (self::index_charac_group($karak_arr,'Osnovne karakteristike') === false)){
						$karak_arr[] = (object) array('group' => 'Osnovne karakteristike', 'characteristic' => array());
					}
				}

				if(!is_null($row->karakteristika_grupa) && $row->karakteristika_grupa != ''){
					$karak_arr[self::index_charac_group($karak_arr,$row->karakteristika_grupa)]->characteristic[] = (object) array('name' => $row->karakteristika_naziv, 'value' => $row->karakteristika_vrednost);
				}else{
					$karak_arr[self::index_charac_group($karak_arr,'Osnovne karakteristike')]->characteristic[] = (object) array('name' => $row->karakteristika_naziv, 'value' => $row->karakteristika_vrednost);					
				}
			}
		}
		return $karak_arr;
	}

	public static function index_charac_group($charac_arr,$charac_group){
		foreach($charac_arr as $key => $value){
			if($charac_group == $value->group){
				return $key;
				break;
			}
		}
		return false;
	}

}