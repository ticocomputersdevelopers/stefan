<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminSupport;
use AdminCommon;
use DOMDocument;
use ZipArchive;
use File;
use All;
use Service\Mailer;

class Shopmania {

	public static function execute($export_id,$kind,$config){

		if(isset($config['auto_export'])) {
			$auto = 1;
		} else {
			$auto = 0;
		}

		if(isset($config['grupa_pr_ids'])){
			$group_ids= array();
			foreach($config['grupa_pr_ids'] as $gr_id){
				AdminCommon::allGroups($group_ids,$gr_id);
			}
			$grupa_query = " AND grupa_pr_id IN (".implode(",",$group_ids).")";
		}else{
			$group_ids= array();
			AdminCommon::allGroups($group_ids,0);
			$grupa_query = " AND grupa_pr_id IN (".implode(",",$group_ids).")";			
		}
		$product_count = DB::select("SELECT COUNT(roba_id) as count FROM roba WHERE flag_aktivan = 1 AND web_cena > 0".$grupa_query)[0]->count;
		$limit = 3000;
		$iter = intdiv($product_count,$limit);
		$mod = $product_count % $limit;
		if($mod > 0){
			$iter++;
		}

		if($auto == 1) {
			if($kind=='xml'){

				for($i = 1; $i < 15; $i++) {
					if(File::exists('files/exporti/shopmania/auto/shopmania-export'.$i.'.xml')){
	                	File::delete('files/exporti/shopmania/auto/shopmania-export'.$i.'.xml');
	            	}
				}

				foreach(range(0,$iter-1) as $val){			
					$export_products = DB::select("SELECT roba_id, sku, web_cena, mpcena, naziv_web, tezinski_faktor, web_flag_karakteristike, web_karakteristike, web_opis, grupa_pr_id, keywords, description, tags, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT SUM(kolicina) FROM roba LEFT JOIN lager ON lager.roba_id = roba.roba_id) AS kolicina FROM roba WHERE flag_aktivan = 1 AND web_cena > 0".$grupa_query." LIMIT ".$limit." OFFSET ".strval($val*$limit));

					self::xml_exe($export_products,$config,$val);
				}

				echo 'Uspešno je izvršen eksport, molimo Vas da sačuvate ove linkove: <br>';
				for($i = 1; $i < 30; $i++) {
					if(File::exists('files/exporti/shopmania/auto/shopmania-export'.$i.'.xml')){
	                	echo 'http://akcijskacena.tico.rs/files/exporti/shopmania/auto/shopmania-export'.$i.'.xml <br>';
	            	}
				}

			} else {
				echo '<h2>Dati format nije podržan!</h2>';
			}
		} else {
			if($kind=='xml'){
				$zipname = 'files/exporti/shopmania/shopmania-export.zip';
	            if(File::exists($zipname)){
	                File::delete($zipname);
	            }
				$zip = new ZipArchive;
				$zip->open($zipname, ZipArchive::CREATE);

				foreach(range(0,$iter-1) as $val){			
					$export_products = DB::select("SELECT roba_id, sku, web_cena, mpcena, naziv_web, tezinski_faktor, web_flag_karakteristike, web_karakteristike, web_opis, grupa_pr_id, keywords, description, tags, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT SUM(kolicina) FROM dobavljac_cenovnik dc WHERE dc.roba_id=roba.roba_id GROUP BY roba_id) AS kolicina FROM roba WHERE flag_aktivan = 1 AND web_cena > 0".$grupa_query." LIMIT ".$limit." OFFSET ".strval($val*$limit));

					$zip->addFile(self::xml_exe($export_products,$config,$val));
				}
				$zip->close();

				header('Content-Type: application/zip');
				header('Content-disposition: attachment; filename=shopmania-export.zip');
				header('Content-Length: ' . filesize($zipname));
				readfile($zipname);
			}else{
				echo '<h2>Dati format nije podržan!</h2>';
			}
		}
		

	}

	public static function xml_exe($products,$config,$offset){

		if(isset($config['auto_export'])) {
			$auto = 1;
		} else {
			$auto = 0;
		}

		$shopmania_grupe = self::shopmania_grupe();

		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("products");
		$xml->appendChild($root);
		foreach($products as $article){
			
			$product   = $xml->createElement("product");

		    Support::xml_node($xml,"key",$article->roba_id,$product);
			if(isset($article->sku)){ Support::xml_node($xml,"sku",$article->sku,$product); }
		    if(isset($config['name'])){ Support::xml_node($xml,"name",$article->naziv_web,$product); }
		    if(isset($config['category'])){ 
			    Support::xml_node($xml,"category", isset($shopmania_grupe[$article->grupa_pr_id]) ? $shopmania_grupe[$article->grupa_pr_id]->naziv : $article->grupa,$product);
			    Support::xml_node($xml,"full_category",!isset($shopmania_grupe[$article->grupa_pr_id]) ? self::puna_putanja_grupe($article->grupa_pr_id) : self::puna_putanja_grupe_shopmania($shopmania_grupe[$article->grupa_pr_id]->shopmania_grupa_id),$product);
			    $dodatne_kategorije = self::dodatne_kategorije($article->roba_id);
			    if(count($dodatne_kategorije)>0){		    	
				    foreach($dodatne_kategorije as $key => $grupa_id){
				    	Support::xml_node($xml,"secundary_category_".strval($key+1),isset($shopmania_grupe[$grupa_id]) ? $shopmania_grupe[$grupa_id] : DB::table('grupa_pr')->where('grupa_pr_id',$grupa_id)->pluck('grupa'),$product);
				    }
			    }
		    }
		    if(isset($config['category_photo'])){
		    	$category_photo = DB::table('grupa_pr')->where('grupa_pr_id',$article->grupa_pr_id)->pluck('putanja_slika');
		    	if(isset($category_photo)){
		    		Support::xml_node($xml,"category_photo",AdminOptions::base_url().$category_photo,$product);
		    	}
		    }
		    if(isset($config['manufacturer'])){ Support::xml_node($xml,"manufacturer",$article->proizvodjac,$product); }
		    if(isset($config['description_meta'])){ Support::xml_node($xml,"description_meta",strtolower(AdminSupport::seo_description($article->roba_id)),$product); }
		    if(isset($config['keywords'])){ Support::xml_node($xml,"keywords",self::kwards($article->keywords,$article->naziv_web),$product); }
		    if(isset($config['price'])){ Support::xml_node($xml,"price",round($article->web_cena),$product); }
		    if(isset($config['mp_price'])){
		    	if($article->mpcena > 0){
		    		Support::xml_node($xml,"market_price",round($article->mpcena),$product);
		    	}
		    }
		    if(isset($config['currency'])){ Support::xml_node($xml,"currency","RSD",$product); }

		    if(isset($config['photo'])){ 
			    $slike = self::slike($article->roba_id);;
			    if(count($slike)>0){		    	
				    foreach($slike as $key => $putanja){
			    		$sufix = "_".$key;
				    	if($key==0){
				    		$sufix = "";
				    	}
				    	Support::xml_node($xml,"photo".$sufix,AdminOptions::base_url().$putanja,$product);
				    }
			    }
		    }

		    if(isset($config['url'])){ Support::xml_node($xml,"url",Support::product_link($article->roba_id),$product); }
		    if(isset($config['availability'])){ Support::xml_node($xml,"availability",$article->kolicina > 0 ? 1000 : 0,$product); }
		    if(isset($config['stock'])){ Support::xml_node($xml,"mp",10,$product); }
		    if(isset($config['weight'])){ Support::xml_node($xml,"weight",$article->tezinski_faktor/1000,$product); }
		    if(isset($config['tags'])){ Support::xml_node($xml,"tags",$article->tags,$product); }
		    if(isset($config['additional_file'])){
		    	$addon = DB::table('web_files')->where('roba_id',$article->roba_id)->where('vrsta_fajla_id','!=',4)->orderBy('web_file_id','asc')->first();
		    	if(isset($addon)){
		    		Support::xml_node($xml,"additional_file",AdminOptions::base_url().$addon->putanja,$product);
		    	}
		    }
		    if(isset($config['product_video'])){
		    	$video_addon = DB::table('web_files')->where(array('roba_id'=>$article->roba_id,'vrsta_fajla_id'=>4))->orderBy('web_file_id','asc')->first();
		    	if(isset($video_addon)){
		    		Support::xml_node($xml,"product_video",AdminOptions::base_url().$video_addon->putanja,$product);
		    	}
		    }
			if(isset($article->web_opis)){
			if(isset($config['description'])){ Support::xml_node($xml,"description",str_replace("../../", AdminOptions::base_url(), $article->web_opis) ."\n".self::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product); }
			}else{
			if(isset($config['description'])){ Support::xml_node($xml,"description",str_replace("../../", AdminOptions::base_url(), $article->web_karakteristike) ."\n".self::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product); }
			}

			$root->appendChild($product);

		}

		$store_path = 'files/exporti/shopmania/auto/shopmania-export'.strval($offset+1).'.xml';

		$xml->formatOutput = true;
		$xml->save($store_path) or die("Error");
		return $store_path;

		// header('Content-type: text/xml');
		// header('Content-Disposition: attachment; filename="shopmania-export'.strval($offset+1).'.xml"');
		// echo file_get_contents($store_path);		
	}

	public static function characteristics($roba_id,$web_flag_karakteristike,$karakteristike){
		if($web_flag_karakteristike == 0){
			return '<p>'.$karakteristike.'</p>';
		}
		elseif($web_flag_karakteristike == 1){
			$generisane = DB::select("SELECT (SELECT naziv FROM grupa_pr_naziv WHERE grupa_pr_naziv_id = wrk.grupa_pr_naziv_id) as naziv, vrednost FROM web_roba_karakteristike wrk WHERE roba_id = ".$roba_id." ORDER BY rbr ASC");
			$generisane_karakteristike = '<table>';
			foreach($generisane as $row){
				$generisane_karakteristike .= '<tr><td>'.$row->naziv.'</td><td>'.$row->vrednost.'</td></tr>';
			}
			return $generisane_karakteristike.'</table>';
		}
		elseif($web_flag_karakteristike == 2){
			// $dobavljac = DB::select("SELECT karakteristika_naziv, karakteristika_vrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = ".$roba_id." ORDER BY dobavljac_cenovnik_karakteristike_id ASC");
			// $dobavljac_karakteristike = '<table>';
			// foreach($dobavljac as $row){
			// 	$dobavljac_karakteristike .= '<tr><td>'.$row->karakteristika_naziv.'</td><td>'.$row->karakteristika_vrednost.'</td></tr>';
			// }
			// return $dobavljac_karakteristike.'</table>';


			if(All::check_ewe($roba_id)==1){
		
				$query_type_ewe=DB::select("select Distinct karakteristika_grupa, redni_br_grupe from dobavljac_cenovnik_karakteristike where roba_id=? and redni_br_grupe>0 order by redni_br_grupe asc",array($roba_id));

				$dobavljac_karakteristike = '<div id="product_description" class="prod_desc" itemprop="description" style="margin-top: 20px;">
        <table style="width:600px;border-width: 1px;border-style: solid;border-color: #CCC;box-shadow: 1px 1px 10px #D1D1D1; BACKGROUND-COLOR:#ffffff;background-color: #FFF;border-radius: 5px;" class="tabela-karakteristike-generisana" cellspacing="0" cellpadding="0">';

				foreach($query_type_ewe as $grupa_kar){
				
					$karakteristika_grupa = $grupa_kar->karakteristika_grupa;
					
					$dobavljac_karakteristike .= '<tr><td style="padding:10px; font-family: Arial,Verdana,sans-serif;text-align: center; border: 1px solid #fff; background: #eee;font-size:16px">'.$karakteristika_grupa.'</td><td><table style="width: 100%;">';

					foreach(DB::table('dobavljac_cenovnik_karakteristike')->where(array('karakteristika_grupa'=>$karakteristika_grupa,'roba_id'=>$roba_id))->orderBy('dobavljac_cenovnik_karakteristike_id', 'asc')->get() as $row){
						$karakteristike_naziv = $row->karakteristika_naziv;
						$karakteristike_vrednost = $row->karakteristika_vrednost;

						$dobavljac_karakteristike .= '<tr><td style="padding: 5px 10px;font-family: Arial,Verdana,sans-serif;color: #1F1F1F;font-size: 12px;text-align: left;font-weight: bold; width: 50%;" class="celija-karakteristike-generisana-naziv">'.$karakteristike_naziv.' </td><td style="padding: 5px 10px;font-family: Arial,Verdana,sans-serif;color: #1F1F1F;font-size: 12px;text-align: left;font-weight: normal; width: 50%;" class="celija-karakteristike-generisana-vrednost">'.$karakteristike_vrednost.'</td></tr>';

						}

					$dobavljac_karakteristike .= '</table></td><tr>';
					
				}

				$dobavljac_karakteristike .= '</table></div>';

				return $dobavljac_karakteristike;

			} else {
				$dobavljac_karakteristike = '';
				$dobavljac_karakteristike .= '<table>';
				foreach(DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id)->distinct()->orderBy('dobavljac_cenovnik_karakteristike_id', 'asc')->get() as $row){
				$karakteristike_naziv = $row->karakteristika_naziv;
				$karakteristike_vrednost = $row->karakteristika_vrednost;
				$dobavljac_karakteristike .= "<tr><td>".$karakteristike_naziv."</td><td>".$karakteristike_vrednost."</td></tr>";
				}
				$dobavljac_karakteristike .= "</table>";

				return $dobavljac_karakteristike;
			}

		}		
	}

	public static function slike($roba_id){
        return array_map('current',DB::table('web_slika')->select('putanja')->where('roba_id',$roba_id)->limit(8)->orderBy('akcija','desc')->get());	
	}

	public static function puna_putanja_grupe($grupa_pr_id,$link=''){
        $first = DB::table('grupa_pr')->where(array('grupa_pr_id'=>$grupa_pr_id))->first();
        if($link!=''){
        	$link = $first->grupa.'/'.$link;
        }else{
        	$link = $first->grupa;
        }
        if($first->parrent_grupa_pr_id != 0){
        	$link = self::puna_putanja_grupe($first->parrent_grupa_pr_id,$link);
        }
        return $link;
	}
	public static function puna_putanja_grupe_shopmania($shopmania_grupa_id,$link=''){
        $first = DB::table('shopmania_grupe')->where(array('shopmania_grupa_id'=>$shopmania_grupa_id))->first();
        if($link!=''){
        	$link = $first->naziv.'/'.$link;
        }else{
        	$link = $first->naziv;
        }
        if($first->parent_id != 0){
        	$link = self::puna_putanja_grupe_shopmania($first->parent_id,$link);
        }
        return $link;
	}
	public static function shopmania_grupe(){
        $results = array();
        foreach(DB::table('shopmania_grupe')->get() as $row){
        	if(isset($row->grupa_pr_ids)){
        		foreach(explode(',',$row->grupa_pr_ids) as $grupa_pr_id){
        			$results[$grupa_pr_id] = (object) array('shopmania_grupa_id' => $row->shopmania_grupa_id, 'naziv' => $row->naziv);
        		}
        	}
        }
        return $results;
	}

	public static function dodatne_kategorije($roba_id){
        return array_map('current',DB::table('roba_grupe')->select('grupa_pr_id')->where('roba_id',$roba_id)->limit(5)->get());
	}
	public static function kwards($keywords,$naziv_web){
		if(isset($keywords)){
			return $keywords;
		}
		else{
			$naziv_arr = explode(' ',str_replace(array(',',')','('),array('','',''),$naziv_web));
			return strtolower(implode(', ',array_diff($naziv_arr,array(' '))));
		}
	}
	public static function shopmania_our_groups(){
		$shpm_gr_str = '';
		foreach (DB::table('shopmania_grupe')->get() as $row) {
			if(isset($row->grupa_pr_ids)){
				$shpm_gr_str .= trim($row->grupa_pr_ids) . ",";
			}
		}
		return explode(',',$shpm_gr_str);
	}


	public static function execute_auto(){

		$auto = 1;
		$group_ids= array();
		AdminCommon::allGroups($group_ids,0);
		$grupa_query = " AND grupa_pr_id IN (".implode(",",$group_ids).")";			

		$product_count = DB::select("SELECT COUNT(roba_id) as count FROM roba WHERE flag_aktivan = 1 AND web_cena > 0".$grupa_query)[0]->count;

		$limit = 3000;
		$iter = intdiv($product_count,$limit);
		$mod = $product_count % $limit;

		if($mod > 0){
			$iter++;
		}

		for($i = 1; $i < 30; $i++) {
			if(File::exists('files/exporti/shopmania/auto/shopmania-export'.$i.'.xml')){
            	File::delete('files/exporti/shopmania/auto/shopmania-export'.$i.'.xml');
        	}
		}

		foreach(range(0,$iter-1) as $val){			
			$export_products = DB::select("SELECT roba_id, sku, web_cena, mpcena, naziv_web, tezinski_faktor, web_flag_karakteristike, web_karakteristike, web_opis, grupa_pr_id, keywords, description, tags, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT SUM(kolicina) FROM roba LEFT JOIN lager ON lager.roba_id = roba.roba_id) AS kolicina FROM roba WHERE flag_aktivan = 1 AND web_cena > 0".$grupa_query." LIMIT ".$limit." OFFSET ".strval($val*$limit));

			self::xml_exe_auto($export_products,$val);
		}

		$report = 'Uspešno je izvršen eksport, molimo Vas da sačuvate ove linkove: <br>';
		for($i = 1; $i < 40; $i++) {
			if(File::exists('files/exporti/shopmania/auto/shopmania-export'.$i.'.xml')){
            	$report .= AdminOptions::base_url().'files/exporti/shopmania/auto/shopmania-export'.$i.'.xml <br>';
        	}
		}

		echo $report;

		// Šalje mail posle exporta
		$subject = 'Automatski Export - '.AdminOptions::company_name();
		$content = $report;

		 Mailer::send(AdminOptions::company_email(),'info@tico.rs', $subject, $content);


	}

	public static function xml_exe_auto($products,$offset){

		$auto = 1;
		$shopmania_grupe = self::shopmania_grupe();

		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("products");
		$xml->appendChild($root);

		foreach($products as $article){
			
			$product   = $xml->createElement("product");

		    Support::xml_node($xml,"key",$article->roba_id,$product);
			// if(isset($article->sku)){ Support::xml_node($xml,"sku",$article->sku,$product); }
		    Support::xml_node($xml,"name",$article->naziv_web,$product);

		    Support::xml_node($xml,"category", isset($shopmania_grupe[$article->grupa_pr_id]) ? $shopmania_grupe[$article->grupa_pr_id]->naziv : $article->grupa,$product);
		    Support::xml_node($xml,"full_category",!isset($shopmania_grupe[$article->grupa_pr_id]) ? self::puna_putanja_grupe($article->grupa_pr_id) : self::puna_putanja_grupe_shopmania($shopmania_grupe[$article->grupa_pr_id]->shopmania_grupa_id),$product);
		    $dodatne_kategorije = self::dodatne_kategorije($article->roba_id);
		    if(count($dodatne_kategorije)>0){		    	
			    foreach($dodatne_kategorije as $key => $grupa_id){
			    	Support::xml_node($xml,"secundary_category_".strval($key+1),isset($shopmania_grupe[$grupa_id]) ? $shopmania_grupe[$grupa_id] : DB::table('grupa_pr')->where('grupa_pr_id',$grupa_id)->pluck('grupa'),$product);
			    }
		    }
		    
		    // if(isset($config['category_photo'])){
		    // 	$category_photo = DB::table('grupa_pr')->where('grupa_pr_id',$article->grupa_pr_id)->pluck('putanja_slika');
		    // 	if(isset($category_photo)){
		    // 		Support::xml_node($xml,"category_photo",AdminOptions::base_url().$category_photo,$product);
		    // 	}
		    // }

			Support::xml_node($xml,"manufacturer",$article->proizvodjac, $product);
		    Support::xml_node($xml,"description_meta", $article->description, $product);
		   	Support::xml_node($xml,"keywords", $article->keywords, $product);
		    Support::xml_node($xml,"price",round($article->web_cena),$product);

	    	if($article->mpcena > 0){
	    		Support::xml_node($xml,"market_price",round($article->mpcena),$product);
	    	}

		    Support::xml_node($xml,"currency","RSD",$product); 

		    $slike = self::slike($article->roba_id);
		    if(count($slike)>0){		    	
			    foreach($slike as $key => $putanja){
		    		$sufix = "_".$key;
			    	if($key==0){
			    		$sufix = "";
			    	}
			    	Support::xml_node($xml,"photo".$sufix,AdminOptions::base_url().$putanja,$product);
			    }
		    }


		    // if(isset($config['url'])){ Support::xml_node($xml,"url",Support::product_link($article->roba_id),$product); }
		    Support::xml_node($xml,"availability",$article->kolicina > 0 ? 1000 : 0,$product);
		    Support::xml_node($xml,"mp",10,$product);
		    Support::xml_node($xml,"weight",$article->tezinski_faktor/1000,$product); 
		    Support::xml_node($xml,"tags",$article->tags,$product); 
		    // if(isset($config['additional_file'])){
		    // 	$addon = DB::table('web_files')->where('roba_id',$article->roba_id)->where('vrsta_fajla_id','!=',4)->orderBy('web_file_id','asc')->first();
		    // 	if(isset($addon)){
		    // 		Support::xml_node($xml,"additional_file",AdminOptions::base_url().$addon->putanja,$product);
		    // 	}
		    // }
		    // if(isset($config['product_video'])){
		    // 	$video_addon = DB::table('web_files')->where(array('roba_id'=>$article->roba_id,'vrsta_fajla_id'=>4))->orderBy('web_file_id','asc')->first();
		    // 	if(isset($video_addon)){
		    // 		Support::xml_node($xml,"product_video",AdminOptions::base_url().$video_addon->putanja,$product);
		    // 	}
		    // }
			if(isset($article->web_opis)){
			Support::xml_node($xml,"description",str_replace("../../", AdminOptions::base_url(), $article->web_opis) ."\n".self::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product);
			}else{
			Support::xml_node($xml,"description",str_replace("../../", AdminOptions::base_url(), $article->web_karakteristike) ."\n".self::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product);
			}			
			$root->appendChild($product);
		}

		$store_path = 'files/exporti/shopmania/auto/shopmania-export'.strval($offset+1).'.xml';

		$xml->formatOutput = true;
		$xml->save($store_path) or die("Error");
		return $store_path;
	}


}