<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;
use AdminGroups;
use AdminSeo;

class Google {

  public static function execute($export_id,$kind,$short=false){
    
    $export_products = DB::select("SELECT * FROM roba WHERE flag_aktivan = 1 AND flag_prikazi_u_cenovniku=1 AND web_cena>0 ORDER BY roba_id ASC");

    if($kind=='csv'){
      return self::csv_exe($export_products);
    }else{
      echo '<h2>Dati format nije podržan!</h2>';
    }

  }

  public static function csv_exe($export_products){

        header('Content-Type: text/csv charset=UTF-8');
        header('Content-Disposition: attachment; filename="google.csv"');
        $results = '';
        echo 'ID'."," . 'ID2'. "," . 'Item title'. "," . 'Final URL'."," . 'Image URL'."," . 'Item subtitle'."," . 'Item description'."," .'Item category'."," .'Price'."," .'Sale price'."," .'Contextual keywords'."," .'Item address'."," .'Tracking template'."," .'Custom parameter'."," .'Final mobile URL'."," .'Android app link'."," .'iOS app link'."," .'iOS app store ID'.",,,,,,,,,,,,,,,,\n";
        
        
        
        foreach($export_products as $article){
            $key = $article->roba_id;
           
            // if($key==3377){
            //   var_dump(number_format($article->web_cena,2,'','.'));die;
            // }


            $sifra_d = $article->sifra_d;
            $dobavljac = $article->dobavljac_id;
            // $name = substr($article->naziv_web, 0,25);
            $name = "";
            foreach (explode(" ",$article->naziv_web) as $naziv) {
              if((strlen($name) + strlen($naziv) + 1) <= 25) {
                $name .= $naziv." ";
              } elseif((strlen($name) + strlen($naziv)) <= 25) {
                $name .= $naziv;
              } 
            }
           // $name = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$name);
            $naziv_disp = "";
            foreach (explode(" ",$article->naziv_displej) as $naziv) {
              if((strlen($naziv_disp) + strlen($naziv) + 1) <= 25) {
                $naziv_disp .= $naziv." ";
              } elseif((strlen($naziv_disp) + strlen($naziv)) <= 25) {
                $naziv_disp .= $naziv;
              } 
            }
            //$naziv_disp = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$naziv_disp);
            $opis =  substr($article->web_karakteristike, 0,25);
            $opis = preg_replace('/[^A-Za-z0-9\-]/', '', $opis);
            if (empty($opis)) {
              $opis = "";
            }
            $name =str_replace(array("&scaron;",";"),array("š",""), $name);
            
            $category = AdminGroups::find($article->grupa_pr_id,'grupa');
            
           // $web_cena=str_replace(",","",$article->web_cena);
            $price = number_format($article->web_cena,2,'.','');

            $akc_cena = str_replace(",","",$article->akcijska_cena); 
            $akcijska_cena = $article->akcija_flag_primeni = 1 ? $akc_cena : '0.00';
            if(number_format($akcijska_cena,2,'.','') != '0.00') {
              $akcijska_cena = number_format($akcijska_cena,2,'.','').' RSD';
            } else {
              $akcijska_cena = "";
            }
            $vidljivost = "";
     
            echo ($key.", ".str_replace(",",".",$sifra_d).", ".str_replace(",",".",ltrim(pg_escape_string($name),"-")).",".Support::product_link($key).",".Support::major_image($key).",".str_replace(",",".",$naziv_disp).",".str_replace(",", "", $opis).",".str_replace(",", "", $category).",".$price.' RSD'.",".$akcijska_cena.",".$vidljivost.",".$vidljivost.",".",".",".",".",".","."\n");
        }
         die;
      
  }

}