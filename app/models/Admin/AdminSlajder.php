<?php

class AdminSlajder {

	public static function sliderTypes(){
		return DB::table('slajder_tip')->where('flag_aktivan',1)->orderBy('rbr','asc')->get();
	}

	// public static function getSliderType($type_id){
	// 	return DB::table('slajder_tip')->where('slajder_tip_id',$type_id)->first();
	// }

	public static function getSliderTypeBySliderId($slider_id){
		return DB::table('slajder_tip')->where('slajder_tip_id',DB::table('slajder')->where('slajder_id',$slider_id)->pluck('slajder_tip_id'))->first();
	}

	// public static function sliders(){
	// 	return DB::table('slajder')->where('flag_aktivan',1)->orderBy('rbr','asc')->get();
	// }

	public static function sliderItems($slider_id){
		return DB::table('slajder_stavka')->where(['slajder_id'=>$slider_id])->orderBy('rbr','asc')->get();
	}

	public static function sliderItem($slider_item_id){
		return DB::table('slajder_stavka')->where(['slajder_stavka_id'=>$slider_item_id])->first();
	}

	public static function sliderItemLang($slider_item_id,$lang_id){
		return DB::table('slajder_stavka_jezik')->where(['slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$lang_id])->first();
	}


}