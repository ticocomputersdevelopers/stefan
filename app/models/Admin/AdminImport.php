<?php
use Import\Support;
use Import\Kimtec;
use Import\Computerland;

class AdminImport {

    public static function dobavljaci_web_import()
    {
        return DB::table('dobavljac_cenovnik_kolone')->join('partner', 'dobavljac_cenovnik_kolone.partner_id', '=', 'partner.partner_id')->select('dobavljac_cenovnik_kolone.partner_id', 'partner.naziv')->where('sifra_attribute_naziv', null)->where('dobavljac_cenovnik_kolone.partner_id','!=','-1')->get();
    }

    public static function fetchAll_Import($criteria, $pagination=null, $sort=null)
    {   
        
        $select="SELECT dobavljac_cenovnik_id, sku, roba_id, flag_slika_postoji,sifra_kod_dobavljaca, partner_id, flag_opis_postoji,web_flag_karakteristike, povezan,  (select naziv from partner pr where pr.partner_id = dc.partner_id) AS dobavljac_naziv, naziv, kolicina, (select porez from tarifna_grupa tg where tg.tarifna_grupa_id=dc.tarifna_grupa_id ) AS tarifna_grupa_porez,  web_cena, web_marza, cena_nc, pmp_cena, flag_prikazi_u_cenovniku, mp_marza, mpcena, flag_aktivan, flag_zakljucan,grupa, podgrupa, model, (select grupa from grupa_pr gp where gp.grupa_pr_id= dc.grupa_pr_id) AS nasa_grupa, proizvodjac, (select naziv from proizvodjac pr where pr.proizvodjac_id=dc.proizvodjac_id) AS nas_proizvodjac, cena_a, a_marza FROM dobavljac_cenovnik dc";    
        $where=" where dobavljac_cenovnik_id<>-1";    

        if(isset($criteria['proizvodjac']) && $criteria['proizvodjac'] != '0'){
            $proizvodjaci = str_replace("+", ", ", $criteria['proizvodjac']);
            $where .= " AND proizvodjac_id IN (". $proizvodjaci .")";
        }

        if(isset($criteria['dobavljac']) && $criteria['dobavljac'] != '0'){
            $dobavljaci = str_replace("-", ", ", $criteria['dobavljac']);
            $where .= " AND partner_id IN (". $dobavljaci .")";
        }

        if(isset($criteria['grupa_pr_id'])  && $criteria['grupa_pr_id'] != '0'){
            // $grupa_pr_id = str_replace("-", ", ", $criteria['grupa_pr_id']);
            if ($criteria['grupa_pr_id'] == -2) {
                $criteria['grupa_pr_id'] = -1;
            }
            $grupa_pr_id = $criteria['grupa_pr_id'];
            $sve_grupe_arr = array();
            AdminCommon::allGroups($sve_grupe_arr,$grupa_pr_id);
            $where .= " AND grupa_pr_id IN (". implode(',',$sve_grupe_arr) .")";
        }

        if(isset($criteria['filteri'])){
            $filteri = explode("-", $criteria['filteri']);
            if($filteri[0] == "0"){
                $where .= " AND flag_opis_postoji = 0";
            }
            if($filteri[0] == "1"){
                $where .= " AND flag_opis_postoji = 1";
            }
            if($filteri[1] == "0"){
                $where .= " AND web_flag_karakteristike <> 0";
            }
            if($filteri[1] == "1"){
                $where .= " AND web_flag_karakteristike = 0";
            }
            if($filteri[2] == "0"){
                $where .= " AND web_flag_karakteristike <> 2";
            }
            if($filteri[2] == "1"){
                $where .= " AND web_flag_karakteristike = 2";
            }
            if($filteri[3] == "0"){
                $where .= " AND flag_slika_postoji = 0";
            }
            if($filteri[3] == "1"){
                $where .= " AND flag_slika_postoji = 1";
            }
            if($filteri[4] == "0"){
                $where .= " AND kolicina = 0";
            }
            if($filteri[4] == "1"){
                $where .= " AND kolicina > 0";
            }
            if($filteri[5] == "0"){
                $where .= " AND flag_aktivan = 0";
            }
            if($filteri[5] == "1"){
                $where .= " AND flag_aktivan = 1";
            }
            if($filteri[6] == "0"){
                $where .= " AND roba_id = -1";
            }
            if($filteri[6] == "1"){
                $where .= " AND roba_id <> -1";
            }
            if($filteri[7] == "0"){
                $where .= " AND flag_prikazi_u_cenovniku = 0";
            }
            if($filteri[7] == "1"){
                $where .= " AND flag_prikazi_u_cenovniku = 1";
            }
            if($filteri[8] == "0"){
                $where .= " AND dc.roba_id <> -1 AND (SELECT racunska_cena_nc FROM roba r WHERE r.roba_id=dc.roba_id) = dc.cena_nc";
            }
            if($filteri[8] == "1"){
                $where .= " AND dc.roba_id <> -1 AND (SELECT racunska_cena_nc FROM roba r WHERE r.roba_id=dc.roba_id) <> dc.cena_nc";
            }
            if($filteri[9] == "0"){
                $where .= " AND dc.roba_id <> -1 AND ((SELECT SUM(kolicina) FROM lager l WHERE l.roba_id=dc.roba_id GROUP BY l.roba_id) < 1 OR dc.roba_id NOT IN (SELECT roba_id FROM lager))";
            }
            if($filteri[9] == "1"){
                $where .= " AND dc.roba_id <> -1 AND (SELECT SUM(kolicina) FROM lager l WHERE l.roba_id=dc.roba_id GROUP BY l.roba_id) > 0";
            }
            if($filteri[10] == "0"){
                $where .= " AND roba_id <> -1";
            }
            if($filteri[10] == "1"){
                $where .= " AND dc.roba_id <> -1 AND dc.dobavljac_cenovnik_id = (SELECT dobavljac_cenovnik_id FROM dobavljac_cenovnik WHERE roba_id = dc.roba_id ORDER BY cena_nc ASC LIMIT 1)";
            }
            if($filteri[11] == "0"){
                $where .= "";
            }
            if($filteri[11] == "1"){
                $where .= " AND povezan = 1 AND (SELECT COUNT(roba_id) FROM dobavljac_cenovnik WHERE roba_id = dc.roba_id AND roba_id<>-1) > 1";
            }
        }

        if(isset($sort)){
            $sort = explode("-", $sort);
            $order = " ORDER BY ".$sort[0]." ".$sort[1]."";
        }else{
            $order = " ORDER BY dobavljac_cenovnik_id DESC";
        }
        
        if(isset($criteria['search']) && $criteria['search'] != '0'){
            $search_str = $criteria['search'];

                $input_arr = explode('+',$search_str);
                $search_arr = $input_arr;
                foreach($input_arr as $word){
                    $search_arr[] = strtoupper($word);
                }
                $search_arr = array_unique($search_arr);
                $where .= " AND (";

                $where .= AdminSupport::searchQueryString($search_arr,'roba_id::varchar');
                $where .= " OR ";
                $where .= AdminSupport::searchQueryString($search_arr,'naziv');
                $where .= " OR ";
                $where .= AdminSupport::searchQueryString($search_arr,'sku');
                $where .= " OR ";
                $where .= AdminSupport::searchQueryString($search_arr,'dc.grupa');
                $where .= " OR ";
                $where .= AdminSupport::searchQueryString($search_arr,'dc.podgrupa');
                $where .= " OR ";
                $where .= AdminSupport::searchQueryString($search_arr,'dc.proizvodjac');
                $where .= " OR ";
                $where .= AdminSupport::searchQueryString($search_arr,'sifra_kod_dobavljaca');
                $where .= ")";
            
        }

        if(isset($criteria['nabavna'])){
            $nabavna_arr = explode('-',$criteria['nabavna']);
            if(is_numeric($nabavna_arr[0]) && is_numeric($nabavna_arr[1])){
                $where .= " AND cena_nc BETWEEN ".$nabavna_arr[0]." AND ".$nabavna_arr[1]."";
            }
        }

        if(is_array($pagination)){
            $pagination = " LIMIT ".$pagination['limit']." OFFSET ".$pagination['offset']."";
        }else{
            $pagination = "";
        }

        return DB::select($select.$where.$order.$pagination);
    }
    
    public static function dobavljaci_web_import_tabela()
    {
        return DB::table('dobavljac_cenovnik_kolone')->join('partner', 'dobavljac_cenovnik_kolone.partner_id', '=', 'partner.partner_id')->select('dobavljac_cenovnik_kolone.partner_id', 'partner.naziv')->where('dobavljac_cenovnik_kolone.partner_id','!=','-1')->get();
    }
    
    // public static function dobavljac_cenovnik_slike($sifra_kod_dobavljaca)
    // {
    //     return DB::table('dobavljac_cenovnik_slike')->join('dobavljac_cenovnik', 'dobavljac_cenovnik_slike.sifra_kod_dobavljaca', '=', 'dobavljac_cenovnik.sifra_kod_dobavljaca')->where('dobavljac_cenovnik.sifra_kod_dobavljaca',$sifra_kod_dobavljaca)->get();
    // }

    public static function saveImages($slike,$roba_id=null)
    {
        if(count($slike) > 0){
            if(is_null($roba_id)){
                $roba_id = DB::select("SELECT currval('roba_roba_id_seq')")[0]->currval;
            }
            $sirina_slike = DB::table('options')->where('options_id',1331)->pluck('int_data');
            foreach($slike as $row){
                try { 
                    $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
                    if(Support::checkPartner($row->partner_id,'import_computerland') || Support::checkPartner($row->partner_id,'import_uspon')){
                    $name = $slika_id.'.jpg';
                    }else{
                    $name = $slika_id.'.'.self::fileExtension($row->putanja);
                    }
                    $destination = 'images/products/big/'.$name;
                    $url = $row->putanja;
                    $akcija = $row->akcija;
                    $dobavljac = $row->partner_id;
                    $sifra= $row->sifra_kod_dobavljaca;

                    if(Support::checkPartner($row->partner_id,'import_kimtec')){
                        Kimtec::getKimtecImageContent($row->partner_id,$url,$destination);
                    }
                    elseif(Support::checkPartner($row->partner_id,'import_computerland')){
                        self::getComputerlandImageContent($url,$destination);
                    }
                    elseif(Support::checkPartner($row->partner_id,'import_rabalux')){
                        self::getRabaluxImageContent($url,$destination);
                    }
                    else{
                        $content = @file_get_contents($url);
                        file_put_contents($destination,$content);
                    }

                    $insert_data = array(
                        'web_slika_id'=>intval($slika_id),
                        'roba_id'=>intval($roba_id),
                        'akcija'=>$akcija, 
                        'dobavljac_id'=>$dobavljac, 
                        'sifra_kod_dobavljaca'=>$sifra,
                        'flag_prikazi'=>1,
                        'putanja'=>'images/products/big/'.$name
                        );

                    Image::make($destination)->resize($sirina_slike, null, function ($constraint){ $constraint->aspectRatio(); })->save();
                //    Image::make($destination)->resize(DB::table('options')->where('options_id',1333)->pluck('int_data'), null, function ($constraint){ $constraint->aspectRatio(); })->save('images/products/small/'.$name);

                    DB::table('web_slika')->insert($insert_data);
 
                }
                catch (Exception $e) {
                }
            }
        }
    }
    public static function getComputerlandImageContent($image,$destination){
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        ); 
        $content = file_get_contents($image,false,stream_context_create($arrContextOptions));
        // $fp = fopen(str_replace('.jpg','.png',$destination), "w");
        $fp = fopen($destination, "w");
        fwrite($fp, $content); 
        fclose($fp);
    }
    public static function getRabaluxImageContent($image, $destination) {
        $ch = curl_init($image);
        $fp = fopen($destination, "wb");

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $result = curl_exec($ch);

        curl_close($ch);
        fclose($fp);
    }
    public static function saveArticlesImages($roba_ids)
    {
        foreach($roba_ids as $roba_id){
            $dc = DB::table('dobavljac_cenovnik')->select('sifra_kod_dobavljaca','partner_id')->where('roba_id',$roba_id)->first();
            if(!is_null($dc) && !is_null($dc->sifra_kod_dobavljaca) && $dc->sifra_kod_dobavljaca != '' && $dc->partner_id > 0){
                $slike = DB::table('dobavljac_cenovnik_slike')->select('partner_id','putanja','akcija','sifra_kod_dobavljaca')->where(array('sifra_kod_dobavljaca'=>$dc->sifra_kod_dobavljaca, 'partner_id'=> $dc->partner_id))->get();
                self::saveImages($slike,$roba_id);
            }
        }
    }
    public static function dc_mapped($dc_ids,$all=false,$groups=true,$manufacturers=true,$others=true){
        if(count($dc_ids) == 0){
            return (object) array('dc_ids' => array());
        }
        $db_cnvs = DB::select("SELECT dobavljac_cenovnik_id, roba_id, povezan, flag_zakljucan, grupa_pr_id, proizvodjac_id, flag_aktivan, partner_id, grupa, podgrupa, proizvodjac, web_marza, mp_marza, tarifna_grupa_id, cena_nc FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id IN (".implode(',',$dc_ids).") AND roba_id = -1 AND povezan = 0 AND flag_zakljucan = 0");
        $opseg = self::definisana_marza_opseg();

        $dc_ids = array();
        
        foreach ($db_cnvs as $db_cnv) {
            $partner_grupa = DB::table('partner_grupa')->where(array('partner_id'=>$db_cnv->partner_id,'grupa'=>AdminImport::mapped_groups($db_cnv->grupa,$db_cnv->podgrupa)))->first();
            $partner_proizvodjac = DB::table('partner_proizvodjac')->where(array('partner_id'=>$db_cnv->partner_id,'proizvodjac'=>$db_cnv->proizvodjac))->first();

            $data = array();
            $web_cena_update = "";
            $mp_cena_update = "";

            if($others){
                if($db_cnv->flag_aktivan == 0 || $all){
                    $data['flag_aktivan'] = 1;
                }
                if($db_cnv->tarifna_grupa_id == -1 || $all){
                    $tarifna_grupa_id = DB::table('tarifna_grupa')->where('porez',20)->pluck('tarifna_grupa_id');
                    $data['tarifna_grupa_id'] = !is_null($tarifna_grupa_id) ? $tarifna_grupa_id : 0;
                }
            }

            if($groups && !is_null($partner_grupa)){
                if($db_cnv->grupa_pr_id == -1 || $all){
                    $data['grupa_pr_id'] = $partner_grupa->grupa_pr_id;
                }
                if($db_cnv->web_marza == 0 || $all){
                    $data['web_marza'] = ($partner_grupa->definisane_marze == 0) ? $partner_grupa->web_marza : self::find_definisana_marza($opseg,$db_cnv->cena_nc)->web_marza;
                    $web_cena_update = " web_cena = ROUND((dc.cena_nc+(dc.cena_nc*dc.web_marza/100))*((select porez from tarifna_grupa tg where tg.tarifna_grupa_id=dc.tarifna_grupa_id)/100+1))";
                }
                if($db_cnv->mp_marza == 0 || $all){
                    $data['mp_marza'] = ($partner_grupa->definisane_marze == 0) ? $partner_grupa->mp_marza : self::find_definisana_marza($opseg,$db_cnv->cena_nc)->mp_marza;
                    $mp_cena_update = " mpcena = ROUND((dc.cena_nc+(dc.cena_nc*dc.mp_marza/100))*((select porez from tarifna_grupa tg where tg.tarifna_grupa_id=dc.tarifna_grupa_id)/100+1))";
                }
            }

            if($manufacturers && !is_null($partner_proizvodjac) && ($db_cnv->proizvodjac_id == -1 || $all)){
                $data['proizvodjac_id'] = $partner_proizvodjac->proizvodjac_id;
            }

            if(count($data)>0){
                DB::table('dobavljac_cenovnik')->where(array('dobavljac_cenovnik_id'=>$db_cnv->dobavljac_cenovnik_id))->update($data);

                //cene update
                $cena_upd_string = $web_cena_update != "" ? ($web_cena_update . ($mp_cena_update != "" ? ",".$mp_cena_update : "")) : ($mp_cena_update != "" ? $mp_cena_update : "");
                if($cena_upd_string != ""){
                    DB::statement("UPDATE dobavljac_cenovnik dc SET".$cena_upd_string." where dc.dobavljac_cenovnik_id=".$db_cnv->dobavljac_cenovnik_id." and dc.flag_zakljucan = 0");
                }
            }

            $db_cnv_upd = DB::table('dobavljac_cenovnik')->select('grupa_pr_id','proizvodjac_id')->where(array('dobavljac_cenovnik_id'=>$db_cnv->dobavljac_cenovnik_id))->first();
            if($db_cnv_upd->grupa_pr_id != -1 && $db_cnv_upd->proizvodjac_id != -1){
                $dc_ids[] = $db_cnv->dobavljac_cenovnik_id;
            }

            $data = array();
        }

        return (object) array('dc_ids' => $dc_ids);
    }
    public static function dc_mapped_import($partner_id){
        $dc_ids = array_map('current',DB::select("SELECT dobavljac_cenovnik_id FROM dobavljac_cenovnik WHERE partner_id = ".$partner_id." AND roba_id = -1 AND povezan = 0 AND flag_zakljucan = 0"));
        self::dc_mapped($dc_ids);
    }

    public static function instant_import_to_roba($partner_id){
        $dc_ids = array_map('current',DB::select("SELECT dobavljac_cenovnik_id FROM dobavljac_cenovnik WHERE partner_id = ".$partner_id." AND roba_id = -1 AND povezan = 0 AND flag_aktivan = 1 AND kolicina > 0"));

        $url = AdminOptions::base_url() . 'admin/ajax/dc_articles';
        $post_array = array(
            'action'=> 'unesi_nove',
            'flag'=>1,
            'fast_insert'=>1,
            'dc_ids'=> $dc_ids
            );

        Input::merge($post_array);
        $result = json_decode(App::make('AdminAjaxController')->ajaxDcArticles());
        return $result->roba_ids;
    }

    public static function mapped_groups($grupa,$podgrupa){
        if(!is_null($podgrupa) && $podgrupa != ''){
            // return htmlspecialchars($grupa,ENT_QUOTES).' -> '.htmlspecialchars($podgrupa,ENT_QUOTES);
            return $grupa.' -> '.$podgrupa;
        }
        // return htmlspecialchars($grupa,ENT_QUOTES);
        return $grupa;
    }

    public static function rowColors($aktivan,$povezan){
         if($aktivan == 0){
            return 'text-red';
         }
         elseif($povezan == 1){
            return 'text-green';
         }
         else{
            return '';
         }
    }

    public static function definisana_marza_opseg(){
        $opseg = array();
        $definisane_marze = DB::table('definisane_marze')->orderBy('definisane_marze_id','asc')->get();
        foreach($definisane_marze as $key => $definisane_marza){
            $opseg[] = (object) array(
                'from_nc' => ($key == 0) ? 0 : $definisane_marze[$key-1]->nc,
                'nc' => $definisane_marza->nc,
                'web_marza' => $definisane_marza->web_marza,
                'mp_marza' => $definisane_marza->mp_marza
            );
        }
        return $opseg;
    }

    public static function find_definisana_marza($opseg,$nabavna_cena){
        if(count($opseg) == 0){
            return (object) array('from_nc' => 0,'nc' => 0,'web_marza' => 0,'mp_marza' => 0);
        }
        foreach($opseg as $definisane_marza){
            if($nabavna_cena < $definisane_marza->nc){
                return $definisane_marza;
                break;
            }
        }
        return $opseg[count($opseg)-1];
    }
    public static function fileExtension($image_path){
        $extension = 'jpg';
        $slash_parts = explode('/',$image_path);
        $old_name = $slash_parts[count($slash_parts)-1];
        $old_name_arr = explode('.',$old_name);
        $extension = $old_name_arr[count($old_name_arr)-1];
        return $extension;
    }
}
