<?php
namespace Import;
use Import\Support;
use DB;
use File;
use AdminOptions;

class CTCUnit {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){ 
	if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/ctcunit/ctcunit_json/ctcunit.json');
			$products_file = "files/ctcunit/ctcunit_json/ctcunit.json";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
	        
	        foreach($products as $product_arr) {
	        	$product = (object) $product_arr;
	        	$sifra = $product->Identifier;

	        	$category = explode('/', $product->Category);
	        	if(isset($category[0]))
	        	{
	        		$grupa = $category[0];
	        	}
	        	if(isset($category[1]))
	        	{
	        		$podgrupa = $category[1];
	        	}
	        	$naziv = $product->Name;
	        	$opis = $product->Description;
	        	$kolicina = $product->Stock;
	        	$cena_nc = (float) $product->Price;
	        	if(isset($product->ImageUlr)) {
	        		$slika = "https://".str_replace("\\", "/", $product->ImageUlr);
	        	}
	        	$karakteristike = $product->ItemAttributes;
	        	$proizvodjac = "";

				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0){					

	        	foreach ($karakteristike as $karakteristika) {
	        	 	
	        	 	$naziv_karakteristike = $karakteristika['Name'];
	        	 	$vrednost = $karakteristika['Value'];
	        	 	if ($naziv_karakteristike == 'BREND') {
	        	 		$proizvodjac = $vrednost;
	        	 	}
       						DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost)"
				."VALUES(".$dobavljac_id.",'" . addslashes(Support::encodeTo1250($sifra)) . "',".Support::quotedStr(Support::encodeTo1250($naziv_karakteristike)).","
				." ".Support::quotedStr(Support::encodeTo1250($vrednost)).") ");
	        	 } 


				if(isset($slika))
				{
				
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'" . addslashes(Support::encodeTo1250($sifra)) . "','".Support::encodeTo1250($slika)."',1 )");
					
					$flag_slika_postoji = 1;
				}
				else
				{
					$flag_slika_postoji = 0;
				}

				


					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($naziv)) . "',";					
					$sPolja .= " grupa,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($grupa)) . "',";
					if (isset($proizvodjac) && !empty($proizvodjac)) {
					$sPolja .= " proizvodjac,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($proizvodjac)) . "',";
					}
					$sPolja .= " web_flag_karakteristike,";	$sVrednosti .= " 2,";			
					$sPolja .= " podgrupa,";				$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($podgrupa)) . "',";				
					$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " ".$flag_slika_postoji.",";
					if (isset($opis)) {
						$sPolja .= " opis,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($opis)) . "',";
						$sPolja .= " flag_opis_postoji,";		$sVrednosti .= "1,";	
					}				
					$sPolja .= " pdv,";						$sVrednosti .= "20,";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";
					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					

				}


	        
			}
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/ctcunit/ctcunit_json/ctcunit.json');
			$products_file = "files/ctcunit/ctcunit_json/ctcunit.json";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
			
	        foreach($products as $product_arr) {
	        	$product = (object) $product_arr;	        	
	        	$sifra = $product->Identifier;
				$kolicina = $product->Stock;
	        	$cena_nc = (float) $product->Price;


				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0){
					
					
					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}
			}

			// Support::queryShortExecute($dobavljac_id, $type);
			// Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

}