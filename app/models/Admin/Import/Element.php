<?php
namespace Import;
use Import\Support;
use DB;
use File; 
use PHPExcel; 
use PHPExcel_IOFactory;

class Element {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/element/element_xml/element.xml');
			$products_file = "files/element/element_xml/element.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}


		$products_2 = simplexml_load_file($products_file);
		$products_2 = $products_2->xpath('//product');

		foreach($products_2 as $product):

			$sifra = (string) $product->textId;
			$naziv = (string) $product->naziv;
			$opis = (string) $product->opis;
			$proizvodjac = (string) $product->proizvodjac;
			$slika = (string) $product->slika;
			$cena_nc = (float) $product->netoCena;
			$kolicina= (int) $product->lagerVp;



			$specifikacije_name = $product->specifications->xpath('attribute'); 
			$specifikacije_vrednost = $product->specifications->value;
			
			if (isset($sifra) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0) {
				
			foreach ($specifikacije_name as $key => $spec) {
				$naziv_karakteristike = (string) $spec->attributes()->name;
				$vrednost_karakteristike = (string) $specifikacije_vrednost[$key];

				DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost)"
				."VALUES(".$dobavljac_id."," . Support::quotedStr($sifra) . ",".Support::quotedStr(Support::encodeTo1250($naziv_karakteristike)).","
				." ".Support::quotedStr(Support::encodeTo1250($vrednost_karakteristike)).") ");
       					

			}

			
			
			$sPolja = '';
			$sVrednosti = '';

			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($sifra) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "'" . Support::encodeTo1250($naziv) . " ',";
			//opis je nepregledan (ucitao sam karakteristike)
			// if (isset($opis) && !empty($opis)) {
			// $sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 1,";	
			// $sPolja .= "opis,";					$sVrednosti .= "'" . Support::encodeTo1250($opis) . " ',";
			// }
			$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . Support::encodeTo1250($proizvodjac) . " ',";
			if(isset($slika) && !empty($slika)){
				$sPolja .= "flag_slika_postoji,";	$sVrednosti .= " 1,";	
			}
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";
			$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
			if (isset($slika) && !empty($slika)) {
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250($slika)."',1 )");			
			}		
			}
			endforeach;
			
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
	
				$file_name = Support::file_name("files/element/element_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/element/element_xml/".$file_name);
		}
		}
}


	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
				
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/element/element_xml/element.xml');
			$products_file = "files/element/element_xml/element.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}


		$products_2 = simplexml_load_file($products_file);
		$products_2 = $products_2->xpath('//product');


		foreach($products_2 as $product):

			
			$sifra = (string) $product->textId;
			$cena_nc = (float) $product->netoCena;
			$kolicina= (int) $product->lagerVp;

			if (isset($sifra) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0) {
				

			$sPolja = '';
			$sVrednosti = '';

			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($sifra) . ",";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";
			$sPolja .= "cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
		
			}
			endforeach;
			
			
	
				$file_name = Support::file_name("files/element/element_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/element/element_xml/".$file_name);
		}
	}
}
}