<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Vox {

	public static function ftpConnection() {

			$ftp_host = '94.127.7.141';
            $ftp_user_name = 'stefan@black666.mycpanel.rs';
            $ftp_user_pass = 'Ari123Lje2021';
            $connection = ftp_connect($ftp_host);
            $login_result = ftp_login($connection,$ftp_user_name,$ftp_user_pass);
            ftp_pasv($connection, true);

            if(ftp_size($connection, "erg.xml") != -1){
			ftp_get($connection,'files/vox/vox_xml/vox.xml','erg.xml', FTP_BINARY);
           
            }

            ftp_close($connection);
	}
	public static function execute($dobavljac_id,$kurs=null,$extension=null){		
		
		if($extension==null){
			self::ftpConnection();
			Support::autoDownload('http://www.voxelectronics.com/webservis/externalsite/vox/product_specification_xml.php?seckey=Js6X95U5KP9Pp&user=productfeed&pass=productfeedxml&lang=rs',"files/vox/vox_xml/vox_karakteristike.xml");
			$products_file = "files/vox/vox_xml/vox.xml";
			$products_file_karakt ="files/vox/vox_xml/vox_karakteristike.xml";
			$continue = false;
            if(File::exists($products_file) OR File::exists($products_file_karakt)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/vox/vox_xml/vox.'.$extension;
			$products_file_karakt = 'files/vox/vox_xml/vox_karakteristike.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	
			$products_karakteristike = simplexml_load_file($products_file_karakt);     
			
			foreach ($products_karakteristike as $product):    

			  $specifikacija=$product->specifications;      

			    foreach ($specifikacija as $el) {
			      $el = preg_replace('~//<!\[CDATA\[\s*|\s*//\]\]>~', '', $el);
			      $el = trim($el);
			      $el = str_replace(" ", "_", $el);
			      $el = str_replace("(", "", $el);
			      $el = str_replace(")", "", $el);
			      $el = str_replace("/", "-", $el);
			      $el = str_replace("+", "Plus", $el);
			      $el = str_replace("<-", "</", $el);
			      $el = str_replace("3D", "_3D", $el);
			      $el = str_replace("©", "", $el);
			      $el = str_replace("°", "", $el);
			      $el = str_replace(".", "", $el);
			      $el = str_replace(",", "", $el);
			      $el = str_replace(":", "", $el);
			      $el = str_replace("5", "Pet", $el);
      			  $el = str_replace("8", "Osam", $el);
			      $el = str_replace("BAR-kod", "BAR", $el);
			      $el = str_replace("2 brzine", "brzine", $el);
			      $el = str_replace("<2 brzine>", "<brzine>", $el);
			      $el = str_replace("<Child-lock>", "<Child_lock>", $el);
			      $el = str_replace("</2 brzine>", "</brzine>", $el);
			      $el = str_replace("<2", "<", $el);
			      $el = str_replace("</2", "</", $el);
			      $el = str_replace("<BAR-kod>", "<BAR>", $el);
			      $el = str_replace("<3D distribucija mikrotalasa>", "<_3D distribucija mikrotalasa>", $el);
			      $el = str_replace("</3D distribucija mikrotalasa>", "</_3D distribucija mikrotalasa>", $el);
			      $el = str_replace("</BAR-kod>", "</BAR>", $el);
			      $el = str_replace("<8", "<Osam", $el);
			      $el = str_replace("</8", "</Osam", $el);
			      $el = str_replace("<3", "<Tri", $el);
			      $el = str_replace("</3", "</Tri", $el);
			      $el = str_replace("&", "AND", $el);
			      
			      $karakteristike = simplexml_load_string( '<?xml version="1.0" encoding="UTF-8"?><karakteristike>'.$el.'</karakteristike>' );
			        
			      $karakteristike =((array)$karakteristike);

			        foreach ($karakteristike as $name => $value) {                  
			          if(is_array($value)){
			            $value = implode(', ', $value);
			          }else if(is_object($value)){
			            $value = implode(', ', (array)$value);  
			          }
			          
			          $sPolja = '';
			          $sVrednosti = '';
			          $sPolja .= "partner_id,";       $sVrednosti .= "24,";
			          $sPolja .= "sifra_kod_dobavljaca,";   $sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250(trim($product->product_code))) . ",";  
			          $sPolja .= "karakteristika_naziv,";   $sVrednosti .= "" . Support::quotedStr(pg_escape_string(Support::encodeTo1250(str_replace(array("_","Plus","brzine","BAR","AND","Pet","Osam","Tri"),array(" ","+","2 brzine","Bar kod","&","5","8","3"), $name)))) . ",";
			          $sPolja .= "karakteristika_vrednost"; $sVrednosti .= "'" . Support::encodeTo1250(pg_escape_string(str_replace(array("_","Plus","brzine","BAR","AND","Pet","Osam","Tri"),array(" ","+","2 brzine","Bar kod","&","5","8","3"), $value))) . "'";
			            
			          DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			        }
        		}
     		endforeach;
			
			foreach ($products as $product):
				if(!empty($product->id)){

					$slika =  'http://www.voxelectronics.com/files/images/slike_proizvoda/'.trim($product->id).'.jpg' ;
					$slika1 = 'http://www.voxelectronics.com/files/images/slike_proizvoda/'.trim($product->id).'_1.jpg' ;
					$slika2 = 'http://www.voxelectronics.com/files/images/slike_proizvoda/'.trim($product->id).'_2.jpg' ;
					$slika3 = 'http://www.voxelectronics.com/files/images/slike_proizvoda/'.trim($product->id).'_3.jpg' ;

					$sPolja = '';
					$sVrednosti = '';
				
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250(trim($product->id))) . "',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->name) . " ( " . Support::encodeTo1250(trim($product->id)) . " )") . "',";
					$sPolja .= "grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->category)) . "',";
					$sPolja .= "proizvodjac,";				$sVrednosti .= "'Vox',";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . str_replace('>','',$product->stock) . ",";
					$sPolja .= "barkod,";					$sVrednosti .= "'" . $product->barcode . "',";
					$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 2,";
					$sPolja .= "pdv,";						$sVrednosti .= " 20,";
					$sPolja .= "tarifna_grupa_id,";			$sVrednosti .= " 0,";
					
					if(self::checkUrl($slika) ==1){
						$sPolja .= "flag_slika_postoji,";	$sVrednosti .= "1,";
					}else{
						$sPolja .= "flag_slika_postoji,";	$sVrednosti .= "0,";
					}
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '', $product->price),1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					if(self::checkUrl($slika) ==1){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr(trim($product->id)).",'".Support::encodeTo1250($slika)."',1 )");
					}
					if(self::checkUrl($slika1) ==1){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr(trim($product->id)).",'".Support::encodeTo1250($slika1)."',0 )");
					}
					if(self::checkUrl($slika2) ==1){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr(trim($product->id)).",'".Support::encodeTo1250($slika2)."',0 )");
					}
					if(self::checkUrl($slika3) ==1){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr(trim($product->id)).",'".Support::encodeTo1250($slika3)."',0 )");
					}

				}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		$extension = 'xml';
		if($extension==null){
			$products_file = "files/vox/vox_xml/vox.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			self::ftpConnection();
			$continue = true;
			$products_file = 'files/vox/vox_xml/vox.'.$extension;
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	        
			foreach ($products as $product):
				if(!empty($product->id)){

					$sPolja = '';
					$sVrednosti = '';
				
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->id)) . "',";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . str_replace('>','',$product->stock) . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '', $product->price),1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}
	public function checkUrl($url){
		$headers = get_headers($url, 1);
		if ($headers[0] == 'HTTP/1.1 200 OK') {
		  return 1;
		}else{
		  return 0;
		}
	}

}