<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Wobyhaus {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/wobyhaus/wobyhaus_xml/wobyhaus.xml');
			$products_file = "files/wobyhaus/wobyhaus_xml/wobyhaus.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	
	        
			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';


				$sifra = (string) $product->sifra_artikla;
				$barkod = (string) $product->barkod;
				$naziv = (string) $product->naziv;
				$grupa = (string) $product->grupa;
				$podgrupa = (string) $product->podgrupa;
				$proizvodjac = (string) $product->proizvodjac;
				$kolicina = (int) $product->kolicina;
				$cena_nc = (float) $product->mpcena;
				$opis = (string) $product->opis;
				$slike = $product->slike->slika;

				$flag_slika_postoji = 0;
				foreach ($slike as $key => $slika) {
					if($key==0){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".Support::encodeTo1250($sifra)."','".Support::encodeTo1250($slika)."',1 )");
				}else{
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".Support::encodeTo1250($sifra)."','".Support::encodeTo1250($slika)."',0 )");
				}
				$flag_slika_postoji = "1";
				
				}	

				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";
				$sPolja .= " barkod,";					$sVrednosti .= " '" . Support::encodeTo1250($barkod) . "',";	
				$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). "',";
				$sPolja .= " grupa,";					$sVrednosti .= " '" . Support::encodeTo1250($grupa) . "',";
				$sPolja .= " podgrupa,";				$sVrednosti .= " '" . Support::encodeTo1250($podgrupa) . "',";
				$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . Support::encodeTo1250($proizvodjac) . "',";
				if (isset($opis) && !empty($opis)) {
				$sPolja .= " opis,";					$sVrednosti .= " '" .	Support::encodeTo1250(str_replace(array("Ř","Đ"),array("R","Dj"),pg_escape_string($opis))). "',";
				$sPolja .= " flag_opis_postoji,";		$sVrednosti .= "1,";
				}
				$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " '" . $flag_slika_postoji. "',";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($cena_nc*0.85,1,$kurs,$valuta_id_nc),2, '.', '') . ",";	
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/wobyhaus/wobyhaus_xml/wobyhaus.xml');
			$products_file = "files/wobyhaus/wobyhaus_xml/wobyhaus.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

		$products = simplexml_load_file($products_file);	
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';

				$sifra = (string) $product->sifra_artikla;
				$cena_nc = (float) $product->mpcena;
				$kolicina = (int) $product->kolicina;

				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($cena_nc*0.85,1,$kurs,$valuta_id_nc),2, '.', '') . ",";	
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";
						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}
	


}