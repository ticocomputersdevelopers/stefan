<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Rabalux {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/rabalux/rabalux.csv";
			$products_file_stock = "files/rabalux/rabalux_stock.csv";
			self::get_ftp_file('91.144.96.87','21','webshop-export-1','ws-export-9276','/webshop_RS.csv',$products_file);
			self::get_ftp_file('91.144.96.87','21','webshop-export-1','ws-export-9276','/webshop_stock.csv',$products_file_stock);
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }

		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}

		if(File::exists($products_file_stock)){
                $stock_barcode_array = array ();
				$handle = fopen($products_file_stock, "r");
				$i = 0;
				while (($data = fgetcsv($handle,10000,";")) !== FALSE) {
					$i++;
			
					if($i > 1) {
						$barcode = $data[0];
						$stock = ($data[2] == 'available') ? '1' : $data[2];
						$stock_barcode_array[$barcode] = $stock;
					}
				}
        }
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			
			if($extension == 'xlsx' || $extension == 'xls') {
				$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        	$excelObj = $excelReader->load($products_file);
	        	$worksheet = $excelObj->getSheet(0);
	        	$lastRow = $worksheet->getHighestRow();
	        

	       	    for ($row = 2; $row <= $lastRow; $row++) {
	       	    	$sifra = (string) $worksheet->getCell('A'.$row)->getValue();
	       	    	if(!empty((string) $worksheet->getCell('C'.$row)->getValue())) {
	       	    		$cena_nc = (float) $worksheet->getCell('C'.$row)->getValue();
	       	    	} else {
	       	    		$cena_nc = $worksheet->getCell('D'.$row)->getValue();
	       	    	}
	       	    	$data = array(
	       	    		        	'cena_nc' => $cena_nc,
	       	    		        	'kolicina' => 1
	       	    				);
	       	    	if (!empty($sifra)) {
		       	    	if(DB::table('dobavljac_cenovnik')->where(array('sifra_kod_dobavljaca'=>$sifra,'partner_id'=>$dobavljac_id))) {
		       	    		DB::table('dobavljac_cenovnik')->where(array('sifra_kod_dobavljaca'=>$sifra,'partner_id'=>$dobavljac_id))->update($data);
		       	    	}
	       	    	}
	       	    }

	       	    $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        	$excelObj = $excelReader->load($products_file);
	        	$worksheet = $excelObj->getSheet(1);
	        	$lastRow = $worksheet->getHighestRow();

	       		for ($row = 2; $row <= $lastRow; $row++) {
	       	    	$sifra = (string) $worksheet->getCell('A'.$row)->getValue();
	       	    	$cena_nc = (float) $worksheet->getCell('C'.$row)->getValue();
	       	    	$data = array(
	       	    		        	'cena_nc' => $cena_nc,
	       	    		        	'kolicina' => 1
	       	    				);
	       	    	if (!empty($sifra)) {
		       	    	if(DB::table('dobavljac_cenovnik')->where(array('sifra_kod_dobavljaca'=>$sifra,'partner_id'=>$dobavljac_id))) {
		       	    		DB::table('dobavljac_cenovnik')->where(array('sifra_kod_dobavljaca'=>$sifra,'partner_id'=>$dobavljac_id))->update($data);
		       	    	}
	       	    	}
	       	    }

	       	   	$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        	$excelObj = $excelReader->load($products_file);
	        	$worksheet = $excelObj->getSheet(2);
	        	$lastRow = $worksheet->getHighestRow();

	        	for ($row = 2; $row <= $lastRow; $row++) {
	       	    	$sifra = (string) $worksheet->getCell('A'.$row)->getValue();
	       	    	$cena_nc = (float) $worksheet->getCell('C'.$row)->getValue();
	       	    	$data = array(
	       	    		        	'cena_nc' => $cena_nc,
	       	    		        	'kolicina' => 1
	       	    				);
	       	    	if (!empty($sifra)) {
		       	    	if(DB::table('dobavljac_cenovnik')->where(array('sifra_kod_dobavljaca'=>$sifra,'partner_id'=>$dobavljac_id))) {
		       	    		DB::table('dobavljac_cenovnik')->where(array('sifra_kod_dobavljaca'=>$sifra,'partner_id'=>$dobavljac_id))->update($data);
		       	    	}
	       	    	}
	       	    }


			} else {
			$i = 0;
			$handle = fopen($products_file, "r");
		
			while (($data = fgetcsv($handle,10000,";")) !== FALSE) {
			$i++;
			
			if($i > 1) {
			$br_stavke = $data[0];
			$prezime = $data[1];
			$ean_kod = $data[2];
			$kategorija = $data[3];
			$tip = $data[4];
			$stil_lampe = $data[5];
			$preporucena_prostorija_za_koriscenje = $data[6];
			$temperatura_boje_izvora_svetlosti = $data[7];
			$svetlosni_tok_izvora_svetlosti_lm = $data[8];
			$specifikacije_izvora_svetlosti = $data[9];
			$led_tehnologija = $data[10];
			$ip_zastita = $data[11];
			$specifikacije_senzora_pokreta = $data[12];
			$boja_lampe = $data[13];
			$boja_abazura = $data[14];
			$materijal_lampe = $data[15];
			$materijal_abazura = $data[16];
			$vek_trajanja_izvora_svetlosti_h = $data[17];
			$ukljucenih_izvora_svetlosti = $data[18];
			$energetska_klasa_izvora_osvetljenja = $data[19];
			$garancija_godina = $data[20];
			$ugao_osvetljenja = $data[21];
			$ponderisana_potrosnja_energije = $data[22];
			$broj_grla = $data[23];
			$vrsta_grla = $data[24];
			$broj_grla_br_2 = $data[25];
			$vrsta_grla_br_2 = $data[26];
			$primenjena_voltaza = $data[27];
			$specifikacije_osvetljenja = $data[28];
			


			$sifra = $br_stavke;
			$naziv = $prezime.$tip;
			$barkod = $ean_kod;
			$grupa = $kategorija;
			$podgrupa = $tip;
			$model = $prezime;
			$opis = "";
			$proizvodjac = 'Rabalux';
			
			if(isset($stock_barcode_array[$barkod])){
				$kolicina = $stock_barcode_array[$barkod];
			}else{
				$kolicina = '0';
			}

			$karakteristika_naziv = array();
			$karakteristika_vrednost = array();

			if(!empty($boja_lampe)) {
				$opis .= "Boja lampe: ".$boja_lampe.", ";
				$karakteristika_naziv[] = "Boja lampe";
				$karakteristika_vrednost[] = $boja_lampe;
			}

			if(!empty($boja_abazura)) {
				$opis .= "Boja abažura: ".$boja_abazura.", ";
				$karakteristika_naziv[] = "Boja abažura";
				$karakteristika_vrednost[] = $boja_abazura;
			}

			if(!empty($materijal_lampe)) {
				$opis .= "Materijal lampe: ".$materijal_lampe.", ";
				$karakteristika_naziv[] = "Materijal lampe";
				$karakteristika_vrednost[] = $materijal_lampe;
			}

			if(!empty($materijal_abazura)) {
				$opis .= "Materijal abažura: ".$materijal_abazura.", ";
				$karakteristika_naziv[] = "Materijal abažura";
				$karakteristika_vrednost[] = $materijal_abazura;
			}

			if(!empty($vek_trajanja_izvora_svetlosti_h)) {
				$opis .= "Vek trajanja izvora svetlosti (h): ".$vek_trajanja_izvora_svetlosti_h.", ";
				$karakteristika_naziv[] = "Vek trajanja izvora svetlosti (h)";
				$karakteristika_vrednost[] = $vek_trajanja_izvora_svetlosti_h;
			}

			if (!empty($ukljucenih_izvora_svetlosti)) {
				$opis .= "Uključenih izvora svetlosti: ".$ukljucenih_izvora_svetlosti.", ";
				$karakteristika_naziv[] = "Uključenih izvora svetlosti";
				$karakteristika_vrednost[] = $ukljucenih_izvora_svetlosti;
			}

			if(!empty($energetska_klasa_izvora_osvetljenja)) {
				$opis .= "Energetska klasa izvora osvetljenja: ".$energetska_klasa_izvora_osvetljenja.", ";
				$karakteristika_naziv[] = "Energetska klasa izvora osvetljenja";
				$karakteristika_vrednost[] = $energetska_klasa_izvora_osvetljenja;
			}

			if(!empty($garancija_godina)) {
				$opis .= "Garancija (godina): ".$garancija_godina.", ";
				$karakteristika_naziv[] = "Garancija (godina)";
				$karakteristika_vrednost[] = $garancija_godina;
			}

			if(!empty($ugao_osvetljenja)) {
				$opis .= "Ugao osvetljenja (x°): ".$ugao_osvetljenja.", ";
				$karakteristika_naziv[] = "Ugao osvetljenja (x°)";
				$karakteristika_vrednost[] = $ugao_osvetljenja;
			}

			if(!empty($ponderisana_potrosnja_energije)) {
				$opis .= "Ponderisana potrošnja energije (kwh/1000 ah): ".$ponderisana_potrosnja_energije.", ";
				$karakteristika_naziv[] = "Ponderisana potrošnja energije (kwh/1000 ah)";
				$karakteristika_vrednost[] = $ponderisana_potrosnja_energije;
			}

			if(!empty($broj_grla)) {
				$opis .= "Broj grla: ".$broj_grla.", ";
				$karakteristika_naziv[] = "Broj grla";
				$karakteristika_vrednost[] = $broj_grla;
			}

			if(!empty($vrsta_grla)) {
				$opis .= "Vrsta grla: ".$vrsta_grla.", ";
				$karakteristika_naziv[] = "Vrsta grla";
				$karakteristika_vrednost[] = $vrsta_grla;
			}

			if(!empty($stil_lampe)) {
				$opis .= "Stil lampe: ".$stil_lampe.", ";
				$karakteristika_naziv[] = "Stil lampe";
				$karakteristika_vrednost[] = $stil_lampe;
			}

			if(!empty($primenjena_voltaza)) {
				$opis .= "Primenjena voltaža: ".$primenjena_voltaza.", ";
				$karakteristika_naziv[] = "Primenjena voltaža";
				$karakteristika_vrednost[] = $primenjena_voltaza;
			}

			if(!empty($specifikacije_osvetljenja)) {
				$opis .= "Specifikacije osvetljenja: ".$specifikacije_osvetljenja.", ";
				$karakteristika_naziv[] = "Specifikacije osvetljenja";
				$karakteristika_vrednost[] = $specifikacije_osvetljenja;
			}

			if(!empty($opis)) {
				$opis = rtrim($opis, ", ");
				$opis .= ".";
			}

			$slike = array();
			for($g=75;$g<=84;$g++) {
			 	if(!empty($data[$g])){
			 		$slike[] = $data[$g];	
			 	}
			 }
			 if(sizeof($slike) > 0) {
			 	$flag_slika_postoji = 1;
			 } else {
			 	$flag_slika_postoji = 0;
			 }

			 foreach ($slike as $key => $slika) {
			 	if ($key == 0) {
			 		DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250($slika)."',1 )");
			 	} else {
			 		DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250($slika)."',0 )");
			 	}
			 }

			 foreach ($karakteristika_naziv as $key => $karak_naziv) {
			 		$sPolja = '';
					$sVrednosti = '';

					$sPolja .= "partner_id,";					$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";			$sVrednosti .= "" . Support::quotedStr($sifra) . ",";	
					$sPolja .= "karakteristika_naziv,";			$sVrednosti .= "'" . Support::encodeTo1250($karak_naziv) . "',";
					$sPolja .= "karakteristika_vrednost";		$sVrednosti .= "'" . Support::encodeTo1250($karakteristika_vrednost[$key]) . "'";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			
			 }

		
					if(!empty($sifra)){
					$sPolja = '';
					$sVrednosti = '';					
					
					$sPolja .= "partner_id,";					$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";			$sVrednosti .= "" . Support::quotedStr($sifra) . ",";
					$sPolja .= "barkod,";						$sVrednosti .= "" . Support::quotedStr($barkod) . ",";
					$sPolja .= "model,";						$sVrednosti .= "'" . addslashes($model) . "',";
					$sPolja .= "naziv,";						$sVrednosti .= "'" . Support::encodeTo1250($naziv) . "',";
					$sPolja .= "kolicina,";						$sVrednosti .= "" . $kolicina . ",";
					$sPolja .= "proizvodjac,";					$sVrednosti .= "'" . Support::encodeTo1250($proizvodjac) . "',";
					$sPolja .= "grupa,";						$sVrednosti .= "'" . Support::encodeTo1250($grupa) . "',";
					$sPolja .= "podgrupa,";						$sVrednosti .= "'" . Support::encodeTo1250($podgrupa) . "',";
					if(!empty($opis)) {
					$sPolja .= "flag_opis_postoji,";			$sVrednosti .= "1,";
					$sPolja .= "opis,";							$sVrednosti .= "'" . Support::encodeTo1250($opis) . "',";
					}
					if(sizeof($karakteristika_vrednost) > 0) {
					$sPolja .= "web_flag_karakteristike,";		$sVrednosti .= "1,";
					} else {
					$sPolja .= "web_flag_karakteristike,";		$sVrednosti .= "0,";
					}
					$sPolja .= "flag_slika_postoji";			$sVrednosti .= "" . $flag_slika_postoji . "";

										
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					}
				}	
			}
			}
		}	
		
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
	}


	//nema kratku automatiku!!!
	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/rabalux/rabalux.csv');
			$products_file = "files/rabalux/rabalux.csv";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			
			$i = 0;
			$handle = fopen($products_file, "r");
		
			while (($data = fgetcsv($handle,10000,";")) !== FALSE) {		
		
			$sifra = $data[0];
			$ean = $data[1];
 			//var_dump($data[5]);die();
 			$flag_slika_postoji = "0";

 				if(isset($data[2])){
		
					$sPolja = '';
					$sVrednosti = '';					
				
					$sPolja .= "partner_id,";					$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";			$sVrednosti .= "'" . addslashes($sifra) . "',";
					$sPolja .= "kolicina,";						$sVrednosti .= "'" . number_format(intval($data[6]), 2, '.', '') . "',";
					$sPolja .= "cena_nc,";						$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $data[3]),1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
					$sPolja .= "pmp_cena";						$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $data[4]),1,$kurs,$valuta_id_nc)), 2, '.', '') . "";	
										
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}	
			}	
		}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
	}

	public static function get_ftp_file($ftp_host,$port,$ftp_user_name,$ftp_user_pass,$server_file,$products_file)
	{

            $connection = ftp_connect($ftp_host,$port);
            $login_result = ftp_login($connection,$ftp_user_name,$ftp_user_pass);
            ftp_pasv($connection, true);
            if(ftp_size($connection, $server_file) != -1){
				ftp_get($connection,$products_file,$server_file, FTP_BINARY);
            }

            ftp_close($connection); 
	}

}