<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;

class Spektar {

	public static function execute($dobavljac_id,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/spektar/spektar_xml/spektar.xml');
			}
			$file_name = Support::file_name("files/spektar/spektar_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/spektar/spektar_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}



		foreach ($products as $product) {

			$model=str_replace(array("//<![CDATA[","//]]>"),"",$product->model);

			//karakteristike
			$specifikacija=$product->xpath('attributes/attribute');		
			
			foreach ($specifikacija as $el) {
				
				$name=str_replace(array("//<![CDATA[","//]]>"),"",$el->name);
				$value=str_replace(array("//<![CDATA[","//]]>"),"",$el->value);
				
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->ean)) . ",";	
					$sPolja .= "karakteristika_naziv,";		$sVrednosti .= "" . Support::quotedStr(trim(Support::encodeTo1250((pg_escape_string($name))))) . ",";
					$sPolja .= "karakteristika_vrednost";	$sVrednosti .= "'" . trim(Support::encodeTo1250((pg_escape_string($value)))) . "'";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			}
			
			//slike
			$images = $product->xpath('images/image/large');
			$flag_slika_postoji = "0";
			$i=0;
			foreach ($images as $slika){			
				if($i==0){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->ean).",'".Support::encodeTo1250($slika)."',1 )");
				}else{
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->ean).",'".Support::encodeTo1250($slika)."',0 )");
				}
				$flag_slika_postoji = "1";
				$i++;
			}	
			$name=str_replace(array("//<![CDATA[","//]]>"),"",$product->name);
			$category=str_replace(array("//<![CDATA[","//]]>"),"",$product->category);
			$manufacturer=str_replace(array("//<![CDATA[","//]]>"),"",$product->manufacturer);
			$description=str_replace(array("//<![CDATA[","//]]>"),"",$product->description);
			

			//proizvodi
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->ean)) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStrPC(Support::encodeTo1250(pg_escape_string($name))) . ",";
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250(pg_escape_string($category))) . ",";		
			$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 2,";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->stock . ",";
			$sPolja .= "model,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250(pg_escape_string($model))) . ",";
			$sPolja .= "barkod,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250(pg_escape_string($product->ean))) . ",";
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . $product->price . ",";			
			$sPolja .= "opis,";						$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($description)) . ",";
			$sPolja .= "proizvodjac,";				$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($manufacturer)) . ",";
			$sPolja .= "flag_slika_postoji";		$sVrednosti .= "".$flag_slika_postoji."";
							
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			DB::statement("UPDATE dobavljac_cenovnik_temp SET flag_opis_postoji = 1 where opis != '' ");
		}


		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
		
		//Brisemo fajl
		$file_name = Support::file_name("files/spektar/spektar_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/spektar/spektar_xml/".$file_name);
		}
	
	}

	public static function executeShort($dobavljac_id,$extension=null){

		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/spektar/spektar_xml/ewe.xml');
			}
			$file_name = Support::file_name("files/spektar/spektar_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/spektar/spektar_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		foreach ($products as $product) {
			$model=str_replace(array("//<![CDATA[","//]]>"),"",$product->model);

			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->ean)) . ",";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->stock . ",";
			$sPolja .= "cena_nc";					$sVrednosti .= "" . $product->price . "";		
			
							
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

		}
		
		//Support::queryShortExecute($dobavljac_id);
		
		//Brisemo fajl
		$file_name = Support::file_name("files/spektar/spektar_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/spektar/spektar_xml/".$file_name);
		}
	
	}

}