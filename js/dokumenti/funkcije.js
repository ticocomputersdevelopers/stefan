var base_url = $('#base_url').val();

$(document).ready(function(){
	$('.JSbtn-delete').on('click', function(e){
	e.preventDefault();
	var link = $(this).data('link');
	  	alertify.confirm("Da li ste sigurni da želite da izvršite akciju brisanja?",
	                function(e){
	                if(e){
	                    location.href = link;
	                }
	          });
	});
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
