		

		function initialize() {
			var lat = parseFloat(document.getElementById('lat').value);
			var long = parseFloat(document.getElementById('long').value);

			var map_canvas = document.getElementById('bounce_map_canvas');

			var myCenter = new google.maps.LatLng(lat,long);

			var map_options = {
				center: myCenter,
				zoom: 15,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}

			var map = new google.maps.Map(map_canvas, map_options);

			var marker = new google.maps.Marker({
				position: myCenter,
				animation: google.maps.Animation.BOUNCE,
				map: map,
				title: 'Nasa lokacija'
			});

			marker.setMap(map);

		} 
		
		google.maps.event.addDomListener(window, 'load', initialize);





