	base_url = $('#base_url').val();
	vodjenje_lagera = $('#vodjenje_lagera').val();
	$(document).ready(function() {
		$('.JSKolicinaVezani').change(function() {
			$(this).closest('section').find('.add-to-cart').attr('data-kolicina', $(this).val());
		});
		$(document).on('click','.add-to-cart',function() {
			//alert(base_url);
			var roba_id = $(this).data('product-id');
			var kolicina = $(this).data('kolicina');
			var status = $(this).data('status');
			var lager = $(this).data('lager');
			var cart_kol = $(this).data('cart_kol');
			var h_br_c = document.getElementById('h_br_c').value;
			var broj_cart = Number(h_br_c) + 1;
			var new_kol = lager - kolicina;
			if (Number(vodjenje_lagera) == 1) {
				if (lager - kolicina < 0) {
					$(this).html('Nije dostupno');
					$(this).addClass('not-available');
					$(this).removeClass('add-to-cart');
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Već ste dodali maksimalnu dostupnu količinu.</p>");
				} else {
					if (new_kol == 0) {
						$(this).html('Nije dostupno');
						$(this).addClass('not-available');
						$(this).removeClass('add-to-cart');
					}
					$(this).data('lager', new_kol);
					$.ajax({
						type: "POST",
						url: base_url + 'cart_add',
						data: { roba_id: roba_id, kolicina: kolicina, status: status },
						success: function(msg) {
							$('.header-cart-content').html(msg);
							document.getElementById('h_br_c').value = broj_cart;
							$('#broj_cart').html("(" + broj_cart + ")");
							$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
							$('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
						}
					});
				}
			} else {
				$.ajax({
					type: "POST",
					url: base_url + 'cart_add',
					data: { roba_id: roba_id, kolicina: kolicina, status: status },
					success: function(msg) {
						$('.header-cart-content').html(msg);
						document.getElementById('h_br_c').value = broj_cart;
						$('#broj_cart').html("(" + broj_cart + ")");
						$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
						$('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
					}
				});
			}
		});
		$('.cart-less').click(function() {
			var kol_id = $(this).data('kol-id');
			kolicina = $('#' + kol_id + '').val();
			var roba_id = $(this).data('roba-id');
			if (Number(kolicina) == 1) {
				$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
				$('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
			} else {
				$.ajax({
					type: "POST",
					url: base_url + 'cart_add',
					data: { roba_id: roba_id, kolicina: 1, status: 3 },
					success: function(msg) {
						location.reload();
					}
				});
			}
		});
		$('.cart-more').click(function() {
			var max_col = $(this).data('max-kol');
			var kol_id = $(this).data('kol-id');
			kolicina = $('#' + kol_id + '').val();
			var roba_id = $(this).data('roba-id');
			if (Number(vodjenje_lagera) == 1) {
				if (max_col == kolicina) {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Dostupna količina je " + max_col + " kom.</p>");
				} else {
					$.ajax({
						type: "POST",
						url: base_url + 'cart_add',
						data: { roba_id: roba_id, kolicina: 1, status: 1 },
						success: function(msg) {
							location.reload();
						}
					});
				}
			} else {
				$.ajax({
					type: "POST",
					url: base_url + 'cart_add',
					data: { roba_id: roba_id, kolicina: 1, status: 1 },
					success: function(msg) {
						location.reload();
					}
				});
			}
		});
		$('.cart-amount').change(function() {
			var max_col = $(this).data('max-kol');
			var old_kol = $(this).data('old-kol');
			var new_kol = $(this).val();
			var roba_id = $(this).data('roba-id');
			if (Number(vodjenje_lagera) == 1) {
				if (max_col < new_kol) {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Dostupna količina je " + max_col + " kom.</p>");
					$(this).val(max_col);
				} else if (Number(new_kol) < 1) {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
					$(this).val(1);
				} else {
					$.ajax({
						type: "POST",
						url: base_url + 'cart_add',
						data: { roba_id: roba_id, kolicina: new_kol, status: 2 },
						success: function(msg) {
							location.reload();
						}
					});
				}
			} else {
				if (Number(new_kol) < 1) {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
					$(this).val(1);
				} else {
					$.ajax({
						type: "POST",
						url: base_url + 'cart_add',
						data: { roba_id: roba_id, kolicina: new_kol, status: 2 },
						success: function(msg) {
							location.reload();
						}
					});
				}
			}
		});
		// $('.cart-remove-all').click(function() {
		// 	$('.delete-all').fadeIn("fast");
		// });
		$('.add-amount-more').click(function() {
			var max_col = $(this).data('max-kol');
			var kolicina = $('.add-amount').val();
			if (Number(vodjenje_lagera) == 1) {
				if ((Number(kolicina) + 1) > max_col) {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Dostupna količina je " + max_col + " kom.</p>");
				} else {
					new_kol = Number(kolicina) + 1;
					$('.add-amount').val(new_kol);
				}
			} else {
				new_kol = Number(kolicina) + 1;
				$('.add-amount').val(new_kol);
			}
		});
		$('.add-to-cart-artikal').click(function() {
			var kolicina = $('.add-amount').val();
			var status = 2;
			var roba_id = $(this).data('roba-id');
			if (Number(vodjenje_lagera) == 1) {
				var max_col = $('.add-amount').data('max-kol');
				if (max_col > 0) {
					$.ajax({
						type: "POST",
						url: base_url + 'cart_add',
						data: { roba_id: roba_id, kolicina: kolicina, status: status },
						success: function(msg) {
							$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
							$('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
							location.reload();
						}
					});
				} else {
					$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Artikal nije dostupan.</p>");
				}
			} else {
				$.ajax({
					type: "POST",
					url: base_url + 'cart_add',
					data: { roba_id: roba_id, kolicina: kolicina, status: status },
					success: function(msg) {
						$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
						$('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
						location.reload();
					}
				});
			}
		});
		$('.add-amount-less').click(function() {
			var kolicina = $('.add-amount').val();
			if (Number(kolicina) == 1) {
				$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
				$('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
			} else {
				new_kol = Number(kolicina) - 1;
				$('.add-amount').val(new_kol);
			}
		});


		$('.add-amount').change(function() {
			var kolicina = $(this).val();
			var max_col = $(this).data('max-kol');
			if (Number(vodjenje_lagera) == 1) {
				if (kolicina > max_col) {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Dostupna količina je " + max_col + " kom.</p>");
					$('.add-amount').val(max_col);
				} else if (Number(kolicina) < 1) {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
					$('.add-amount').val(1);
				}
			} else {
				if (Number(kolicina) < 1) {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
					$('.add-amount').val(1);
				}
			}
		});
		$('.no').click(function() {
			$('.confirm-popup').fadeOut("fast");
		});
		$('.cart-del').click(function() {
			var cart_id = $(this).data('cart-id');
			var h_br_c = document.getElementById('h_br_c').value;
			var broj_cart = Number(h_br_c) - 1;
			$('.confirm-popup').fadeOut("fast");
			$.ajax({
				type: "POST",
				url: base_url + "cart_delete",
				data: { korpa_id: cart_id },
				success: function(msg) {
					$('.header-cart-content').html(msg);
					$('#broj_cart').html("(" + broj_cart + ")");
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Artikal je uklonjen iz korpe.</p>");
					location.reload();
				}
			});
		});
		$('.cart-del-all').click(function() {
			$('.confirm-popup').fadeOut("fast");
			$.ajax({
				type: "POST",
				url: base_url + "cart_delete_all",
				data: {},
				success: function() {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>Korpa je ispražnjena.</p>");
					location.reload();
				}
			});
		});
		$('.submit-order-button').click(function() {
			if (Number(vodjenje_lagera) == 0) {
				var status = $('#status').val();
				if (status == 2) {
					var vrsta_kupca = $('.active').find('#vrsta_kupca').val();
					var ime = $('#without-reg-name').val();
					var surname = $('#without-reg-surname').val();
					var company = $('#without-reg-company').val();
					var pib = $('#without-reg-pib').val();
					var email = $('#without-reg-email').val();
					var phone = $('#without-reg-phone').val();
					var city = $('#without-reg-city').val();
					var adresa = $('#without-reg-address').val();
					var n_p = $('#nacin_placanja').val();
					var n_i = $('#nacin_isporuke').val();
					var napomena = $('#napomena').val();
					if (vrsta_kupca == 0) {
						var valid = 0;
						if (ime == '' || ime == ' ') {
							$('#without-reg-name').css("border", "1px solid red").focus().fadeIn(200);
						} else if (surname == '' || surname == ' ') {
							$('#without-reg-surname').css("border", "1px solid red").focus().fadeIn(200);
						} else {
							valid = 1;
						}
					} else {
						var valid = 0;
						if (company == '' || company == ' ') {
							$('#without-reg-company').css("border", "1px solid red").focus().fadeIn(200);
						} else if (pib == '' || pib == ' ') {
							$('#without-reg-pib').css("border", "1px solid red").focus().fadeIn(200);
						} else {
							valid = 1;
						}
					}
					if (valid == 0) {} else if (isValidEmailAddress(email) == false) {
						$('#without-reg-email').css("border", "1px solid red").focus().fadeIn(200);
					} else if (phone == '' || phone == ' ') {
						$('#without-reg-phone').css("border", "1px solid red").focus().fadeIn(200);
					} else if (adresa == '' || adresa == ' ') {
						$('#without-reg-address').css("border", "1px solid red").focus().fadeIn(200);
					} else if (city == '-1') {
						$('#without-reg-city').css("border", "1px solid red").focus().fadeIn(200);
					} else {
						$.ajax({
							type: "POST",
							url: base_url + 'order',
							data: { vrsta_kupca: vrsta_kupca, email: email, ime: ime, prezime: surname, company: company, pib: pib, telefon: phone, mesto_id: city, adresa: adresa, n_p: n_p, n_i: n_i, napomena: napomena },
							success: function(msg) {
								if(msg!='0'){
									window.location.href = base_url+'narudzbina/'+msg+'';
								}
							}
						});
					}
				} else {
					var n_p = $('#nacin_placanja').val();
					var n_i = $('#nacin_isporuke').val();
					var napomena = $('#napomena').val();
					$.ajax({
						type: "POST",
						url: base_url + 'order',
						data: { n_p: n_p, n_i: n_i, napomena: napomena },
						success: function(msg) {
							if(msg!='0'){
								window.location.href = base_url+'narudzbina/'+msg+'';
							}
						}
					});
				}
			} else {
				//alert("Proba");
				$.ajax({
					url: base_url + 'provera_stanja',
					success: function(msg) {
						var vrednost2 = 0;
						var index;
						var a = msg;
						for (index = 0; index < a.length; index += 3) {
							if (a[index] != '') {
								vrednost = Number(index) + 1;
								naziv = Number(index) + 2;
								if (a[vrednost] >= 1) {
									$('#' + a[index] + '').val(a[vrednost]);
									$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
									$('.info-popup  .popup-inner').html("<p class='p-info'>Dostupna količina artikla " + a[naziv] + " je " + a[vrednost] + " kom.  </p>");
									upload_cart(a[index], a[vrednost]);
								} else {
									$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
									$('.info-popup  .popup-inner').html("<p class='p-info'>Artikal " + a[naziv] + " je uklonjen iz korpe jer više nije dostupan.  </p>");
									delete_cart(a[index]);
								}
								location.reload();
							} else {
								var vrsta_kupca = $('.active').find('#vrsta_kupca').val();
								var status = $('#status').val();
								var ime = $('#without-reg-name').val();
								var surname = $('#without-reg-surname').val();
								var company = $('#without-reg-company').val();
								var pib = $('#without-reg-pib').val();
								var email = $('#without-reg-email').val();
								var phone = $('#without-reg-phone').val();
								var city = $('#without-reg-city').val();
								var adresa = $('#without-reg-address').val();
								var n_p = $('#nacin_placanja').val();
								var n_i = $('#nacin_isporuke').val();
								var napomena = $('#napomena').val();
								order(vrsta_kupca, email, ime, surname, company, pib, phone, city, adresa, status, n_p, n_i, napomena);
							}
						}
					}
				});
			}
		});
		$('.comment_add').click(function() {
			var ime = $('#comment_name').val();
			var pitanje = $('#comment_message').val();
			var roba_id = $('#roba_id').val();
			if (ime == '' || ime == ' ') {
				$('#comment_name').css("border", "1px solid red").focus().fadeIn(200);
			} else if (pitanje == '' || pitanje == ' ') {
				$('#comment_message').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$.ajax({
					type: "POST",
					url: base_url + 'coment_add',
					data: { ime: ime, pitanje: pitanje, roba_id: roba_id },
					success: function(msg) {
						$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
						$('.info-popup .popup-inner').html("<p class='p-info'>Vaš komentar je poslat.</p>");
						//$('.info-popup .popup-inner').html("<p class='p-info'>"+msg+"</p>");
					}
				});
			}
		});
		
		$('.JSreset-filters').click(function() {
			var url = $("#filters-url").val();
			window.location.href = base_url + 'artikli/' + url + '/0/0';
		});

		$('.filter-close').click(function() {
			var tip = $(this).data("type");
			var rb = $(this).data("rb");
			var element = $(this).data("element");
			var url = $("#filters-url").val();
			var niz_proiz = $("#niz_proiz").val();
			var niz_kara = $("#niz_kara").val();
			var proizvodjaci = "";
			var karakteristike = "";
			if (tip == 1) {
				karakteristike = niz_kara;
				niz = niz_proiz.split("-");
				if (niz.length > 1) {
					br = 0;
					niz.forEach(function(el) {
						if (el != element) {
							if (br == 0) {
								proizvodjaci += el;
							} else {
								proizvodjaci += "-" + el;
							}
							br = br + 1;
						}
					});
				} else {
					proizvodjaci = 0;
				}
			}
			if (tip == 2) {
				proizvodjaci = niz_proiz;
				niz = niz_kara.split("-");
				if (niz.length > 1) {
					br = 0;
					niz.forEach(function(el) {
						if (el != element) {
							if (br == 0) {
								karakteristike += el;
							} else {
								karakteristike += "-" + el;
							}
							br = br + 1;
						}
					});
				} else {
					karakteristike = 0;
				}
			}
			//alert(base_url+'filter/'+url+'/'+proizvodjaci+'/'+karakteristike);
			window.location.href = base_url + 'artikli/' + url + '/' + proizvodjaci + '/' + karakteristike;
		});

	});
	function delete_popup(korpa_id) {
		$('.cart-del').data('cart-id', korpa_id);
		$('.confirm-popup').fadeIn("fast");
	}
	function upload_cart(korpa_id, kolicina) {
		$.ajax({
			type: "POST",
			url: base_url + 'upload_cart',
			data: { korpa_id: korpa_id, kolicina: kolicina },
		});
	}
	function delete_cart(korpa_id) {
		$.ajax({
			type: "POST",
			url: base_url + "cart_delete",
			data: { korpa_id: korpa_id }
		});
	}
	//search
	function search() {
		search = document.getElementById('search').value;
		search = encodeURIComponent(search);
		window.location.href = base_url + "search/" + search;
	}
	
	function newsletter() {
		email = document.getElementById('newsletter').value;
		if (isValidEmailAddress(email) == true) {
			/*$.post(base_url+'newsletter',{ emeil:email }, success: function( msg ) {
		alert( msg );
		});*/
			$.ajax({
				type: "POST",
				url: base_url + 'b2b/newsletter',
				data: { email: email },
				success: function(msg) {
					$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
					$('.info-popup .popup-inner').html("<p class='p-info'>" + msg + "</p>");
				}
			});
		} else {
			$('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");
			$('.info-popup .popup-inner').html("<p class='p-info'>E-mail adresa nije validna.</p>");
		}
	}
	
	function filters_send() {
		var strChoices = "";
		var proiz = "";
		var objCBarray2 = document.getElementsByName('proizvodjac[]');
		var objCBarray = document.getElementsByName('vrednost[]');
		var url = $("#filters-url").val();
		var vrednost_arr = new Array();
		var proiz_arr = new Array();
		var j = 0;
		var p = 0;
		for (i = 0; i < objCBarray.length; i++) {
			if (objCBarray[i].checked) {
				vrednost_arr[j] = objCBarray[i].value;
				j++;
			}
		}
		for (i = 0; i < vrednost_arr.length; i++) {
			if (i == 0) {
				strChoices += vrednost_arr[i] + "";
			} else {
				strChoices += "-" + vrednost_arr[i] + ""
			}
		}
		for (i = 0; i < objCBarray2.length; i++) {
			if (objCBarray2[i].checked) {
				proiz_arr[p] = objCBarray2[i].value;
				p++;
			}
		}
		for (n = 0; n < proiz_arr.length; n++) {
			if (n == 0) {
				proiz += proiz_arr[n] + "";
			} else {
				proiz += "-" + proiz_arr[n] + "";
			}
		}
		if (vrednost_arr.length == 0) {
			strChoices = 0;
		}
		if (proiz_arr.length == 0) {
			proiz = 0;
		}
		//alert(base_url+'filters/'+url+'/'+proiz+'/'+strChoices);
		window.location.href = base_url + 'artikli/' + url + '/' + proiz + '/' + strChoices;
	}

	function order(vrsta_kupca, email, ime, surname, company, pib, phone, city, adresa, status, n_p, n_i, napomena) {
		if (status == 2) {
			if (vrsta_kupca == 0) {
				var valid = 0;
				if (ime == '' || ime == ' ') {
					$('#without-reg-name').css("border", "1px solid red").focus().fadeIn(200);
				} else if (surname == '' || surname == ' ') {
					$('#without-reg-surname').css("border", "1px solid red").focus().fadeIn(200);
				} else {
					valid = 1;
				}
			} else {
				var valid = 0;
				if (company == '' || company == ' ') {
					$('#without-reg-company').css("border", "1px solid red").focus().fadeIn(200);
				} else if (pib == '' || pib == ' ') {
					$('#without-reg-pib').css("border", "1px solid red").focus().fadeIn(200);
				} else {
					valid = 1;
				}
			}
			if (valid == 0) {} else if (isValidEmailAddress(email) == false) {
				$('#without-reg-email').css("border", "1px solid red").focus().fadeIn(200);
			} else if (phone == '' || phone == ' ') {
				$('#without-reg-phone').css("border", "1px solid red").focus().fadeIn(200);
			} else if (adresa == '' || adresa == ' ') {
				$('#without-reg-address').css("border", "1px solid red").focus().fadeIn(200);
			} else if (city == '-1') {
				$('#without-reg-city').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$.ajax({
					type: "POST",
					url: base_url + 'order',
					data: { vrsta_kupca: vrsta_kupca, email: email, ime: ime, prezime: surname, company: company, pib: pib, telefon: phone, mesto_id: city, adresa: adresa, n_p: n_p, n_i: n_i, napomena: napomena },
					success: function(msg) {
						if(msg!='0'){
							window.location.href = base_url+'narudzbina/'+msg+'';
						}
					}
				});
			}
		} else {
			var n_p = $('#nacin_placanja').val();
			var n_i = $('#nacin_isporuke').val();
			var napomena = $('#napomena').val();
			$.ajax({
				type: "POST",
				url: base_url + 'order',
				data: { n_p: n_p, n_i: n_i, napomena: napomena },
				success: function(msg) {
					if(msg!='0'){
						window.location.href = base_url+'narudzbina/'+msg+'';
					}
				}
			});
		}
	}
	function check_fileds(polje) {
		polje2 = document.getElementById(polje).value;
		if (polje == 'email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else if (polje == 'without-reg-email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#without-reg-email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#without-reg-email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else if (polje == 'kontakt-email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#kontakt-email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#kontakt-email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else {
			if (polje2 == '' || polje2 == ' ') {
				$('#' + polje).css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#' + polje).css("border", "1px solid #ccc").fadeIn(200);
			}
		}
	}

	//Proverava da li je polje prazno ili sadrzi nedozvoljene karaktere
	function isValidField(fields) {
		var check = true;
		var field = "";
		var script = /["-*<->-(){}`|^]+$/; // Invalid characters:  !"#$%^&*'() <=?> {}`|^ Valid characters: +-_., SPACE a-z A-Z 0-1
		for (i = 0; i < fields.length; i++) {
			var ind = fields[i].value.search(script);
			//For empty fields
			if ((fields[i].value == "") || (ind != -1)) {
				check = false;
				//Fokusira se na polje.Treba napraviti obavestenje za to polje, zvezdica ili nesto slicno
				fields[i].focus(fields[i].id);
				$('#' + fields[i].id).css("border", "1px solid red").next().fadeIn(200);
			} else {
				$('#' + fields[i].id).css("border", "1px solid gray").next().fadeOut(200);
			}
		}
		return check;
	}
	//Proverava da li je validna e-mail adresa
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};
	//Dozvoljava samo numeric values
	function isNumberKey(charCode) {
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}

 	function meil_send() { 
		name = document.getElementById('JSkontakt-name').value;
		email = document.getElementById('JSkontakt-email').value;
		message = document.getElementById('message').value;
		if (name == '' || name == ' ') {
			$('#JSkontakt-name').css("border", "1px solid red").focus().fadeIn(200);
		} else if (isValidEmailAddress(email) == false) {
			$('#JSkontakt-email').css("border", "1px solid red").focus().fadeIn(200);
		} else {
			$.ajax({
				type: "POST",
				url: base_url + 'meil_send',
				data: { name: name, message: message, email: email },
				success: function() {
					$('.info-popup').show().delay(1000).fadeOut(1000);
					$('.info-popup .popup-inner').html("<p class='p-info'>Vaša poruka je poslata!</p>");
					$('.popup').fadeOut("fast");
					location.reload(true);
				}
			});
		}
	}

	function getUrlVars() {
	    var vars = {};
	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	        vars[key] = value;
	    });
	    return vars;
	}

	function popupMessage(message) {
		$(document).ready(function() {
			$('.info-popup').fadeIn("fast").delay(1000).fadeOut();
		    $('.info-popup .popup-inner').html("<p class='p-info'>"+message+"</p>");
		});
	}
