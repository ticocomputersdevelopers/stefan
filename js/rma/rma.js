
$(document).ready(function () {

	$('input[name="datum_od"]').datepicker({
		format: 'Y-m-d'
	});

	$('input[name="datum_do"]').datepicker({
		format: 'Y-m-d'
	});
	$('select[name="partner_id"]').select2({placeholder: 'Izaberi servis'});
	$('select[name="serviser_id"]').select2({placeholder: 'Izaberi servisera'});
	$('select[name="roba_id"]').select2({placeholder: 'Izaberi uređaj'});
	$('select[name="rezervni_deo_id"]').select2({placeholder: 'Izaberi rezervni deo'});

	$('#JSRadniNalogSearch').click(function(){
		var search = $('input[name="search"]').val() == '' ? 'null' : $('input[name="search"]').val();
		var params = getUrlVars();
		delete params['page'];
		delete params['izvestaj'];
		delete params['izvestaj_sve'];
		params['search'] = search;

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	$('#JSRadniNalogIzvestaj').click(function(){
		var params = getUrlVars();
		delete params['page'];
		delete params['izvestaj_sve'];
		params['izvestaj'] = 1;

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSRadniNalogIzvestajSve').click(function(){
		var params = getUrlVars();
		delete params['page'];
		delete params['izvestaj'];
		params['izvestaj_sve'] = 1;

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	$('#JSRadniNalogIzvestajServiseri').click(function(){
		var params = getUrlVars();
		if($('input[name="datum_od"]').val()){
			params['datum_od'] = $('input[name="datum_od"]').val();
		}
		if($('input[name="datum_do"]').val()){
			params['datum_do'] = $('input[name="datum_do"]').val();
		}
		if($('input[name="status_id"]').val()){
			params['status_id'] = $('input[name="status_id"]').val();
		}
		window.location.href = base_url+'rma/izvestaj-serviseri'+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSRadniNalogIzvestajBrendovi').click(function(){
		var params = getUrlVars();
		if($('input[name="datum_od"]').val()){
			params['datum_od'] = $('input[name="datum_od"]').val();
		}
		if($('input[name="datum_do"]').val()){
			params['datum_do'] = $('input[name="datum_do"]').val();
		}

		window.location.href = base_url+'rma/izvestaj-brendovi'+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSRadniNalogIzvestajGrupe').click(function(){
		var params = getUrlVars();
		if($('input[name="datum_od"]').val()){
			params['datum_od'] = $('input[name="datum_od"]').val();
		}
		if($('input[name="datum_do"]').val()){
			params['datum_do'] = $('input[name="datum_do"]').val();
		}

		window.location.href = base_url+'rma/izvestaj-grupe'+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSRadniNalogIzvestajGrupeModeli').click(function(){
		var params = getUrlVars();
		if($('input[name="datum_od"]').val()){
			params['datum_od'] = $('input[name="datum_od"]').val();
		}
		if($('input[name="datum_do"]').val()){
			params['datum_do'] = $('input[name="datum_do"]').val();
		}
		if($('select[name="grupa_pr_id"]').val()){
			params['grupa_pr_id'] = $('select[name="grupa_pr_id"]').val();
		}
		window.location.href = base_url+'rma/izvestaj-modeli-grupe'+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSRadniNalogIzvestajRezervniDelovi').click(function(){
		var params = getUrlVars();
		if($('input[name="datum_od"]').val()){
			params['datum_od'] = $('input[name="datum_od"]').val();
		}
		if($('input[name="datum_do"]').val()){
			params['datum_do'] = $('input[name="datum_do"]').val();
		}
		if($('select[name="partner_id"]').val()){
			params['partner_id'] = $('select[name="partner_id"]').val();
		}

		window.location.href = base_url+'rma/izvestaj-rezervni-delovi'+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSRadniNalogIzvestajOperacije').click(function(){
		var params = getUrlVars();
		if($('input[name="datum_od"]').val()){
			params['datum_od'] = $('input[name="datum_od"]').val();
		}
		if($('input[name="datum_do"]').val()){
			params['datum_do'] = $('input[name="datum_do"]').val();
		}
		if($('select[name="partner_id"]').val()){
			params['partner_id'] = $('select[name="partner_id"]').val();
		}
		
		window.location.href = base_url+'rma/izvestaj-operacije'+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	$('input[name="datum_od"]').change(function(){
		var params = getUrlVars();
		delete params['page'];
		delete params['izvestaj'];
		delete params['izvestaj_sve'];
		params['datum_od'] = $(this).val();

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	

	$('input[name="datum_do"]').change(function(){
		var params = getUrlVars();
		delete params['page'];
		delete params['izvestaj'];
		delete params['izvestaj_sve'];
		params['datum_do'] = $(this).val();

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	

	$('#JSRadniNalogPartnerSearch').click(function(){
		var params = getUrlVars();
		var val = $('select[name="partner_id"]').val();
		if(val){
			params['partner_id'] = $('select[name="partner_id"]').val().join('-');
		}else{
			delete params['partner_id'];
		}
		delete params['serviser_id'];
		delete params['page'];
		delete params['izvestaj'];
		delete params['izvestaj_sve'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	

	$('#JSRadniNalogServiserSearch').click(function(){
		var params = getUrlVars();
		var val = $('select[name="serviser_id"]').val();
		if(val){
			params['serviser_id'] = $('select[name="serviser_id"]').val().join('-');
		}else{
			delete params['serviser_id'];
		}
		delete params['page'];
		delete params['izvestaj'];
		delete params['izvestaj_sve'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	
	$('#JSRadniNalogRobaSearch').click(function(){
		var params = getUrlVars();
		var val = $('select[name="roba_id"]').val();
		if(val){
			params['roba_id'] = $('select[name="roba_id"]').val().join('-');
		}else{
			delete params['roba_id'];
		}
		delete params['page'];
		delete params['izvestaj'];
		delete params['izvestaj_sve'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	

	$('#JSRadniNalogRezervniDeoSearch').click(function(){
		var params = getUrlVars();
		var val = $('select[name="rezervni_deo_id"]').val();
		if(val){
			params['rezervni_deo_id'] = $('select[name="rezervni_deo_id"]').val().join('-');
		}else{
			delete params['rezervni_deo_id'];
		}
		delete params['page'];
		delete params['izvestaj'];
		delete params['izvestaj_sve'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	$('select[name="radni_nalog_status_id"]').change(function(){
		var params = getUrlVars();
		delete params['page'];
		params['status_id'] = $(this).val();
		delete params['izvestaj'];
		delete params['izvestaj_sve'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	
	$('select[name="otpis"]').change(function(){
		var params = getUrlVars();
		delete params['page'];
		params['otpis'] = $(this).val();
		delete params['izvestaj'];
		delete params['izvestaj_sve'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});		
	$('select[name="ostecen"]').change(function(){
		var params = getUrlVars();
		delete params['page'];
		params['ostecen'] = $(this).val();
		delete params['izvestaj'];
		delete params['izvestaj_sve'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	$('.JSSort').click(function(){
		var params = getUrlVars();
		params['sort_column'] = $(this).data('sort_column');
		params['sort_direction'] = $(this).data('sort_direction');
		delete params['page'];
		delete params['izvestaj'];
		delete params['izvestaj_sve'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	


	$('input[name="datum_prijema"]').datetimepicker({
		format: 'Y-m-d H:i',
		mask: true
	});

	$('input[name="datum_kupovine"]').datepicker({
		format: 'Y-m-d'
	});

	$('input[name="datum_servisiranja"]').datepicker({
		format: 'Y-m-d'
	});

	$("#JSPartnerDetail").click(function() {
		if($("#JSPartner").val() !== null) {
			window.open(base_url + 'admin/kupci_partneri/partner/' + $("#JSPartner").val(), '_blank');
		}else if($("#JSPartner").val() == null) {
			alert("Mora biti izabran servis.");
		}
	});	

	$("#JSKupacDetail").click(function() {
		if($("#JSKupacId").val() != 0) {
			if($('input[name="vrsta_kupca"]:checked').val() == 'b2c' || $('input[name="vrsta_kupca"]').val() == 'b2c'){
				window.open(base_url + 'admin/kupci_partneri/kupci/' + $("#JSKupacId").val(), '_blank');
			}else{
				window.open(base_url + 'admin/kupci_partneri/partner/' + $("#JSKupacId").val(), '_blank');
			}
		}else if($("#JSKupacId").val() == 0) {
			alert("Mora biti izabran kupac.");
		}
	});	

	$("#JSPartner").change(function() {

		$.post(base_url + 'rma/ajax/radni-nalog-partner-podaci', { partner_id:$(this).val() }, function (response){
			var result = $.parseJSON(response);
			$('#JSPartnerAjaxContent').html(result.partner_details);
			$('select[name="primio_serviser_id"]').html(result.serviseri_select);
		});
	});

	$(".JSVrstaKupca").click(function() {
		$('#JSKupacId').val('0');
		$('#JSKupacAjaxContent').html('');
		$('#JSKupacIme').val('');
	});

	var kupciTime;
	$(document).on("keyup", '#JSKupacIme', function() {
		clearTimeout(kupciTime);
		kupciTime = setTimeout(function(){
			$('#JSKupacId').val('0');
			$('#JSKupacAjaxContent').html('');
			var vrsta_kupca = $('input[name="vrsta_kupca"]:checked').val();

			$('.kupac_list').remove();
			var kupci = $('#JSKupacIme').val();
			if (kupci != '' && kupci.length > 2) {
				$.post(base_url + 'rma/ajax/kupci-search', { kupci: kupci, vrsta_kupca: vrsta_kupca }, function (response){
					$('#JSKupacSearchContent').html(response);
				});
			} 				
		}, 500);
	});
	$(document).on("click", ".kupac_list__item", function() {
		$('#JSKupacIme').val($(this).find('.kupac_list__item__link__text').text());
		$('#JSKupacId').val($(this).data('web_kupac_id'));

		var vrsta_kupca = $('input[name="vrsta_kupca"]:checked').val();

		$.post(base_url + 'rma/ajax/radni-nalog-kupac-podaci', { kupac_id: $(this).data('web_kupac_id'), vrsta_kupca: vrsta_kupca }, function (response){
			$('#JSKupacAjaxContent').html(response);
		});

		$('.kupac_list').remove();
	});
	$('html :not(.kupac_list)').on("click", function() {
		$('.kupac_list').remove();
	});

	$('#JSKupacDataCreate').click(function(){
		if($('#JSKupacPolja').attr('hidden')){
			$('#JSKupacPolja').removeAttr('hidden');
			$('#JSKupacIme').val('');
			$('#JSKupacAjaxContent').html('');
			$('.kupac_list').remove();
		}else{
			$('#JSKupacPolja').attr('hidden','hidden');
		}
	});


	var productsTime;
	$(document).on("keyup", '#JSUredjajPrijem', function() {
		clearTimeout(productsTime);
		productsTime = setTimeout(function(){
			$('#JSRDRobaId').val('');
			$('.articles_list').remove();
			var articles = $('#JSUredjajPrijem').val();
			if (articles != '' && articles.length > 2) {
				$.post(base_url + 'rma/ajax/radni-nalog-rezervni-deo-roba-search', { articles: articles, lager: false }, function (response){
					$('#JSUredjajSearchContent').html(response);
				});
			} 				
		}, 500);
	});


	//OPIS RADA
	var current_norma_sat = 1.00;
	var operacija_iznos = 0.00;

	$('.JSEditRNOperacija').on('click', function(){
		var radni_nalog_operacije_id = $(this).data('radni_nalog_operacije_id');
		var radni_nalog_id = $(this).data('radni_nalog_id');

		$.post(base_url + 'rma/ajax/radni-nalog-operacija-modal-content', { radni_nalog_operacije_id: radni_nalog_operacije_id, radni_nalog_id: radni_nalog_id }, function (response){
			$('#JSEditRNOperacijaModalContent').html(response);
			current_norma_sat = $('#JSRNONormaSat').val();
			operacija_iznos = $('#JSOperacijaCenaSata').val();
			$('#JSEditRNOperacijaModal').foundation('reveal', 'open');
		});
	});

	$(document).on('keyup', '#JSRNONormaSat', function(){
		var norma_sat = $(this).val();

		if(norma_sat != ''){
			if($.isNumeric(norma_sat) && norma_sat >= 0.00 && norma_sat.length < 10){
				current_norma_sat = norma_sat;
				$('#JSRNONormaSat').val(norma_sat);
				$('#JSRNOIznos').val((operacija_iznos*norma_sat).toFixed(2));
			}else{
				$('#JSRNONormaSat').val(current_norma_sat);
			}
		}
	});

	$(document).on('change', '#JSGrupaOperacijeSelect', function(){
		var operacija_grupa_id = $(this).val();
		var partner_id = $(this).data('partner_id');
		var norma_sat = $('#JSRNONormaSat').val();

		$.post(base_url + 'rma/ajax/radni-nalog-operacija-select', { partner_id: partner_id, operacija_grupa_id: operacija_grupa_id }, function (response){
			operacija_iznos = response.iznos;
			$('#JSOperacijeSelect').html(response.operacije_select);
			$('#JSOpisOperacije').val(response.operacija_naziv);
			$('#JSRNOIznos').val((response.iznos*norma_sat).toFixed(2));
		});
	});

	$(document).on('change', '#JSOperacijeSelect', function(){
		var opis = $("#JSOperacijeSelect option:selected").text();
		var norma_sat = $('#JSRNONormaSat').val();

		var operacija_id = $(this).val();
		var partner_id = $(this).data('partner_id');

		$.post(base_url + 'rma/ajax/radni-nalog-operacija-change', { partner_id: partner_id, operacija_id: operacija_id }, function (response){
			var result = $.parseJSON(response);
			operacija_iznos = result.iznos;
			$('#JSOpisOperacije').val(opis);
			$('#JSRNOIznos').val((result.iznos*norma_sat).toFixed(2));
		});
	});

	$(document).on('click', '#JSRNOperacijaSubmit', function(){

		if($('#JSOperacijeSelect').val() == null){
			$('#JSGrupaOperacijeSelect').addClass('error');
			$('#JSOperacijeSelect').addClass('error');
		}else{

			var radni_nalog_operacije_id = $(this).data('radni_nalog_operacije_id');
			var radni_nalog_id = $(this).data('radni_nalog_id');
			var serviser_id = $('#JSServiserId').val();
			var partner_id = $(this).data('partner_id');
			var operacija_id = $('#JSOperacijeSelect').val();
			var opis_operacije = $('#JSOpisOperacije').val();
			var norma_sat = $('#JSRNONormaSat').val();
			var iznos = $('#JSRNOIznos').val();

			var data = {
				radni_nalog_operacije_id: radni_nalog_operacije_id,
				radni_nalog_id: radni_nalog_id,
				serviser_id: serviser_id,
				partner_id: partner_id,
				operacija_id: operacija_id,
				opis_operacije: opis_operacije,
				norma_sat: norma_sat,
				iznos: iznos
			};

			$.post(base_url + 'rma/ajax/radni-nalog-operacija-save', data, function (response){
				var result = $.parseJSON(response);
				setTimeout(function() { location.reload(true); }, 800);
				$('#JSEditRNOperacijaModal').foundation('reveal', 'close');
				if(result.success){
					alertify.success('Uspešno ste sačuvali podatke.');
				}else{
					alertify.error(result.error_message);
				}
			});
		}
	});


	$('.JSEditRNRezervniDeo').on('click', function(){
		var radni_nalog_rezervni_deo_id = $(this).data('radni_nalog_rezervni_deo_id');
		var radni_nalog_id = $(this).data('radni_nalog_id');

		$.post(base_url + 'rma/ajax/radni-nalog-rezervni-deo-modal-content', { radni_nalog_rezervni_deo_id: radni_nalog_rezervni_deo_id, radni_nalog_id: radni_nalog_id }, function (response){
			$('#JSEditRNRezervniDeoModalContent').html(response);
			$('#JSEditRNRezervniDeoModal').foundation('reveal', 'open');
		});
	});

	var articlesTime;
	$(document).on("keyup", '#JSRezervniDeoNaziv', function() {
		clearTimeout(articlesTime);
		articlesTime = setTimeout(function(){
			$('#JSRDRobaId').val('0');
			$('.articles_list').remove();
			var articles = $('#JSRezervniDeoNaziv').val();
			if (articles != '' && articles.length > 2) {
				$.post(base_url + 'rma/ajax/radni-nalog-rezervni-deo-roba-search', { articles: articles, lager: true }, function (response){
					$('#JSSearchContent').html(response);
				});
			} 				
		}, 500);
	});

	$(document).on("click", ".articles_list__item", function() {
		$('#JSUredjajPrijem').val($(this).find('.articles_list__item__link__text').text());
		$('#JSRDRobaId').val($(this).data('roba_id'));
		$('#JSRezervniDeoNaziv').val($(this).find('.articles_list__item__link__text').text());

		var kolicina = parseInt($('#JSRDKolicina').val());
		var cena = parseFloat($(this).data('ncena'));
		$('#JSRezervniDeoCena').val(cena);
		$('#JSRezervniDeoUkupno').val(parseFloat(kolicina*cena));

		$('.articles_list').remove();
	});

	$('html :not(.articles_list)').on("click", function() {
		$('.articles_list').remove();
	});

	$(document).on("keyup", "#JSRDKolicina", function() {
		var kolicina = parseInt($(this).val());
		var cena = parseFloat($('#JSRezervniDeoCena').val());
		$('#JSRezervniDeoUkupno').val(parseFloat(kolicina*cena).toFixed(2));
	});
	$(document).on("keyup", "#JSRezervniDeoCena", function() {
		var cena = parseFloat($(this).val());
		var kolicina = parseInt($('#JSRDKolicina').val());
		$('#JSRezervniDeoUkupno').val(parseFloat(kolicina*cena).toFixed(2));
	});
	$(document).on("keyup", "#JSRezervniDeoUkupno", function() {
		var cena_ukupno = parseFloat($(this).val());
		var kolicina = parseInt($('#JSRDKolicina').val());
		$('#JSRezervniDeoCena').val(parseFloat(cena_ukupno/kolicina).toFixed(2));
	});

	$(document).on('click', '#JSRNRezervniDeoSubmit', function(){

		var radni_nalog_rezervni_deo_id = $(this).data('radni_nalog_rezervni_deo_id');
		var radni_nalog_id = $(this).data('radni_nalog_id');
		var roba_id = $('#JSRDRobaId').val();
		var kolicina = $('#JSRDKolicina').val();
		var cena = $('#JSRezervniDeoCena').val();
		var iznos = $('#JSRezervniDeoUkupno').val();

		var data = {
			radni_nalog_rezervni_deo_id: radni_nalog_rezervni_deo_id,
			radni_nalog_id: radni_nalog_id,
			roba_id: roba_id,
			kolicina: kolicina,
			cena: cena,
			iznos: iznos
		};

		$('#JSRezervniDeoNaziv').removeClass('error');
		$('#JSRDKolicina').removeClass('error');
		$('#JSRezervniDeoCena').removeClass('error');
		$('#JSRezervniDeoUkupno').removeClass('error');

		$.post(base_url + 'rma/ajax/radni-nalog-rezervni-deo-save', data, function (response){
			if(response.success){
				setTimeout(function() { location.reload(true); }, 800);
				$('#JSEditRNRezervniDeoModal').foundation('reveal', 'close');
				alertify.success('Uspešno ste sačuvali podatke.');
			}else{
				if(response.error_message){
					$('#JSEditRNRezervniDeoModal').foundation('reveal', 'close');
					alertify.error(response.error_message);
				}

				if(response.errors.roba_id && response.errors.roba_id.length > 0){
					$('#JSRezervniDeoNaziv').addClass('error');
				}
				if(response.errors.kolicina && response.errors.kolicina.length > 0){
					$('#JSRDKolicina').addClass('error');
				}
				if(response.errors.cena && response.errors.cena.length > 0){
					$('#JSRezervniDeoCena').addClass('error');
				}
				if(response.errors.iznos && response.errors.iznos.length > 0){
					$('#JSRezervniDeoUkupno').addClass('error');
				}
			}
		});

	});

	$('.JSEditRNTrosak').on('click', function(){
		var radni_nalog_trosak_id = $(this).data('radni_nalog_trosak_id');
		var radni_nalog_id = $(this).data('radni_nalog_id');

		$.post(base_url + 'rma/ajax/radni-nalog-trosak-modal-content', { radni_nalog_trosak_id: radni_nalog_trosak_id, radni_nalog_id: radni_nalog_id }, function (response){
			$('#JSEditRNTrosakModalContent').html(response);

			$('#JSDatumRacuna').datetimepicker({
				format: 'Y-m-d',
				timepicker: false
			});

			$('#JSEditRNTrosakModal').foundation('reveal', 'open');
		});
	});

	$(document).on('click', '#JSRNTrosakSubmit', function(){

		var radni_nalog_trosak_id = $(this).data('radni_nalog_trosak_id');
		var radni_nalog_id = $(this).data('radni_nalog_id');
		var partner_id = $('#JSUsluzniServisId').val();
		var broj_racuna = $('#JSBrojRacuna').val();
		var datum_racuna = $('#JSDatumRacuna').val();
		var vrednost_racuna = $('#JSVrednostRacuna').val();
		var opis = $('#JSTrosakOpis').val();

		var data = {
			radni_nalog_trosak_id: radni_nalog_trosak_id,
			radni_nalog_id: radni_nalog_id,
			partner_id: partner_id,
			broj_racuna: broj_racuna,
			datum_racuna: datum_racuna,
			vrednost_racuna: vrednost_racuna,
			opis: opis
		};

		$('#JSUsluzniServisId').removeClass('error');
		$('#JSBrojRacuna').removeClass('error');
		$('#JSDatumRacuna').removeClass('error');
		$('#JSVrednostRacuna').removeClass('error');
		$('#JSTrosakOpis').removeClass('error');

		$.post(base_url + 'rma/ajax/radni-nalog-trosak-save', data, function (response){
			if(response.success){
				setTimeout(function() { location.reload(true); }, 800);
				$('#JSEditRNTrosakModal').foundation('reveal', 'close');
				alertify.success('Uspešno ste sačuvali podatke.');
			}else{
				if(response.error_message){
					$('#JSEditRNTrosakModal').foundation('reveal', 'close');
					alertify.error(response.error_message);
				}

				if(response.errors.partner_id && response.errors.partner_id.length > 0){
					$('#JSUsluzniServisId').addClass('error');
				}
				if(response.errors.broj_racuna && response.errors.broj_racuna.length > 0){
					$('#JSBrojRacuna').addClass('error');
				}
				if(response.errors.datum_racuna && response.errors.datum_racuna.length > 0){
					$('#JSDatumRacuna').addClass('error');
				}
				if(response.errors.vrednost_racuna && response.errors.vrednost_racuna.length > 0){
					$('#JSVrednostRacuna').addClass('error');
				}
				if(response.errors.opis && response.errors.opis.length > 0){
					$('#JSTrosakOpis').addClass('error');
				}
			}
		});

	});

	var searchTimeUzrok;
	$('#JSRNOperacijaUzrok').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSRNOperacijaUzrok').val() != ''){
        	timer_uzrok();
    	}
		clearTimeout(searchTimeUzrok);
		searchTimeUzrok = setTimeout(timer_uzrok, 700);
	});
 
	function timer_uzrok(){
		$.post(base_url + 'rma/ajax/radni-nalog-opreacija-uzrok-save', { radni_nalog_id: $('#JSRNOperacijaUzrok').data('radni_nalog_id'), uzrok_kvara: $('#JSRNOperacijaUzrok').val() }, function (response){
			if(!response.success){
				alertify.error(response.error_message);
			}else{
				alertify.success('Uspešno ste sačuvali podatke.');
			}
		});
	}

	var searchTime;
	$('#JSRNOperacijaNapomena').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSRNOperacijaNapomena').val() != ''){
        	timer();
    	}
		clearTimeout(searchTime);
		searchTime = setTimeout(timer, 700);
	});
 
	function timer(){
		$.post(base_url + 'rma/ajax/radni-nalog-opreacija-napomena-save', { radni_nalog_id: $('#JSRNOperacijaNapomena').data('radni_nalog_id'), napomena_operacije: $('#JSRNOperacijaNapomena').val() }, function (response){
			if(!response.success){
				alertify.error(response.error_message);
			}else{
				alertify.success('Uspešno ste sačuvali podatke.');
			}
		});
	}

	//dokumenti
	$('input[name="datum_dokumenta"]').datetimepicker({
		format: 'Y-m-d',
		timepicker: false
	});
	$('input[name="prijem_datum_dokumenta"]').datetimepicker({
		format: 'Y-m-d',
		timepicker: false
	});
	$('input[name="datum_zavrsetka"]').datetimepicker({
		format: 'Y-m-d H:i',
		mask: true
	});

	$('#JSRNRekapitulacija').click(function() {
		var link = base_url + "rma/radni-nalog-rekapitulacija/"+$(this).data('id');
		if($('#JSRNRekapitulacijaITrosak').is(":checked")){
			link += '/1';
		}
		location.href = link;
	});
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}