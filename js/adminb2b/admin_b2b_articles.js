$(document).ready(function () {


	var ord = location.pathname.split('/')[11];
	
	if(ord != null){
		var order = ord.split('-');
		if(order[1] == 'ASC'){
			$('.table-head').filter('[data-coll="'+ order[0] +'"]').attr('data-sort', 'ASC');
			$('.table-head').filter('[data-coll="'+ order[0] +'"]').find('span').html('&#x25B2');
		}else{
			$('.table-head').filter('[data-coll="'+ order[0] +'"]').attr('data-sort', 'DESC');
			$('.table-head').filter('[data-coll="'+ order[0] +'"]').find('span').html('&#x25BC');
		}
		
	}
	
	$('.table-head').click(function(){

		if($(this).data('sort') == 'DESC'){
			var coll = $(this).data('coll');
			var sort = 'ASC';
		}else{
			var coll = $(this).data('coll');
			var sort = 'DESC';
		}

		location.href = base_url+'admin/b2b/article-list/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+coll+'-'+sort;
	});
	if ($('#search').length>0) {
		if(criteria[7] != 0){
			$('#search').val(criteria[7]); 
		}
	}
	$('#search-btn').click(function(){

		var search = $('#search').val();
		var search1 = search.replace('/', ' ');
		if(search != ''){
			location.href = base_url+'admin/b2b/article-list/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+search1+'/'+criteria[6];
		}
	});
	$('#clear-btn').click(function(){

		location.href = base_url+'admin/b2b/article-list/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/0/'+criteria[6];

	});
	

	//search cena
	if(criteria != null){
		if(criteria[6] != 'nn-nn'){
			var nabavna_arr = criteria[6].split('-');
			if($.isNumeric(nabavna_arr[0])){
				$('#JSCenaOd').val(nabavna_arr[0]); 
			}
			if($.isNumeric(nabavna_arr[1])){
				$('#JSCenaDo').val(nabavna_arr[1]); 
			}
		}
	}
	$('#JSCenaSearch').click(function(){
		var nabavna_od = $('#JSCenaOd').val();
		var nabavna_do = $('#JSCenaDo').val();
		if($.isNumeric(nabavna_od) && $.isNumeric(nabavna_do)){
			location.href = base_url+'admin/b2b/article-list/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+nabavna_od+'-'+nabavna_do;
		}
	});
	$('#JSCenaClear').click(function(){

		location.href = base_url+'admin/b2b/article-list/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/nn-nn';

	});

     var roba_ids = new Array();
     var selected_roba_ids;
     
	  $(function() {
	    $( "#selectable" ).selectable({
	      filter: 'tr',
	      stop: function() {
		      	roba_ids = [];
		      	$("td").removeClass("ui-selected");
		      	$("td").find('input').removeClass("ui-selected");
		        
		        $( ".ui-selected", this ).each(function() {
		          var id = $( this ).data("id");
		          roba_ids.push(id);
		        });
		        selected_roba_ids = roba_ids;
	        }
	    });
	  });

	  $( "#all" ).click(function(){
	  		$("#selectable").find("tr").addClass("ui-selected");
			$( ".ui-selected", "#selectable" ).each(function() {
	          var id = $( this ).data("id");
	          roba_ids.push(id);
	        });
			//selected_roba_ids = roba_ids;
			selected_roba_ids = all_ids;
	  });

	  $("#proizvodjac_select").select2({
	  	placeholder: 'Izaberi proizvođače'
	  	});
	  $("#dobavljac_select").select2({
	  	placeholder: 'Izaberi dobavljače'
	  	});
	  $("#tip_select").select2({
	  	placeholder: 'Izaberi tip'
	  	});

		if(criteria != null){
			var filters = criteria[4].split("-");
			var i=1;
			$.each(filters, function( index, value ) {
				if(value == "dd"){
					$( ".row" ).find('[data-check="'+i+'"]').attr('checked', '');
					$( ".row" ).find('[data-check="'+(i+1)+'"]').attr('checked', '');
				}
				if(value == "1"){
					$( ".row" ).find('[data-check="'+i+'"]').attr('checked', '');
				}
				if(value == "0"){
					$( ".row" ).find('[data-check="'+(i+1)+'"]').attr('checked', '');
				}
				i=i+2;
			});	 
		}

	   $( ".filter-check" ).click(function(){
	   		var element = $(this);
	     		if(element.attr('checked')){
     				$.each(filters, function( index, value ) {
			     		if(element.data("check") == 2*index+1 && !element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "nn";
			     		}
			     		if(element.data("check") == 2*index+1 && element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "0";
			     		}
			     		if(element.data("check") == 2*index+2 && element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "1";
			     		}
			     		if(element.data("check") == 2*index+2 && !element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "nn";
			     		}
					});
		     	}else{
     				$.each(filters, function( index, value ) {
			     		if(element.data("check") == 2*index+1 && !element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "1";
			     		}
			     		if(element.data("check") == 2*index+1 && element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "1";
			     		}
			     		if(element.data("check") == 2*index+2 && !element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "0";
			     		}
			     		if(element.data("check") == 2*index+2 && element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "0";
			     		}			
					});
		     	}
		  		var filteri = '';
				$.each(filters, function( index, value ) {
					filteri += '-' + value;
				});
				location.href = base_url+'admin/b2b/article-list/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+filteri.substr(1)+'/'+criteria[5]+'/'+criteria[6];	  
		});

	   $( ".dobavljac_proizvodjac" ).click(function(){
	  		var proizvodjaci = '';
	  		$("#proizvodjac_select :selected").each(function(){
	  			proizvodjaci += '-' + $(this).val();
	  		});
	  		if(proizvodjaci == ''){
	  			proizvodjaci = '-0';
	  		}
	  		var dobavljaci = '';
	  		$("#dobavljac_select :selected").each(function(){
	  			dobavljaci += '-' + $(this).val();

	  		});
	  		if(dobavljaci == ''){
	  			dobavljaci = '-0';
	  		}

	  		var magacin = '';
  			magacin = $("#magacin_select").val();
	  		if(magacin == ''){
	  			magacin = '0';
	  		}

	  		location.href = base_url+'admin/b2b/article-list/'+criteria[0]+'/'+proizvodjaci.substr(1)+'/'+dobavljaci.substr(1)+'/'+magacin+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6];
	  });

	// Right click menu
	var active = 0;
	$('.articles-listing').on("contextmenu", function(e) {
		$('.custom-menu, .articles-mn').css('top', e.pageY + "px");
		$('.custom-menu, .articles-mn').css('left', e.pageX - 55 + "px" );

		if(active == 0) {
			$('.custom-menu').addClass('custom-menu-active');
			active = 1;
		} else if(active == 1) {
			$('.custom-menu').removeClass('custom-menu-active');
			active = 0;
		}
		
    	return false;
  	});

	$(document).on('click', function(){
		$('.custom-menu').removeClass('custom-menu-active');
		active = 0;
	});

	$('.custom-menu-item').on('click', function(){
		active = 0;
	});

	$(document).keydown(function(e){
		if(e.which == 114){
			$('#all').trigger('click');
			return false;
		} else if(e.which == 113){
			window.open(base_url+'admin/product/0', '_blank');
			return false;
		} else if(e.which == 115){
			$('#JSIzmeni').trigger('click');
			return false;
		} else if(e.which == 116){
			$('#JSUrediOpis').trigger('click');
			return false;
		} else if(e.which == 119){
			$('#JSObrisiArtikal').trigger('click');
			return false;
		} else if(e.which == 118){
			$('#JSRefresh').trigger('click');
			return false;
		}

	});
	
	$('.custom-menu-sub').hover(function(){
		$('.custom-menu-sub-item').toggleClass('active-sub');
	});


	// Article images dropdown
	$('.JSimages-options').hide();
	$('.JSimages-head').on('click', function(){
		$('.JSimages-options').toggle();
	});


	$('.fixed-cat-menu').on('click', function(){
		$('.categories').toggle();
		$('.articles-content').toggleClass('wide-articles');
	});


// RABAT GRUPA
var  lastVal ="";
var ifEnter = 0;

$('.rabat_edit').focus(function () {	
    lastVal = $(this).val();
});

// $('.rabat_edit, .rabat_group_edit, .akc_rabat_group_edit, #JSCenaOd, #JSCenaDo').keydown(function(e){
// 	if(e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 190 && e.keyCode != 37 && e.keyCode != 39){
// 		e.preventDefault();
// 	}	
// });

$('.rabat_edit').keyup(function(e){

	// if (e.keyCode == 38) {
	//   	$(this).closest('li').prev().find('.rabat_edit').focus();
	//   }

	// if (e.keyCode == 40) {
	//   	$(this).closest('li').next().find('.rabat_edit').focus();
	// }

	if(e.keyCode == 13) {

		if (lastVal != $(this).val()){

			var grupa_id = $(this).closest('div').data('id');
			var rabat = $(this).val();
			if(rabat != ''){

				ifEnter = 1;
				$(this).blur();

				$.post(
					base_url+'admin/b2b/ajax/rabat_edit', {
						action: 'rabat_edit',
						grupa_id: grupa_id,
						rabat: rabat
					}, function (response){
						if(response == 1) {
							alertify.success('Uspešno ste sačuvali rabat.');
						} else {
							alertify.error('Greška prilikom upisivanja u bazu.');
						}
					}
				);
			}		
		}
	}
});

$('.rabat_edit').blur(function(e){

    if (lastVal != $(this).val() && !(ifEnter)){

	    var grupa_id = $(this).closest('div').data('id');
		var rabat = $(this).val();
		if(rabat != ''){


			$.post(
				base_url+'admin/b2b/ajax/rabat_edit', {
					action: 'rabat_edit',
					grupa_id: grupa_id,
					rabat: rabat
				}, function (response){
					if(response == 1) {
						alertify.success('Uspešno ste sačuvali rabat.');
					} else {
						alertify.error('Greška prilikom upisivanja u bazu.');
					}
				}
			);
		}
	} else if(ifEnter){
		ifEnter = 0;
	}
});

// RABAT ARTIKALA
$('#rabat_group_edit-btn').click(function(e){
	var rabat = $('.rabat_group_edit').val();
	var roba_ids = selected_roba_ids;

	if(roba_ids && rabat != '') {
		$.post(
			base_url+'admin/b2b/ajax/rabat_group_edit', {
				action: 'rabat_group_edit',
				roba_ids: roba_ids,
				rabat: rabat
			}, function (response){

				alertify.success('Uspešno ste sačuvali maksimalni rabat.');
				var data = $.parseJSON(response);

				$.each(data, function(index, value) {
	  				var obj_row = $("#selectable").find('[data-id="'+ index +'"]');
	  				obj_row.find('.rabat').html(value);
				});

			}
		);
	} else {
		alertify.error('Morate izabrati artikal/e i upisati željeni maksimalni rabat.');
	}

});

	$('#akc_rabat_group_edit-btn').click(function(e){
	var akcrabat = $('.akc_rabat_group_edit').val();
	var roba_ids = selected_roba_ids;

	if(roba_ids && akcrabat != '') {
		$.post(
			base_url+'admin/b2b/ajax/akc_rabat_group_edit', {
				action: 'akc_rabat_group_edit',
				roba_ids: roba_ids,
				akcrabat: akcrabat
			}, function (response){

				alertify.success('Uspešno ste sačuvali akcijski rabat.');
				var data = $.parseJSON(response);

				$.each(data, function(index, value) {
	  				var obj_row = $("#selectable").find('[data-id="'+ index +'"]');
	  				obj_row.find('.akcrabat').html(value);
				});

			}
		);
	} else {
		alertify.error('Morate izabrati artikal/e i upisati željeni akcijski rabat.');
	}

});

 $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myList li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

});



